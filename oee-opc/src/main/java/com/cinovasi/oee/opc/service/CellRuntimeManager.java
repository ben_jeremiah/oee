package com.cinovasi.oee.opc.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.constants.Tag;
import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.CellTag;
import com.cinovasi.oee.core.service.CellService;
import com.cinovasi.oee.core.service.CellTagService;

public class CellRuntimeManager implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(CellRuntimeManager.class);
    private static CellRuntimeManager INSTANCE;
    CellService cellService;

    private List<CellRuntime> cellRuntimes;
    private ScheduledExecutorService executor;
    private static int TICK_INTERVAL = 5000; // millisecond
    private boolean started;
    private BundleContext bundleContext;

    public static CellRuntimeManager getInstance() {
		if(INSTANCE == null){
			synchronized(CellRuntimeManager.class){
				if(INSTANCE == null){
					INSTANCE = new CellRuntimeManager();
				}
			}
		}
		return INSTANCE;
    }

    private CellRuntimeManager() {
        cellRuntimes = Collections.synchronizedList(new ArrayList<CellRuntime>());
        executor = Executors.newScheduledThreadPool(1);
    }
    
    public void setBundleContext(BundleContext ctx)
    {
    	bundleContext = ctx;
    }

    private void createObjects() {

        cellRuntimes.clear();

        // init tag list
        Collection<CellTag> nonLineTags = getNonLineTags();
        Collection<Cell> cells = getAllCells();
        for (Cell cell : cells) {
        	logger.info("create runtime object for {}:{}", cell.getId(), cell.getDescription());
            CellRuntimeDataFacade dataFacade = new CellRuntimeDataFacade();
            dataFacade.setCellId(cell.getId());
            dataFacade.setCellName(cell.getDescription());

            Map<Tag, String> tagMap = new HashMap<Tag, String>();

        	logger.info("got {} tags", cell.getCellTags().size());
            for (CellTag cellTag : cell.getCellTags()) {
                tagMap.put(cellTag.getTagKey(), cellTag.getTagName());
            }

            for(CellTag cellTag : nonLineTags) {
				tagMap.put(cellTag.getTagKey(), cellTag.getTagName());
            }
            
            dataFacade.setTagNameMap(tagMap);

            CellRuntime cr = new CellRuntime();
            cr.setBundleContext(this.bundleContext);
            cr.setCell(cell);
            cr.setCellId(cell.getId());
            cr.setCellName(cell.getDescription());
            cr.setDataFacade(dataFacade);
            cr.syncState();
            cellRuntimes.add(cr);
        }
    }

    public CellRuntime getCellRuntime(int cellId) {
        CellRuntime toReturn = null;
        for (CellRuntime cr : cellRuntimes) {
            if (cr.getCellId() == cellId) {
                toReturn = cr;
                break;
            }
        }
        return toReturn;
    }

    public void start() {
		createObjects();
    	
		if(executor.isShutdown())
		{
			executor = Executors.newScheduledThreadPool(1);
		}
        executor.scheduleAtFixedRate(this, TICK_INTERVAL, TICK_INTERVAL, TimeUnit.MILLISECONDS);
        started = true;
    }

    public void stop() {
    	if(started)
    	{
	        try {
	            synchronized (executor) {
	                executor.shutdown();
	                executor.awaitTermination(10, TimeUnit.SECONDS);
	            }
	        } catch (Exception e) {
	            logger.error("Error while shutting down", e);
	        }
	        finally
	        {
	    		started = false;
	    		bundleContext = null;
                cellRuntimes.clear();
	        }
    	}
    }

    @Override
    public void run() {
        synchronized (cellRuntimes) {
            for (CellRuntime ar : cellRuntimes) {
                ar.tick();
            }
        }
    }
    
	private ServiceReference<?> getCellServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(CellService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private CellService getCellService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			CellService dao = (CellService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}
	
	private Collection<Cell> getAllCells()
	{
		Collection<Cell> cells = new ArrayList<Cell>();
		ServiceReference<?> ref = getCellServiceReference();
		if(ref != null)
		{
			CellService service = getCellService(ref);
			if(service != null)
			{
				cells = service.getCells();
			}
			bundleContext.ungetService(ref);
		}
		return cells;		
	}
	
	private ServiceReference<?> getCellTagServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(CellTagService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private CellTagService getCellTagService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			CellTagService dao = (CellTagService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}
	
	private Collection<CellTag> getNonLineTags()
	{
		Collection<CellTag> cells = new ArrayList<CellTag>();
		ServiceReference<?> ref = getCellTagServiceReference();
		if(ref != null)
		{
			CellTagService service = getCellTagService(ref);
			if(service != null)
			{
				cells = service.getNonLineTags();
			}
			bundleContext.ungetService(ref);
		}
		return cells;		
	}
	
}