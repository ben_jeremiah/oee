package com.cinovasi.oee.opc;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.opc.service.CellRuntimeManager;

public class Activator implements BundleActivator, ManagedService {
	private static Logger log = LoggerFactory.getLogger(Activator.class);
	private static final String CONFIG_PID = "com.cinovasi.oee.opc";
	private ServiceRegistration<?> serviceReg;
	
	@Override
	public void start(BundleContext context) throws Exception {
		log.info("Activator.started, got BundleContext");
		Worker.getInstance().setBundleContext(context);
		CellRuntimeManager.getInstance().setBundleContext(context);
		Hashtable<String, Object> properties = new Hashtable<String, Object>();
		properties.put(Constants.SERVICE_PID, CONFIG_PID);
		serviceReg = context.registerService(ManagedService.class.getName(), this , properties);

	}

	@Override
	public void stop(BundleContext context) throws Exception {
		try {
			log.info("Activator.stopped");
			Worker.getInstance().stop();
			CellRuntimeManager.getInstance().stop();
			serviceReg.unregister();
		} finally {
		}
	}

	@Override
	public void updated(Dictionary<String, ?> properties) throws ConfigurationException {

		if(properties == null)
		{
			// set default OPCGW address
			Worker.getInstance().setHost("localhost");
			Worker.getInstance().setPort(12000);
		}
		else
		{
			String host = (String) properties.get("host");
			String strPort = (String) properties.get("port");

			if (host != null) {
				Worker.getInstance().setHost(host);
			} else {
				Worker.getInstance().setHost("localhost");
			}

			if (strPort != null) {
				try {
					int port = Integer.valueOf(strPort);
					Worker.getInstance().setPort(port);
				} catch (NumberFormatException e) {
					Worker.getInstance().setPort(12000);
				}
			} else {
				Worker.getInstance().setPort(12000);
			}
			
		}
		Worker.getInstance().start();
		CellRuntimeManager.getInstance().start();
	}

}
