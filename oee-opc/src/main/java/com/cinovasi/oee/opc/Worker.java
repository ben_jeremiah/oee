package com.cinovasi.oee.opc;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.RuntimeDataDTO;
import com.cinovasi.oee.core.entities.CellTag;
import com.cinovasi.oee.core.service.CellTagService;
// import com.cinovasi.oee.core.service.RuntimeHistoryService;
import com.cinovasi.opcgw.api.beans.TagInfo;
import com.cinovasi.opcgw.api.beans.TagInfos;
import com.cinovasi.opcgw.api.beans.TagList;
import com.cinovasi.opcgw.api.exception.MissingInformationException;
import com.cinovasi.opcgw.api.exception.ServiceExecutionException;
import com.cinovasi.opcgw.proxy.OPCGatewayServiceProxy;


/**
 * App
 */
public class Worker implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(Worker.class);
    private static Worker instance ;

    private Thread backgroundThread;
    private volatile boolean threadRun;
    private OPCGatewayServiceProxy proxy; 
    private static String OPC_GROUP_NAME = "OEE_OPC";
    private Lock lock;
    private String host;
    private int port;
	private BundleContext bundleContext;
	private boolean hasValidBundleContext;
	private boolean hasValidConfig;
	private final int SLEEP_TIME = 2000;
	private final int ONE_MINUTE = 60000 / SLEEP_TIME;
    private Map<String, TagInfo> OPC_VALUE_CACHE;
    private Collection<RuntimeDataDTO> cacheList;
    private Collection<RuntimeDataDTO> previousCacheList;
    private Map<String, RuntimeDataDTO> cacheMap;
    private Map<String, RuntimeDataDTO> previousCacheMap;
    

    
    public static Worker getInstance()
    {
		if(instance == null){
			synchronized(Worker.class){
				if(instance == null){
					instance = new Worker();
				}
			}
		}
    	return instance;
    }
    
    private Worker() {
    	lock = new ReentrantLock();
    	hasValidBundleContext = hasValidConfig = false;
    }
    
    public void shutdown()
    {
    	logger.info("set threadRun to FALSE");
        threadRun = false;
    }


    @Override
    public void run()
    {
    	int tickOneMinute = ONE_MINUTE;
    	while(threadRun)
    	{
    		try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
		    	logger.error("Interrupted while sleeping ...", e);
		    	return;
			}
    		
    		try {
				TagInfos ti = proxy.fetch(OPC_GROUP_NAME);
				
                if (ti != null) {
                    Vector<TagInfo> tags = ti.getTagInfos();
                    for (TagInfo tagInfo : tags) {
                        OPC_VALUE_CACHE.put(tagInfo.getTagName(), tagInfo);
                    }
                }
				
				Collection<RuntimeDataDTO> bufferList = new ArrayList<RuntimeDataDTO>();
				Map<String, RuntimeDataDTO> bufferMap = Collections.synchronizedMap( new HashMap<String,RuntimeDataDTO>());
				for(TagInfo t : ti.getTagInfos())
				{
					//logger.info("tagName: {}, value: {}",t.getTagName(), t.getValue());
					
					RuntimeDataDTO dto = new RuntimeDataDTO();
					dto.setTagName(t.getTagName());
					dto.setDataTimeStamp(t.getDataTimeStamp() != null ? t.getDataTimeStamp().getTime() : new Date());
					dto.setQuality(t.getQuality());
					dto.setOpcDataType(t.getOpcDataType());
					dto.setValue(t.getValue());
					dto.setErrMsg(t.getErrMsg());
					bufferList.add(dto);
					bufferMap.put(dto.getTagName(), dto);
				}
				
				setValueCache(bufferList, bufferMap);
				
    		} catch (RemoteException | ServiceExecutionException | MissingInformationException e) {
		    	logger.error("Error while fetch OPC data ...", e);
			}
    		
    	}

    	try {
			proxy.stop(OPC_GROUP_NAME);
		} catch (RemoteException | ServiceExecutionException | MissingInformationException e) {
	    	logger.error("Error while stop OPC Proxy", e);
		}
    	logger.info("Exit background thread loop");
    }

	public void start()
	{
		logger.info("start & initialization");
		
		if(!hasValidBundleContext || !hasValidConfig)
		{
			logger.info("Don't have valid BundleContext or Config yet");
			return;
		}

		Collection<CellTag> tags = getAllTags();
		logger.info("got {} tags to monitor", tags.size());
        Vector<String> TAG_LIST = new Vector<String>();
        for(CellTag ct : tags)
        {
        	TAG_LIST.add(ct.getTagName());
        }
        
        TagList tagList = new TagList();
        tagList.setTags(TAG_LIST);
        OPC_VALUE_CACHE = Collections.synchronizedMap(new HashMap<String, TagInfo>());
    	cacheList = new ArrayList<RuntimeDataDTO>();
        cacheMap = Collections.synchronizedMap(new HashMap<String, RuntimeDataDTO>());
    	previousCacheList = null;
        previousCacheMap = null;

		ClassLoader oldCl = Thread.currentThread().getContextClassLoader();
        try
        {
			proxy = new OPCGatewayServiceProxy(this.host, this.port);
	    	logger.info("change TCCL to OPCGatewayServiceProxy class loader");
			Thread.currentThread().setContextClassLoader(proxy.getClass().getClassLoader());

			logger.info("invoke stop - register - start");
			proxy.stop(OPC_GROUP_NAME);
	    	proxy.register(OPC_GROUP_NAME, tagList);
	    	proxy.start(OPC_GROUP_NAME);
	    	
	        backgroundThread = new Thread(this);
	        threadRun = true;
	    	logger.info("Start Background thread");
	        backgroundThread.start();
	    	logger.info("Initialization completed");
	    	
        }
        catch(Exception exc)
        {
        	logger.error("Error while starting OPCGatewayProxy", exc);
        	return;        	
        }
		finally
		{
			logger.info("change TCCL back to original");
			Thread.currentThread().setContextClassLoader(oldCl);
		}
	}
	
	public void stop()
	{
    	logger.info("stopBundle");
       try {
        	if(backgroundThread != null && backgroundThread.isAlive())
        	{
            	shutdown();
    			backgroundThread.join();
    	    	logger.info("Bye!");
        	}
		} catch (InterruptedException e) {
	    	logger.error("Error while waiting Background thread", e);
		}
        finally
        {
			backgroundThread = null;
			proxy = null;
			bundleContext = null;
	    	hasValidBundleContext = hasValidConfig = false;
			
        }
	}

	private void setValueCache(Collection<RuntimeDataDTO> list, Map<String,RuntimeDataDTO> map)
	{
		lock.lock();
		try
		{
			cacheList = list;
			cacheMap = map;
		}
		finally
		{
			lock.unlock();
		}
		
	}
	
	public Collection<RuntimeDataDTO> getCacheList()
	{
		lock.lock();
		try
		{
			return cacheList;
		}
		finally
		{
			lock.unlock();
		}
	}
	
	public Map<String,RuntimeDataDTO> getCacheMap()
	{
		lock.lock();
		try
		{
			return cacheMap;
		}
		finally
		{
			lock.unlock();
		}
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
		hasValidConfig = true;
	}

	public BundleContext getBundleContext() {
		return bundleContext;
	}

	public void setBundleContext(BundleContext bundleContext) {
		this.bundleContext = bundleContext;
		hasValidBundleContext = true;
	}
	
	private ServiceReference<?> getCellTagServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(CellTagService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private CellTagService getCellTagService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			CellTagService dao = (CellTagService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}
	
	// private ServiceReference<?> getRuntimeHistoryServiceReference()
	// {
	// 	int retry = 30;
	// 	while (retry > 0) {
	// 		ServiceReference<?> ref = bundleContext.getServiceReference(RuntimeHistoryService.class.getName());
	// 		if (ref == null) {
	// 			try {
	// 				retry--;
	// 				Thread.sleep(1000);
	// 			} catch (InterruptedException exc) {
	// 				break;
	// 			}
	// 			continue;
	// 		}
	// 		else return ref;
	// 	}
	// 	return null;
	// }
	
	// private RuntimeHistoryService getRuntimeHistoryService(ServiceReference<?> reference)
	// {
	// 	int retry = 30;
	// 	while (retry > 0) {
	// 		RuntimeHistoryService dao = (RuntimeHistoryService) bundleContext.getService(reference);
	// 		if (dao == null) {
	// 			try {
	// 				retry--;
	// 				Thread.sleep(1000);
	// 			} catch (InterruptedException exc) {
	// 				break;
	// 			}
	// 			continue;
	// 		} else
	// 			return dao;
	// 	}
	// 	return null;
	// }
	
	private Collection<CellTag> getAllTags()
	{
		Collection<CellTag> tags = new ArrayList<CellTag>();
		try
		{
			ServiceReference<?> ref = getCellTagServiceReference();
			if(ref != null)
			{
				CellTagService dao = getCellTagService(ref);
				if(dao != null)
				{
					tags = dao.getCellTags();
				}
				else
				{
					logger.warn("Cannot get CellTagService object !");
				}
				bundleContext.ungetService(ref);
			}
			else
			{
				logger.warn("Cannot get CellTag Service Reference !");
			}
		}
		catch(Exception exc)
		{
			logger.error("Error while getAllTags", exc);
		}
		return tags;
	}
	
    public TagInfo readData(String tagName) {
        return OPC_VALUE_CACHE.get(tagName);
    }
	
    public boolean writeData(String tagName, Object value) {
        // StopWatch sw = new StopWatch();
        // sw.start();
        try {
            synchronized (proxy) {
            	proxy.write(OPC_GROUP_NAME, tagName, value);
                TagInfo tagInfo = OPC_VALUE_CACHE.get(tagName);
                if (tagInfo != null) {
                    tagInfo.setValue(value.toString());
                    OPC_VALUE_CACHE.put(tagName, tagInfo);
                }
            }
            return true;
        } catch (RemoteException | ServiceExecutionException | MissingInformationException e) {
            logger.error("Error while writeData", e);
            return false;
        } finally {
            // sw.stop();
            // logger.debug("writeData within {} ms", sw.getTime());
        }
    }
	
}
