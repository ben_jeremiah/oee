package com.cinovasi.oee.opc.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.opcgw.api.exception.ConfigurationException;

public class ServiceConfiguration {
	private static Logger logger = LoggerFactory.getLogger(ServiceConfiguration.class);

	private static final String PROPERTIES_KEY = "ism.services.properties";
	private static final String PROPERTIES_FILE = "properties/services.properties";

	/** the properties **/
	private Properties properties = null;

	private static ServiceConfiguration INSTANCE = null;

	/**
	 * @return Returns a singleton of Properties
	 */
	public static ServiceConfiguration getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ServiceConfiguration();
		}
		return INSTANCE;
	}

	/**
	 * Default Constructor
	 */
	private ServiceConfiguration() {
		init();
	}

	/**
	 * @return Properties File
	 */
	private String getPropFile() {
		Properties sysProp = System.getProperties();
		return sysProp.getProperty(PROPERTIES_KEY, PROPERTIES_FILE);
	}

	/**
	 * Initializes the properties variable
	 */
	private void init() {
		InputStream inputStream = null;
		try {
			properties = new Properties();
			// inputStream = new FileInputStream(getPropFile());
			inputStream = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE);

			if (inputStream != null) {
				properties.load(inputStream);
			}

			logger.debug(properties.toString());
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public void initialize() {
		/* do init here */
	}

	public void deInitialize() {
		/* do init here */
	}

	// public String getProperty(String property) {
	// return properties.getProperty(property);
	// }

	public String getHost(String serviceName) throws ConfigurationException {
		String hostName = null;
		if (serviceName.equalsIgnoreCase("PMDA")) {
			hostName = this.properties.getProperty("mes.pmda.host");
		} else if (serviceName.equalsIgnoreCase("WIP")) {
			hostName = this.properties.getProperty("mes.wip.host");
		} else if (serviceName.equalsIgnoreCase("MATERIAL")) {
			hostName = this.properties.getProperty("mes.material.host");
		} else if (serviceName.equalsIgnoreCase("CELL")) {
			hostName = this.properties.getProperty("mes.cell.host");
		} else if (serviceName.equalsIgnoreCase("SPC")) {
			hostName = this.properties.getProperty("mes.spc.host");
		} else if (serviceName.equalsIgnoreCase("OPC")) {
			hostName = this.properties.getProperty("mes.opc.host");
		} else if (serviceName.equalsIgnoreCase("NFM")) {
			hostName = this.properties.getProperty("cir.nfm.host");
		} else if (serviceName.equalsIgnoreCase("NFI")) {
			hostName = this.properties.getProperty("ism.nfi.host");
		} else {
			throw new ConfigurationException("Service name not registered");
		}
		return hostName;
	}

	public int getPort(String serviceName) throws ConfigurationException {
		String port = null;
		if (serviceName.equalsIgnoreCase("PMDA")) {
			port = this.properties.getProperty("mes.pmda.port");
		} else if (serviceName.equalsIgnoreCase("WIP")) {
			port = this.properties.getProperty("mes.wip.port");
		} else if (serviceName.equalsIgnoreCase("MATERIAL")) {
			port = this.properties.getProperty("mes.material.port");
		} else if (serviceName.equalsIgnoreCase("CELL")) {
			port = this.properties.getProperty("mes.cell.port");
		} else if (serviceName.equalsIgnoreCase("SPC")) {
			port = this.properties.getProperty("mes.spc.port");
		} else if (serviceName.equalsIgnoreCase("OPC")) {
			port = this.properties.getProperty("mes.opc.port");
		} else if (serviceName.equalsIgnoreCase("NFM")) {
			port = this.properties.getProperty("cir.nfm.port");
		} else if (serviceName.equalsIgnoreCase("NFI")) {
			port = this.properties.getProperty("ism.nfi.port");
		} else {
			throw new ConfigurationException("Service name not registered");
		}
		return Integer.parseInt(port);
	}

}
