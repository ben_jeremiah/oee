package com.cinovasi.oee.opc.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cinovasi.oee.core.constants.Tag;
import com.cinovasi.oee.opc.Worker;
import com.cinovasi.opcgw.api.beans.TagInfo;

public class CellRuntimeDataFacade {

// a class to temporarily contain the tagmapping from db, reduce traffic to db
// so the application can run faster.
    private int cellid;
    private String cellName;
    private Map<Tag, String> tagNameMap;

    
    public CellRuntimeDataFacade() {
        tagNameMap = new HashMap<Tag, String>();
    }

    public int getCellId() {
        return cellid;
    }

    public void setCellId(int id) {
        this.cellid = id;
    }

    public String getCellName() {
        return cellName;
    }

    public void setCellName(String name) {
        this.cellName = name;
    }

    public Map<Tag, String> getTagNameMap() {
        return tagNameMap;
    }

    public void setTagNameMap(Map<Tag, String> tagNameMap) {
        this.tagNameMap = tagNameMap;
    }


//  RUNNING(1, "RUNNING"),
    public Integer getRunning() {
        if(tagNameMap.get(Tag.RUNNING) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.RUNNING));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setRunning(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.RUNNING), val);
    }

//  DOWNTIME(2, "DOWNTIME"),
    public Integer getDowntime() {
        if(tagNameMap.get(Tag.DOWNTIME) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.DOWNTIME));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setDowntime(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.DOWNTIME), val);
    }

    //  IDLE(3, "IDLE"),
    public Integer getIdle() {
        if(tagNameMap.get(Tag.IDLE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.IDLE));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setIdle(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.IDLE), val);
    }

    //  EMERGENCY(4, "EMERGENCY"),
    public Integer getEmergency() {
        if(tagNameMap.get(Tag.EMERGENCY) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.EMERGENCY));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setEmergency(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.EMERGENCY), val);
    }

    //  PART_DELAY(5, "PART_DELAY"),
    public Integer getPartDelay() {
        if(tagNameMap.get(Tag.PART_DELAY) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.PART_DELAY));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setPartDelay(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.PART_DELAY), val);
    }

    //  COUNT_FINISH_GOOD(6, "COUNT_FINISH_GOOD"),
    public Integer getCountFinishGood() {
        if(tagNameMap.get(Tag.COUNT_FINISH_GOOD) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.COUNT_FINISH_GOOD));
            String val = tagInfo.getValue();
            return Integer.valueOf(val);
        }else{
            return -1;
        }

    }

    public void setCountFinishGood(Integer val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.COUNT_FINISH_GOOD), val);
    }

    //  OEE(7, "OEE"),
    public Double getOee() {
        if(tagNameMap.get(Tag.OEE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.OEE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setOee(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.OEE), val);
    }

    //  AVAILABILITY(8, "AVAILABILITY"),
    public Double getAvailability() {
        if(tagNameMap.get(Tag.AVAILABILITY) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.AVAILABILITY));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setAvailability(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.AVAILABILITY), val);
    }

    //  PERFORMANCE(9, "PERFORMANCE"),
    public Double getPerformance() {
        if(tagNameMap.get(Tag.PERFORMANCE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.PERFORMANCE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setPerformance(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.PERFORMANCE), val);
    }

    //  QUALITY(10, "QUALITY"),
    public Double getQuality() {
        if(tagNameMap.get(Tag.QUALITY) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.QUALITY));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setQuality(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.QUALITY), val);
    }

    //  BEARING_DEPTH(11, "BEARING_DEPTH"),
    public Double getBearingDepth() {
        if(tagNameMap.get(Tag.BEARING_DEPTH) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.BEARING_DEPTH));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return null;
        }

    }

    public void setBearingDepth(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.BEARING_DEPTH), val);
    }

    //  BARCODE_ENGINE(12, "BARCODE_ENGINE"),
    public Double getBarcodeEngine() {
        if(tagNameMap.get(Tag.BARCODE_ENGINE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.BARCODE_ENGINE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setBarcodeEngine(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.BARCODE_ENGINE), val);
    }

    //  PRESSURE_LOAD_CELL(13, "PRESSURE_LOAD_CELL"),
    public Double getPressureLoadCell() {
        if(tagNameMap.get(Tag.PRESSURE_LOAD_CELL) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.PRESSURE_LOAD_CELL));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setPressureLoadCell(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.PRESSURE_LOAD_CELL), val);
    }

    //  TORQUE_OK(14, "TORQUE_OK"),
    public Double getTorqueOK() {
        if(tagNameMap.get(Tag.TORQUE_OK) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.TORQUE_OK));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setTorqueOK(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.TORQUE_OK), val);
    }

    //  NUT_RUNNER_TORQUE(15, "NUT_RUNNER_TORQUE"),
    public Double getNutRunnerTorque() {
        if(tagNameMap.get(Tag.NUT_RUNNER_TORQUE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.NUT_RUNNER_TORQUE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setNutRunnerTorque(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.NUT_RUNNER_TORQUE), val);
    }

    //  NUT_RUNNER_ANGLE(16, "NUT_RUNNER_ANGLE"),
    public Double getNutRunnerAngle() {
        if(tagNameMap.get(Tag.NUT_RUNNER_ANGLE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.NUT_RUNNER_ANGLE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setNutRunnerAngle(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.NUT_RUNNER_ANGLE), val);
    }

    //  TIRE_PRESSURE(17, "TIRE_PRESSURE"),
    public Double getTirePressure() {
        if(tagNameMap.get(Tag.TIRE_PRESSURE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.TIRE_PRESSURE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setTirePressure(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.TIRE_PRESSURE), val);
    }

    //  TIGHTENING_OK(18, "TIGHTENING_OK"),
    public Double getTighteningOK() {
        if(tagNameMap.get(Tag.TIGHTENING_OK) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.TIGHTENING_OK));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setTighteningOK(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.TIGHTENING_OK), val);
    }

    //  BARCODE_FRAME(19, "BARCODE_FRAME"),
    public Double getBarcodeFrame() {
        if(tagNameMap.get(Tag.BARCODE_FRAME) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.BARCODE_FRAME));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setBarcodeFrame(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.BARCODE_FRAME), val);
    }

    //  AIR_PRESSURE(20, "AIR_PRESSURE"),
    public Double getAirPressure() {
        if(tagNameMap.get(Tag.AIR_PRESSURE) != null){
            TagInfo tagInfo = Worker.getInstance().readData(tagNameMap.get(Tag.AIR_PRESSURE));
            String val = tagInfo.getValue();
            return Double.valueOf(val);
        }else{
            return -1.0;
        }

    }

    public void setAirPressure(Double val) {
        Worker.getInstance().writeData(tagNameMap.get(Tag.AIR_PRESSURE), val);
    }
}