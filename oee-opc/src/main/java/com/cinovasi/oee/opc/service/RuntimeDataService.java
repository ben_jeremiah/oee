package com.cinovasi.oee.opc.service;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.CellTag;
import com.cinovasi.oee.core.service.CellService;
import com.cinovasi.oee.opc.config.ServiceConfiguration;
import com.cinovasi.opcgw.api.beans.TagInfo;
import com.cinovasi.opcgw.api.beans.TagInfos;
import com.cinovasi.opcgw.api.beans.TagList;
import com.cinovasi.opcgw.api.exception.MissingInformationException;
import com.cinovasi.opcgw.api.exception.ServiceExecutionException;
import com.cinovasi.opcgw.proxy.OPCGatewayServiceProxy;

public class RuntimeDataService implements Runnable {
    final Logger logger = LoggerFactory.getLogger(RuntimeDataService.class);
    private static RuntimeDataService INSTANCE;

    CellService cellService;

    private static String OPC_GROUP_NAME = "OEE_OPC";
    private OPCGatewayServiceProxy opcGatewayServiceProxy;
    private Vector<String> TAG_LIST;
    private Map<String, TagInfo> OPC_VALUE_CACHE;
    private ScheduledExecutorService executor;
    private int retryCountdown = 10;
    private boolean started;

    public static RuntimeDataService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RuntimeDataService();
        }
        return INSTANCE;
    }

    private RuntimeDataService() {
        init();
    }

    public TagInfo readData(String tagName) {
        return OPC_VALUE_CACHE.get(tagName);
    }

    public boolean writeData(String tagName, Object value) {
        // StopWatch sw = new StopWatch();
        // sw.start();
        try {
            synchronized (opcGatewayServiceProxy) {
                opcGatewayServiceProxy.write(OPC_GROUP_NAME, tagName, value);
                TagInfo tagInfo = OPC_VALUE_CACHE.get(tagName);
                if (tagInfo != null) {
                    tagInfo.setValue(value.toString());
                    OPC_VALUE_CACHE.put(tagName, tagInfo);
                }
            }
            return true;
        } catch (RemoteException | ServiceExecutionException | MissingInformationException e) {
            logger.error("Error while writeData", e);
            return false;
        } finally {
            // sw.stop();
            // logger.debug("writeData within {} ms", sw.getTime());
        }
    }

    private void init() {
        logger.debug("Runtime Data Service Initialization");
        executor = Executors.newScheduledThreadPool(1);

        try {

            TAG_LIST = new Vector<String>();

            // init tag list
            Collection<Cell> cells = cellService.getCells();
            for (Cell cell : cells) {
                for (CellTag cellTag : cell.getCellTags()) {
                    TAG_LIST.add(cellTag.getTagName());
                }
            }

            logger.debug("got {} tagnames", TAG_LIST.size());
            OPC_VALUE_CACHE = Collections.synchronizedMap(new HashMap<String, TagInfo>());

            /* prepare opc reader */
            logger.debug("init OPC proxy");
            String host = ServiceConfiguration.getInstance().getHost("OPC");
            int port = ServiceConfiguration.getInstance().getPort("OPC");

            if (opcGatewayServiceProxy == null) {
                opcGatewayServiceProxy = new OPCGatewayServiceProxy(host, port);
            }

            TagList tagList = new TagList();
            tagList.setTags(TAG_LIST);

            opcGatewayServiceProxy.stop(OPC_GROUP_NAME);
            opcGatewayServiceProxy.register(OPC_GROUP_NAME, tagList);
            opcGatewayServiceProxy.start(OPC_GROUP_NAME);
            logger.debug("init completed");

        } catch (Throwable t) {
            logger.error(t.getMessage(), t);
        }
    }

    @Override
    public void run() {
        try {
            synchronized (opcGatewayServiceProxy) {
                TagInfos tagInfos = opcGatewayServiceProxy.fetch(OPC_GROUP_NAME);

                if (tagInfos != null) {
                    Vector<TagInfo> tags = tagInfos.getTagInfos();
                    for (TagInfo tagInfo : tags) {
                        OPC_VALUE_CACHE.put(tagInfo.getTagName(), tagInfo);
                    }
                }
            }

        } catch (Throwable e) {
            retryCountdown--;
            logger.error("OPC Utility couldn't reach OPC Gateway: " + e.getMessage());
            if (retryCountdown <= 0) {
                logger.error("OPC Utility couldn't reach OPC Gateway after 10 attempts, restarting...");

                try {
                    TagList tagList = new TagList();
                    tagList.setTags(TAG_LIST);

                    opcGatewayServiceProxy.stop(OPC_GROUP_NAME);
                    opcGatewayServiceProxy.register(OPC_GROUP_NAME, tagList);
                    opcGatewayServiceProxy.start(OPC_GROUP_NAME);
                } catch (Throwable t) {
                    logger.error(t.getMessage(), t);
                }
                retryCountdown = 10;
            }
        }
    }

    public void initialize() {
        if (!started) {
            executor.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
            started = true;
        }
    }

    public void deInitialize() {
        synchronized (executor) {
            try {
                logger.debug("shutting down ...");
                executor.shutdown();
                // executor.wait();
                started = false;
                logger.debug("shutting down completed");
            } catch (Exception e) {
                logger.error("Error while deInitialize", e);
            }
        }
    }

}