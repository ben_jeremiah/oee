package com.cinovasi.oee.opc.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.sound.sampled.SourceDataLine;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.DowntimeRecord;
import com.cinovasi.oee.core.entities.UptimeRecord;
import com.cinovasi.oee.core.entities.OeeCalculationContext;
import com.cinovasi.oee.core.entities.ProcessDataType;
import com.cinovasi.oee.core.entities.QualityReport;
import com.cinovasi.oee.core.entities.TimeBetweenFailure;
import com.cinovasi.oee.core.entities.TimeToRepair;
import com.cinovasi.oee.core.service.DowntimeRecordService;
import com.cinovasi.oee.core.service.UptimeRecordService;
import com.cinovasi.oee.core.service.OeeCalculationContextService;
import com.cinovasi.oee.core.service.ProcessDataTypeService;
import com.cinovasi.oee.core.service.QualityReportService;
import com.cinovasi.oee.core.service.TimeBetweenFailureService;
import com.cinovasi.oee.core.service.TimeToRepairService;

public class CellRuntime {


	private static Logger logger = LoggerFactory.getLogger(CellRuntime.class);

	enum State {
		STANDBY,
		RUNNING,
		ENDING
	}

	private State state;
	private BundleContext bundleContext;
	private CellRuntimeDataFacade dataFacade;
	private Cell cell;
	private int cellId;
	private String cellName;
	private OeeCalculationContext currentOeeCtx;
	private DowntimeRecord currentDowntimeRecord;
	private UptimeRecord currentUptimeRecord;
	private boolean previousDowntimeNotFinished = false;
	private Long previousDowntimeId = (long) -1;
	private Integer previousReasonId;

	private boolean nullifyDowntimeRecordAfterCommit;
	private boolean nullifyUptimeRecordAfterCommit;
	private boolean nullifyOeeContextAfterCommit;
	// private boolean nullifyActiveWorkOrderAfterCommit;
	private int tickCommit = 15;
	private static int TICK_COMMIT_RELOAD = 15; 
	
	// cmdEndJob should gives stable reading for 3 consecutive ticks, 
	// then it would be considered as valid End Job
	private int endJobCount;
	// private static int END_JOB_COUNT_THRESHOLD = 3;

	private Date today = null;

	// Quality data trigger
	private Boolean recordedBearingDepth = false;
	private Boolean recordedBarcodeEngine = false;
	private Boolean recordedPressureLoadCell = false;
	private Boolean recordedTorqueOK = false;
	private Boolean recordedNutRunAngle = false;
	private Boolean recordedNutRunTorque = false;
	private Boolean recordedTirePressure = false;
	private Boolean recordedTighteningOK = false;
	private Boolean recordedBarcodeFrame = false;
	private Boolean recordedAirPressure = false;

	public CellRuntime() {
		state = State.STANDBY;
	}
	
	public void setBundleContext(BundleContext ctx)
	{
		bundleContext = ctx;
	}

	public void setCell(Cell val) {
		this.cell = val;
	}

	public Cell getCell() {
		return this.cell;
	}

	public void setCellId(int val) {
		this.cellId = val;
	}

	public int getCellId() {
		return this.cellId;
	}

	public void setCellName(String val) {
		this.cellName = val;
	}

	public String getCellName() {
		return this.cellName;
	}

	public void setDataFacade(CellRuntimeDataFacade val) {
		this.dataFacade = val;
	}

	public CellRuntimeDataFacade getDataFacade() {
		return this.dataFacade;
	}

	public void syncState() {
		// to recover from unexpected shutdown in the middle of WO execution
		try
		{
				boolean hasValidContext = false;

				ServiceReference<?> oeeCCSR = getOeeCalculationContextServiceReference();
				OeeCalculationContextService oeeCCService = getOeeCalculationContextService(oeeCCSR);
				OeeCalculationContext oeeCtx = oeeCCService.findUnfinishedCalculationByCell(cellId);
				
				if(oeeCtx != null)
				{
					this.today = oeeCtx.getStartTime();
					logger.info("[{}]Found valid previous OEE Calculation context : {}", cellId, oeeCtx.getId());
					hasValidContext = true;
				}
				
				if(hasValidContext)
				{
					this.currentOeeCtx = oeeCtx;
					// this.activeWorkOrder = wo;
					
					ServiceReference<?> downtimeRecordSR = getDowntimeRecordServiceReference();
					DowntimeRecordService downtimeRecordService = getDowntimeRecordService(downtimeRecordSR);
					
					ServiceReference<?> uptimeRecordSR = getUptimeRecordServiceReference();
					UptimeRecordService uptimeRecordService = getUptimeRecordService(uptimeRecordSR);
					
					DowntimeRecord dr= downtimeRecordService.findLatestRecordByContext(currentOeeCtx.getId());
					UptimeRecord ur= uptimeRecordService.findLatestRecordByContext(currentOeeCtx.getId());
					
					
					if(dr != null && dr.getEndTime() == null)
					{
						if(dr.getPreviousDowntimeId() != null){
							this.previousDowntimeId = dr.getPreviousDowntimeId();
						}
						this.currentDowntimeRecord = dr;
						logger.info("[{}]Found valid unfinished DowntimeRecord : {}", cellId, dr.getId());
					}

					if(ur != null && ur.getEndTime() == null)
					{
						this.currentUptimeRecord = ur;
						logger.info("[{}]Found valid unfinished UptimeRecord : {}", cellId, ur.getId());
					}

					currentOeeCtx.getOee();
					changeState(State.RUNNING);
				}
				else
				{
					logger.info("[{}]No valid Context found", cellId);
					changeState(State.STANDBY);
				}
		}
		catch(Exception exc)
		{
			logger.error(String.format("[%d]Error while syncState", cellId), exc);			
		}
	}

	
	private void startOeeCalculationContext()
	{
		startOeeCalculationContext(new Date());
	}

	private void startOeeCalculationContext(Date timePoint)
	{	
		if(timePoint == null)
			timePoint = new Date();

		try {
			ServiceReference<?> oeeCCSR = getOeeCalculationContextServiceReference();
			OeeCalculationContextService oeeCCService = getOeeCalculationContextService(oeeCCSR);
			OeeCalculationContext oeeCtx = oeeCCService.findUnfinishedCalculationByCell(cellId);
			if(oeeCtx != null)
			{
				// force unknown previous context to close
				oeeCtx.stop(timePoint);
				oeeCtx.getOee();
				oeeCtx.setUpdatedAt(timePoint);
				oeeCCService.updateOeeCalculationContext(oeeCtx);
			}

			oeeCtx = new OeeCalculationContext();
			oeeCtx.start(timePoint);
			oeeCtx.setCell(this.cell);
			//daily hours shift
			oeeCtx.setNetAvailableTime(24*60);
			oeeCCService.addOeeCalculationContext(oeeCtx);

			changeState(State.RUNNING);
			logger.info("[{}]Starting OEE Calculation Context", cellId);
			
			this.currentOeeCtx = oeeCtx;
			
		} catch (Exception e) {
			logger.error(String.format("[%d]Error while starting OEE Calculation Context", cellId), e);
		}
	}
	
	private void endOeeCalculationContext(){
		logger.info("[{}]Ending OEE Calculation Context", cellId);
		try
		{
			ServiceReference<?> downtimeRecordSR = getDowntimeRecordServiceReference();
			DowntimeRecordService downtimeRecordService = getDowntimeRecordService(downtimeRecordSR);
		
			ServiceReference<?> uptimeRecordSR = getUptimeRecordServiceReference();
			UptimeRecordService uptimeRecordService = getUptimeRecordService(uptimeRecordSR);
			
			// 8 hours shift
			double fixNat = (8 * 60);
			//double runNat = this.currentOeeCtx.getNetAvailableTime();
			/*if(runNat > fixNat){
				this.currentOeeCtx.setNetAvailableTime(fixNat);
			}else{
				this.currentOeeCtx.setNetAvailableTime(fixNat); // JM : why force to fixNat if actually less than initial plan?
			}*/
			
			Date now = new Date();
			double planned = this.currentOeeCtx.getPlannedDownTime();
			
			double nat = ((now.getTime() - this.currentOeeCtx.getStartTime().getTime()) / 1000.0 / 60.0) - planned;
			// this.currentOeeCtx.setNetAvailableTime(fixNat);
			this.currentOeeCtx.setNetAvailableTime(nat);
			// close if there's unfinished downtime
			if(this.currentDowntimeRecord != null)
			{
				this.previousDowntimeNotFinished = true;
				this.previousDowntimeId = this.currentDowntimeRecord.getId();

				this.currentDowntimeRecord = downtimeRecordService.getDowntimeRecord(this.currentDowntimeRecord.getId());
				closeDowntimeRecord(now);
				
				long duration = (now.getTime() - this.currentDowntimeRecord.getStartTime().getTime()) / 1000;
				int secDuration = (int) (duration & 0xFFFFFFFF);
				if(this.currentDowntimeRecord.getReason() != null){
					this.previousReasonId = this.currentDowntimeRecord.getReasonId();
				}
				this.currentDowntimeRecord.setDurationSecond(secDuration);
				
				downtimeRecordService.updateDowntimeRecord(this.currentDowntimeRecord);
			}

			if(this.currentUptimeRecord != null)
			{
				this.currentUptimeRecord = uptimeRecordService.getUptimeRecord(this.currentUptimeRecord.getId());
				closeUptimeRecord(now);
				
				long duration = (now.getTime() - this.currentUptimeRecord.getStartTime().getTime()) / 1000;
				// int secDuration = (int) (duration & 0xFFFFFFFF);
				this.currentUptimeRecord.setDurationSecond(duration);
				
				uptimeRecordService.updateUptimeRecord(this.currentUptimeRecord);
			}
			
			this.currentOeeCtx.stop(now);
			double not = ((now.getTime() - this.currentOeeCtx.getStartTime().getTime()) / 1000.0 / 60.0) - this.currentOeeCtx.getStopDuration();
			//this.currentOeeCtx.setNetOperatingTime(this.currentOeeCtx.getNetAvailableTime() - this.currentOeeCtx.getUnplannedDownTime());
			this.currentOeeCtx.setNetOperatingTime(not);
			this.currentOeeCtx.getOee();

			ServiceReference<?> oeeCCSR = getOeeCalculationContextServiceReference();
			OeeCalculationContextService oeeCCService = getOeeCalculationContextService(oeeCCSR);
			this.currentOeeCtx.setUpdatedAt(now);
			oeeCCService.updateOeeCalculationContext(this.currentOeeCtx);
			bundleContext.ungetService(oeeCCSR);
	
			changeState(State.ENDING);
			// dataFacade.setCmdEndJob(false);
			// dataFacade.setCmdResetCounter(true);
			// dataFacade.setCmdOperatorReject(false);
			// dataFacade.setCmdDowntimeReason(false);

			if(nullifyDowntimeRecordAfterCommit)
			{
				nullifyDowntimeRecordAfterCommit = false;
				this.currentDowntimeRecord = null;
			}
			
			nullifyOeeContextAfterCommit = true;
			bundleContext.ungetService(downtimeRecordSR);

		}
		catch(Exception e)
		{				
			logger.error(String.format("[%d]Error while ending OEE Calculation Context", cellId), e);
		}
	}

	private void changeState(State state) {
		this.state = state;
		logger.info("[{}]State = {}", cellId, state);
				
		switch(state)
		{
			case STANDBY:
				clearCalculatedValue();
				clearCommandFlag();
				nullifyDowntimeRecordAfterCommit = false;
				nullifyOeeContextAfterCommit = false;
				// nullifyActiveWorkOrderAfterCommit = false;
				currentOeeCtx = null;
				break;
			case RUNNING:
				// tickCommit = (int)(Math.random() * TICK_COMMIT_RELOAD);
				// if(tickCommit == 0) tickCommit = TICK_COMMIT_RELOAD;
				// endJobCount = END_JOB_COUNT_THRESHOLD;
				break;
			case ENDING:
				break;
			default:
				break;
		}
	}


	public void tick() {
		// WARNING : this function invoked from background thread !!
		try
		{
			synchronized (this) {
				switch(state){
					case STANDBY : tickStandby(); break;
					case RUNNING : tickRunning(); break;
					case ENDING : tickEnding(); break;
				}
			}
		}
		catch(Exception exc)
		{
			logger.error(String.format("Error while ticking Cell %d", this.cellId), exc);
		}
	}

	private void tickStandby()
	{
		// WARNING : this function invoked from background thread !!

		if(this.today == null){
			this.today = new Date();
			if(this.currentOeeCtx == null){
				startOeeCalculationContext();
			}
		}
		// consider to put anomaly handling to facilitate recovery to normal
	}
	
	private void tickRunning()
	{
		// WARNING : this function invoked from background thread !!

		//check if there is a date change
		Date now = new Date();

		// Calendar calendar = Calendar.getInstance();
		// calendar.set(2020, 01, 16, 59, 59, 59);
		// Date test = calendar.getTime();
		
		int dateMargin = getZeroTimeDate(now).compareTo(getZeroTimeDate(this.today));
		if(dateMargin == 1){
			if(this.currentOeeCtx != null){
				this.today = null;
				endOeeCalculationContext();
			}
		}else{
			boolean commitNow = false;
			if(tickCommit > 0)
			{
				tickCommit--;
				if(tickCommit == 0)
				{
					tickCommit = TICK_COMMIT_RELOAD;
					commitNow = true;
				}
			}
			
			try
			{
				if(this.currentOeeCtx == null){
					logger.info("[{}] Context is NULL",cellId);
				}
					
				handleDowntime();
				handleCounter();
				handleRunning();
				handleQualityData();
				
				currentOeeCtx.getOee(); // force recalculate
				// handleCommit();
				
				if(commitNow)
				{
					// JM : to write single variable to OPC could take approx. 20ms
					// using this technique to reduce processing load within single "tick"
					dataFacade.setOee(currentOeeCtx.getOee() * 100.0);
					dataFacade.setAvailability(currentOeeCtx.getAvailability() * 100.0);
					dataFacade.setPerformance(currentOeeCtx.getPerformance() * 100.0);
					dataFacade.setQuality(currentOeeCtx.getQuality() * 100.0);
					handleCommit();
				}
			}
			catch(Exception exc)
			{
				logger.error(String.format("[%d]Error in tickRunning", cellId), exc);
			}
		}
		
	}
	
	private void tickEnding()
	{
		// WARNING : this function invoked from background thread !!
		// if(dataFacade.getCounterTotal() == 0)
		// {
		// 	log.info("[{}]Counter reset to 0", cellId);
		// 	dataFacade.setCmdResetCounter(false);
			changeState(State.STANDBY);
		// }
	}

	@SuppressWarnings("unused")
	private void handleDowntime()
	{		
		// JM : this two objects need to be instantiated as lazy as possible	
		try
		{
			ServiceReference<?> downtimeRecordSR = getDowntimeRecordServiceReference();
			DowntimeRecordService downtimeRecordService = getDowntimeRecordService(downtimeRecordSR);

			ServiceReference<?> TBFSR = getTimeBetweenFailureServiceReference();
			TimeBetweenFailureService TBFService = getTimeBetweenFailureService(TBFSR);
			
			ServiceReference<?> TTRSR = getTimeToRepairServiceReference();
			TimeToRepairService TTRService = getTimeToRepairService(TTRSR);

			Date now = new Date();		
			double downtimeDuration = 0.0;
			double unplanned = 0.0;
			double planned = 0.0;
			double cummulativeDowntime = 0.0;
			
			if (dataFacade.getDowntime() == 1 || dataFacade.getIdle() == 1 ||
			dataFacade.getPartDelay() == 1 || dataFacade.getEmergency() == 1){ // currently DOWN 
				if(this.currentDowntimeRecord == null){
					logger.info("[{}]Machine DOWN", cellId);

					String tagTrigger = "";
					if(dataFacade.getDowntime() == 1){
						tagTrigger = "DOWNTIME";
					}else if(dataFacade.getIdle() == 1){
						tagTrigger = "IDLE";
					}else if(dataFacade.getPartDelay() == 1){
						tagTrigger = "PART DELAY";
					}else if(dataFacade.getEmergency() == 1){
						tagTrigger = "EMERGENCY";
					}

					if(dataFacade.getDowntime() == 1 || dataFacade.getEmergency() == 1){
						DowntimeRecord latestDowntime = downtimeRecordService.findLatestRecordByCell(this.cellId);
						if(latestDowntime != null){
							if(latestDowntime.getId() != this.previousDowntimeId){
								long duration = (now.getTime() - latestDowntime.getStartTime().getTime()) / 1000 / 60;
								int minuteDuration = (int) (duration & 0xFFFFFFFF);
								
								TimeBetweenFailure newTBF = new TimeBetweenFailure();
								newTBF.setTimestamp(now);
								newTBF.setValueMinute(minuteDuration);
								newTBF.setCellId(cellId);
								TBFService.addTimeBetweenFailure(newTBF);
							}
						}
					}

					DowntimeRecord newDowntimeRecord = new DowntimeRecord();
					newDowntimeRecord.setStartTime(now);								
					newDowntimeRecord.setCellId(cellId);
					newDowntimeRecord.setContext(this.currentOeeCtx);
					newDowntimeRecord.setTagTrigger(tagTrigger);

					//Handle change day but previous downtime is not finished
					if(this.previousDowntimeNotFinished){
						this.previousDowntimeNotFinished = false;
						newDowntimeRecord.setPreviousDowntimeId(this.previousDowntimeId);
						newDowntimeRecord.setReasonId(this.previousReasonId);
					}
					
					downtimeRecordService.saveDowntimeRecord(newDowntimeRecord);

					this.currentDowntimeRecord = newDowntimeRecord;
				} 
				
			// 	boolean cmdDowntimeReason = dataFacade.getCmdDowntimeReason();
			// 	if(cmdDowntimeReason){
			// 		String reasonCode = dataFacade.getDowntimeReasonCode().trim();
			// 		if(reasonCode != null && reasonCode.length() > 0){
			// 			log.info("[{}]Downtime reason : {}", cellId, reasonCode);
			// 			if(em == null) em = IsmPersistenceManager.getInstance().createEntityManager();
			// 			if(trx == null || !trx.isActive())
			// 			{
			// 				trx = em.getTransaction();
			// 				trx.begin();
			// 			}

			// 			StatusDao sd = new StatusDao(em);
			// 			Status status = sd.findByStatusCode(reasonCode);
			// 			downtimeRecord.setReason(status.getStatus());
			// 			em.merge(downtimeRecord);
	
			// 			dataFacade.setCmdDowntimeReason(false);
			// 			dataFacade.setDowntimeReasonCode("");
			// 			dataFacade.setDowntimeReasonText("");				
			// 		}
			// 	}
				
				this.currentDowntimeRecord = downtimeRecordService.getDowntimeRecord(this.currentDowntimeRecord.getId());

				unplanned = this.currentOeeCtx.getUnplannedDownTime();
				planned = this.currentOeeCtx.getPlannedDownTime();				
				downtimeDuration = (now.getTime() - this.currentDowntimeRecord.getStartTime().getTime()) / 1000.0 / 60.0;
				if(this.currentDowntimeRecord.getReason() != null){
					if(this.currentDowntimeRecord.getReason().getType() == 2){
						planned += downtimeDuration;
					}else{
						unplanned += downtimeDuration;
					}
				}else{
					unplanned += downtimeDuration;
				}

				
				long duration = (now.getTime() - this.currentDowntimeRecord.getStartTime().getTime()) / 1000;
				int secDuration = (int) (duration & 0xFFFFFFFF);
				this.currentDowntimeRecord.setDurationSecond(secDuration);

				downtimeRecordService.updateDowntimeRecord(this.currentDowntimeRecord);
				
				cummulativeDowntime = this.currentOeeCtx.getStopDuration() + downtimeDuration;
			}
			else if(dataFacade.getDowntime() == 0 && dataFacade.getIdle() == 0 &&
			dataFacade.getPartDelay() == 0 && dataFacade.getEmergency() == 0)
			{
				// currently UP
				if(this.currentDowntimeRecord != null) {
					logger.info("[{}]Machine UP", cellId);
					this.previousDowntimeNotFinished = false;
					this.previousDowntimeId = (long) -1;

					if(dataFacade.getDowntime() == 0 && dataFacade.getEmergency() == 0){
						DowntimeRecord latestDowntime = downtimeRecordService.findLatestRecordByCell(this.cellId);
						if(latestDowntime != null){
							long duration = (now.getTime() - latestDowntime.getStartTime().getTime()) / 1000 / 60;
							int minuteDuration = (int) (duration & 0xFFFFFFFF);
		
							TimeToRepair newTTR = new TimeToRepair();
							newTTR.setTimestamp(now);
							newTTR.setValueMinute(minuteDuration);
							newTTR.setCellId(cellId);
							TTRService.addTimeToRepair(newTTR);
						}
					}

					this.currentDowntimeRecord = downtimeRecordService.getDowntimeRecord(this.currentDowntimeRecord.getId());
					closeDowntimeRecord(now);

					long duration = (now.getTime() - this.currentDowntimeRecord.getStartTime().getTime()) / 1000;
					int secDuration = (int) (duration & 0xFFFFFFFF);
					this.currentDowntimeRecord.setDurationSecond(secDuration);

					downtimeRecordService.updateDowntimeRecord(this.currentDowntimeRecord);
				}
				unplanned = this.currentOeeCtx.getUnplannedDownTime();
				planned = this.currentOeeCtx.getPlannedDownTime();
				cummulativeDowntime = this.currentOeeCtx.getStopDuration();
			}
	
			if(dataFacade.getDowntime() == 1 || dataFacade.getIdle() == 1 ||
			dataFacade.getPartDelay() == 1 || dataFacade.getEmergency() == 1 || dataFacade.getRunning() == 1){
				//NAT & NOT						
				double nat = ((now.getTime() - this.currentOeeCtx.getStartTime().getTime()) / 1000.0 / 60.0) - planned;
				double not = ((now.getTime() - this.currentOeeCtx.getStartTime().getTime()) / 1000.0 / 60.0) - cummulativeDowntime;
				this.currentOeeCtx.setNetAvailableTime(nat);
				this.currentOeeCtx.setNetOperatingTime(not);
				double notOpc = Double.parseDouble(String.format("%.2f", not));
				double downtimeDurationOpc = Double.parseDouble(String.format("%.2f", cummulativeDowntime)); 
				// dataFacade.setNetOperatingTimeDuration(notOpc);
				// dataFacade.setDowntimeDuration(downtimeDurationOpc);
				
				this.currentOeeCtx.getOee(); // force recalculate
			}
			
			if(nullifyDowntimeRecordAfterCommit)
			{
				nullifyDowntimeRecordAfterCommit = false;
				this.currentDowntimeRecord = null;
			}
			bundleContext.ungetService(downtimeRecordSR);
		}
		catch(Exception exc)
		{
			logger.error(String.format("[%d]Error in handling Downtime", cellId), exc);
		}
	}
	
	private void handleCounter()
	{
		// Total Counter
		int counterTotal = dataFacade.getCountFinishGood();
		if(counterTotal > -1){
			this.currentOeeCtx.setTotalCount(counterTotal);
		}else{
			counterTotal = -1;
			this.currentOeeCtx.setTotalCount(-1);
		}
		
		// //Reject Counter
		// int rejectOperatorValue = dataFacade.getCounterOperatorReject();
		// if(rejectOperatorValue != 0){
		// 	int reject = this.currentOeeCtx.getBadCount() + rejectOperatorValue;
		// 	this.currentOeeCtx.setBadCount(reject);
		// 	dataFacade.setCounterCummulativeReject(reject);
		// 	dataFacade.setCounterOperatorReject(0);
		// }
		
		//Good Counter
		// int counterGood = counterTotal - this.currentOeeCtx.getBadCount();
		int counterGood = counterTotal;
		this.currentOeeCtx.setGoodCount(counterGood);
		// dataFacade.setCounterGood(counterGood);
		
		this.currentOeeCtx.getOee(); // force recalculate
	}
	
	private void handleCommit()
	{
		ServiceReference<?> oeeCCSR = getOeeCalculationContextServiceReference();
		OeeCalculationContextService oeeCCService = getOeeCalculationContextService(oeeCCSR);
		Date now = new Date();		
		try
		{
			if(this.currentOeeCtx != null) {
				this.currentOeeCtx.setUpdatedAt(now);
				oeeCCService.updateOeeCalculationContext(this.currentOeeCtx);
			}
				
			// if(downtimeRecord != null) { em.merge(downtimeRecord); }
			
			if(nullifyOeeContextAfterCommit)
			{
				nullifyOeeContextAfterCommit = false;
				this.currentOeeCtx = null;
				logger.info("Context is now NULL");
			}
			
			// if(nullifyDowntimeRecordAfterCommit)
			// {
			// 	nullifyDowntimeRecordAfterCommit = false;
			// 	downtimeRecord = null;
			// }
		}
		catch(Exception exc)
		{
			logger.error(String.format("[%d]Error in handling Commit", cellId), exc);
		}
	}

	private void handleRunning(){
		try
		{
			ServiceReference<?> uptimeRecordSR = getUptimeRecordServiceReference();
			UptimeRecordService uptimeRecordService = getUptimeRecordService(uptimeRecordSR);

			Date now = new Date();	
			if (dataFacade.getRunning() == 1){ // currently UP 
				if(this.currentUptimeRecord == null){
					logger.info("[{}]Machine Running", cellId);

					UptimeRecord newUptimeRecord = new UptimeRecord();
					newUptimeRecord.setStartTime(now);								
					newUptimeRecord.setCellId(cellId);
					newUptimeRecord.setContext(this.currentOeeCtx);
					uptimeRecordService.saveUptimeRecord(newUptimeRecord);

					this.currentUptimeRecord = newUptimeRecord;
				} 
				
				long duration = (now.getTime() - this.currentUptimeRecord.getStartTime().getTime()) / 1000;
				this.currentUptimeRecord = uptimeRecordService.getUptimeRecord(this.currentUptimeRecord.getId());
				this.currentUptimeRecord.setDurationSecond(duration);

				uptimeRecordService.updateUptimeRecord(this.currentUptimeRecord);			
				
			}
			else if(dataFacade.getRunning() == 0)
			{
				// currently not running
				if(this.currentUptimeRecord != null) {
					logger.info("[{}]Machine Not Running", cellId);
					this.currentUptimeRecord = uptimeRecordService.getUptimeRecord(this.currentUptimeRecord.getId());
					closeUptimeRecord(now);

					long duration = (now.getTime() - this.currentUptimeRecord.getStartTime().getTime()) / 1000;
					this.currentUptimeRecord.setDurationSecond(duration);

					uptimeRecordService.updateUptimeRecord(this.currentUptimeRecord);
				}
			}
	
			if(nullifyUptimeRecordAfterCommit)
			{
				nullifyUptimeRecordAfterCommit = false;
				this.currentUptimeRecord = null;
			}
			bundleContext.ungetService(uptimeRecordSR);
		}
		catch(Exception exc)
		{
			logger.error(String.format("[%d]Error in handling Uptime (Running)", cellId), exc);
		}
	}

	private void handleQualityData(){
		try
		{
			ServiceReference<?> processDataTypeSR = getProcessDataTypeServiceReference();
			ProcessDataTypeService processDataTypeService = getProcessDataTypeService(processDataTypeSR);

			ServiceReference<?> qualityReportSR = getQualityReportServiceReference();
			QualityReportService qualityReportService = getQualityReportService(qualityReportSR);

			Date now = new Date();
			Collection<ProcessDataType> processDataTypes = processDataTypeService.getProcessDataTypesByCell(this.cellId);
			for (ProcessDataType processDataType : processDataTypes) {
				if(processDataType.getName().equals("Bearing Depth")){
					if(dataFacade.getBearingDepth() != null){
						Double val = dataFacade.getBearingDepth();
						if(val > 0 && !this.recordedBearingDepth){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Bearing Depth)", cellId);

							//stop record until value back to 0
							this.recordedBearingDepth = true;
						}else if(val <= 0 && this.recordedBearingDepth){
							//Return 0 (so runtime can record next item)
							this.recordedBearingDepth = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Bearing Depth)", cellId);
					}
				
				}else if(processDataType.getName().equals("Barcode Engine")){
					if(dataFacade.getBarcodeEngine() != null){
						Double val = dataFacade.getBarcodeEngine();
						if(val > 0 && !this.recordedBarcodeEngine){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Barcode Engine)", cellId);

							//stop record until value back to 0
							this.recordedBarcodeEngine = true;
						}else if(val <= 0 && this.recordedBarcodeEngine){
							//Return 0 (so runtime can record next item)
							this.recordedBarcodeEngine = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Barcode Engine)", cellId);
					}
				}else if(processDataType.getName().equals("Pressure Load Cell")){
					if(dataFacade.getPressureLoadCell() != null){
						Double val = dataFacade.getPressureLoadCell();
						if(val > 0 && !this.recordedPressureLoadCell){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Pressure Load Cell)", cellId);

							//stop record until value back to 0
							this.recordedPressureLoadCell = true;
						}else if(val <= 0 && this.recordedPressureLoadCell){
							//Return 0 (so runtime can record next item)
							this.recordedPressureLoadCell = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Pressure Load Cell)", cellId);
					}
				}else if(processDataType.getName().equals("Torque OK")){
					if(dataFacade.getTorqueOK() != null){
						Double val = dataFacade.getTorqueOK();
						if(val > 0 && !this.recordedTorqueOK){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Torque OK)", cellId);

							//stop record until value back to 0
							this.recordedTorqueOK = true;
						}else if(val <= 0 && this.recordedTorqueOK){
							//Return 0 (so runtime can record next item)
							this.recordedTorqueOK = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Torque OK)", cellId);
					}
				}else if(processDataType.getName().equals("Nut Runner Angle")){
					if(dataFacade.getNutRunnerAngle() != null){
						Double val = dataFacade.getNutRunnerAngle();
						if(val > 0 && !this.recordedNutRunAngle){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Nut Runner Angle)", cellId);

							//stop record until value back to 0
							this.recordedNutRunAngle = true;
						}else if(val <= 0 && this.recordedNutRunAngle){
							//Return 0 (so runtime can record next item)
							this.recordedNutRunAngle = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Nut Runner Angle)", cellId);
					}
				}else if(processDataType.getName().equals("Nut Runner Torque")){
					if(dataFacade.getNutRunnerTorque() != null){
						Double val = dataFacade.getNutRunnerTorque();
						if(val > 0 && !this.recordedNutRunTorque){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());

							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Nut Runner Torque)", cellId);

							//stop record until value back to 0
							this.recordedNutRunTorque = true;
						}else if(val <= 0 && this.recordedNutRunTorque){
							//Return 0 (so runtime can record next item)
							this.recordedNutRunTorque = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Nut Runner Torque)", cellId);
					}
				}else if(processDataType.getName().equals("Tire Pressure")){
					if(dataFacade.getTirePressure() != null){
						Double val = dataFacade.getTirePressure();
						if(val > 0 && !this.recordedTirePressure){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Tire Pressure)", cellId);

							//stop record until value back to 0
							this.recordedTirePressure = true;
						}else if(val <= 0 && this.recordedTirePressure){
							//Return 0 (so runtime can record next item)
							this.recordedTirePressure = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Tire Pressure)", cellId);
					}
				}else if(processDataType.getName().equals("Tightening OK/NG")){
					if(dataFacade.getTighteningOK() != null){
						Double val = dataFacade.getTighteningOK();
						if(val > 0 ){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Tightening OK/NG)", cellId);

							//stop record until value back to 0
							this.recordedTighteningOK = true;
						}else if(val <= 0 && this.recordedTighteningOK){
							//Return 0 (so runtime can record next item)
							this.recordedTighteningOK = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Tightening OK/NG)", cellId);
					}
				}else if(processDataType.getName().equals("Barcode Frame")){
					if(dataFacade.getBarcodeFrame() != null){
						Double val = dataFacade.getBarcodeFrame();
						if(val > 0 && !this.recordedBarcodeFrame){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Barcode Frame)", cellId);

							//stop record until value back to 0
							this.recordedBarcodeFrame = true;
						}else if(val <= 0 && this.recordedBarcodeFrame){
							//Return 0 (so runtime can record next item)
							this.recordedBarcodeFrame = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Barcode Frame)", cellId);
					}
				}else if(processDataType.getName().equals("Air Pressure")){
					if(dataFacade.getAirPressure() != null){
						Double val = dataFacade.getAirPressure();
						if(val > 0 && !this.recordedAirPressure){
							QualityReport newQualityReport = new QualityReport();
							newQualityReport.setTimestamp(now);								
							newQualityReport.setProcessDataTypeId(processDataType.getId());
							newQualityReport.setValue(val);
							QualityReport prevReport = qualityReportService.findPreviousReport(processDataType.getId());
							if(prevReport != null){
								newQualityReport.setValueDifference(Math.abs(prevReport.getValue()-val));
							}else{
								newQualityReport.setValueDifference(0.0);
							}
							qualityReportService.addQualityReport(newQualityReport);
							logger.info("[{}] Quality Report Recorded (Air Pressure)", cellId);

							//stop record until value back to 0
							this.recordedAirPressure = true;
						}else if(val <= 0 && this.recordedAirPressure){
							//Return 0 (so runtime can record next item)
							this.recordedAirPressure = false;
						}
					}else{
						logger.info("[{}] Data Facade return null (Air Pressure)", cellId);
					}
				}else{
					logger.info("[{}] Process Data Type is not registered in Data Facade", cellId);
				}
			}
		}
		catch(Exception exc)
		{
			logger.error(String.format("[%d]Error in handling Quality Data", cellId), exc);
		}
	}


	private void clearCalculatedValue()
	{
		// dataFacade.setCurrentOeePercentage(0);
		// dataFacade.setCurrentAvailibilityPercentage(0);
		// dataFacade.setCurrentPerformancePercentage(0);
		// dataFacade.setCurrentQualityPercentage(0);
		
		// dataFacade.setCounterGood(0);
		// dataFacade.setCounterCummulativeReject(0);
		
		// dataFacade.setNetOperatingTimeDuration(0);
		// dataFacade.setDowntimeDuration(0);
		// dataFacade.setCurrentWorkOrder("");
	}

	private void clearCommandFlag()
	{
		// dataFacade.setCmdEndJob(false);
		// dataFacade.setCmdResetCounter(false);
		// dataFacade.setCmdOperatorReject(false);
		// dataFacade.setCmdDowntimeReason(false);
	}

	
	private void closeDowntimeRecord(Date endTime)
	{
		//Update OEE CTX
		ServiceReference<?> oeeCCSR = getOeeCalculationContextServiceReference();
		OeeCalculationContextService oeeCCService = getOeeCalculationContextService(oeeCCSR);
		OeeCalculationContext updatedCtx = oeeCCService.findById(this.currentOeeCtx.getId());
		this.currentOeeCtx.setUnplannedDownTime(updatedCtx.getUnplannedDownTime());
		this.currentOeeCtx.setPlannedDownTime(updatedCtx.getPlannedDownTime());
		this.currentOeeCtx.getStopDuration();
		
		this.currentDowntimeRecord.setEndTime(endTime);
		double downtimeDuration = (this.currentDowntimeRecord.getEndTime().getTime() - this.currentDowntimeRecord.getStartTime().getTime()) / 1000.0 / 60.0;
		if(this.currentDowntimeRecord.getReason() != null){
			if (this.currentDowntimeRecord.getReason().getType() == 2) {
				double plannedDowntime = this.currentOeeCtx.getPlannedDownTime();
				plannedDowntime += downtimeDuration;
				this.currentOeeCtx.setPlannedDownTime(plannedDowntime);
			} else {
				double unplannedDowntime = this.currentOeeCtx.getUnplannedDownTime();
				unplannedDowntime += downtimeDuration;
				this.currentOeeCtx.setUnplannedDownTime(unplannedDowntime);
			}
		}else{
			double unplannedDowntime = this.currentOeeCtx.getUnplannedDownTime();
			unplannedDowntime += downtimeDuration;
			this.currentOeeCtx.setUnplannedDownTime(unplannedDowntime);
		}

		oeeCCService.updateOeeCalculationContext(this.currentOeeCtx);
		nullifyDowntimeRecordAfterCommit = true;
	}

	private void closeUptimeRecord(Date endTime)
	{
		this.currentUptimeRecord.setEndTime(endTime);
		// double uptimeDuration = (this.currentUptimeRecord.getEndTime().getTime() - this.currentUptimeRecord.getStartTime().getTime()) / 1000.0 / 60.0;
		nullifyUptimeRecordAfterCommit = true;
	}
	

	

	// <-------------------------------- SERVICE REFERENCE SECTION -------------------------------->
	private ServiceReference<?> getOeeCalculationContextServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(OeeCalculationContextService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private OeeCalculationContextService getOeeCalculationContextService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			OeeCalculationContextService dao = (OeeCalculationContextService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getDowntimeRecordServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(DowntimeRecordService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private DowntimeRecordService getDowntimeRecordService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			DowntimeRecordService dao = (DowntimeRecordService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getUptimeRecordServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(UptimeRecordService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private UptimeRecordService getUptimeRecordService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			UptimeRecordService dao = (UptimeRecordService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getTimeBetweenFailureServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(TimeBetweenFailureService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private TimeBetweenFailureService getTimeBetweenFailureService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			TimeBetweenFailureService dao = (TimeBetweenFailureService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getTimeToRepairServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(TimeToRepairService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private TimeToRepairService getTimeToRepairService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			TimeToRepairService dao = (TimeToRepairService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getProcessDataTypeServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(ProcessDataTypeService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private ProcessDataTypeService getProcessDataTypeService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			ProcessDataTypeService dao = (ProcessDataTypeService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}

	private ServiceReference<?> getQualityReportServiceReference()
	{
		int retry = 30;
		while (retry > 0) {
			ServiceReference<?> ref = bundleContext.getServiceReference(QualityReportService.class.getName());
			if (ref == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			}
			else return ref;
		}
		return null;
	}
	
	private QualityReportService getQualityReportService(ServiceReference<?> reference)
	{
		int retry = 30;
		while (retry > 0) {
			QualityReportService dao = (QualityReportService) bundleContext.getService(reference);
			if (dao == null) {
				try {
					retry--;
					Thread.sleep(1000);
				} catch (InterruptedException exc) {
					break;
				}
				continue;
			} else
				return dao;
		}
		return null;
	}


	// <-------------------------------- ETC FUNCTION SECTION -------------------------------->
	private Date getZeroTimeDate(Date fecha) {
		Date res = fecha;
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime( fecha );
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		res = calendar.getTime();
		return res;
	}
	
}
