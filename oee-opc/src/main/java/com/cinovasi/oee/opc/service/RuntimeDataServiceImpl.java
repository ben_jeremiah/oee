package com.cinovasi.oee.opc.service;

import java.util.Collection;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.RuntimeDataDTO;
import com.cinovasi.oee.core.service.RuntimeDataService;
import com.cinovasi.oee.opc.Worker;

@Bean
@Service
public class RuntimeDataServiceImpl implements RuntimeDataService{
    static final Logger logger = LoggerFactory.getLogger(RuntimeDataServiceImpl.class);
	
	@Override
	public Collection<RuntimeDataDTO> getData() {
		return Worker.getInstance().getCacheList();
	}

}
