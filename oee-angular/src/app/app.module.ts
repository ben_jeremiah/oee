import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// dimsup: add datatables module
import {DataTablesModule} from 'angular-datatables';

import {JwtInterceptor} from './utils/jwt.interceptor';
import {ErrorInterceptor} from './utils/error.interceptor';
import {MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule} from '@angular/material'; 
import {MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import {DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { ChartsModule } from 'ng2-charts';
import { NgSelectModule } from '@ng-select/ng-select';

import {MatMomentDateModule, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import {AppComponent} from './app.component';
import {EmployeeGridComponent} from './components/employee-grid/employee-grid.component';
import {EmployeeFormComponent} from './components/employee-form/employee-form.component';
import {RoleGridComponent} from './components/role-grid/role-grid.component';
import {RoleFormComponent} from './components/role-form/role-form.component';
import {ReasonGridComponent} from './components/reason-grid/reason-grid.component';
import {ReasonFormComponent} from './components/reason-form/reason-form.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {LoginComponent} from './components/login/login.component';
import {AlertComponent} from './components/alert/alert.component';
import {DowntimeRecordGridComponent} from './components/downtime-record-grid/downtime-record-grid.component';
import {DowntimeRecordFilterComponent} from './components/downtime-record-grid/downtime-record-filter.component';
import { OverallSummaryComponent } from './components/overall-summary/overall-summary.component';
import { MachinesSummaryComponent } from './components/machines-summary/machines-summary.component';
import { OeeReportGridComponent } from './components/oee-report-grid/oee-report-grid.component';
import { TraceReportGridComponent } from './components/trace-report-grid/trace-report-grid.component';
import { QualityReportGridComponent } from './components/quality-report-grid/quality-report-grid.component';
import { ShiftGridComponent } from './components/shift-grid/shift-grid.component';
import { ShiftFormComponent } from './components/shift-form/shift-form.component';

@NgModule({
    declarations: [
        AppComponent,
        EmployeeGridComponent,
        EmployeeFormComponent,
        RoleGridComponent,
        RoleFormComponent,
        ReasonGridComponent,
        ReasonFormComponent,
        SidebarComponent,
        LoginComponent,
        AlertComponent,
        DowntimeRecordGridComponent,
        DowntimeRecordFilterComponent,
        OverallSummaryComponent,
        MachinesSummaryComponent,
        OeeReportGridComponent,
        TraceReportGridComponent,
        QualityReportGridComponent,
        ShiftGridComponent,
        ShiftFormComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        DataTablesModule,
        MatDatepickerModule,        // <----- import(must)
        // MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatMomentDateModule,
        ChartsModule,
        NgSelectModule,
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
