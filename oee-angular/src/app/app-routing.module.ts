import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { EmployeeGridComponent } from './components/employee-grid/employee-grid.component';
import { EmployeeFormComponent } from './components/employee-form/employee-form.component';
import { RoleGridComponent } from './components/role-grid/role-grid.component';
import { RoleFormComponent } from './components/role-form/role-form.component';
import { ReasonGridComponent } from './components/reason-grid/reason-grid.component';
import { ReasonFormComponent } from './components/reason-form/reason-form.component';
import { DowntimeRecordGridComponent } from './components/downtime-record-grid/downtime-record-grid.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './utils/auth.guard';
import { AdminAuthGuard } from './utils/admin-auth.guard';
import { OverallSummaryComponent } from './components/overall-summary/overall-summary.component';
import { MachinesSummaryComponent } from './components/machines-summary/machines-summary.component';
import { OeeReportGridComponent } from './components/oee-report-grid/oee-report-grid.component';
import { TraceReportGridComponent } from './components/trace-report-grid/trace-report-grid.component';
import { QualityReportGridComponent } from './components/quality-report-grid/quality-report-grid.component';
import { ShiftGridComponent } from './components/shift-grid/shift-grid.component';
import { ShiftFormComponent } from './components/shift-form/shift-form.component';


// import { AppComponent } from './app.component';
// routes definition
const routes: Routes = [
    {path: '', redirectTo: '/', pathMatch: 'full', canActivate: [AuthGuard]},
    { path: '', component: OverallSummaryComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'employees/authenticate', component: EmployeeGridComponent },
    // { path: 'register', component: RegisterComponent },
    { path: 'employees', component: EmployeeGridComponent, canActivate: [AuthGuard,AdminAuthGuard]  },
    { path: 'employees/:id', component: EmployeeFormComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'employees/create', component: EmployeeFormComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'roles', component: RoleGridComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'roles/:id', component: RoleFormComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'reasons', component: ReasonGridComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'reasons/:id', component: ReasonFormComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'downtimeRecords', component: DowntimeRecordGridComponent, canActivate: [AuthGuard] },
    { path: 'machinesSummary', component: MachinesSummaryComponent, canActivate: [AuthGuard] },
    { path: 'oeeReport', component: OeeReportGridComponent, canActivate: [AuthGuard] },
    { path: 'traceReport', component: TraceReportGridComponent, canActivate: [AuthGuard] },
    { path: 'qualityReport', component: QualityReportGridComponent, canActivate: [AuthGuard] },
    { path: 'shifts', component: ShiftGridComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    { path: 'shifts/:id', component: ShiftFormComponent, canActivate: [AuthGuard,AdminAuthGuard] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
    // dimsup: support for deep links routing
    constructor(private router: Router) {
        this.router.errorHandler = (error: any) => {
            // console.log('url not found');
            this.router.navigate(['']);
        }
    }
}
