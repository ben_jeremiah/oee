import { Component, OnInit, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import * as moment from 'moment';
import { MAT_DATE_FORMATS } from '@angular/material';
import { MY_FORMATS } from '../downtime-record-grid/downtime-record-filter.component';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { CellService } from 'src/app/services/cell.service';
import { Cell } from 'src/app/model/cell';
import { WeeklyOeeReportViewService } from 'src/app/services/weekly-oee-report-view.service';
import { WeeklyDowntimeReportViewService } from 'src/app/services/weekly-downtime-report-view.service';
import { MonthlyOeeReportViewService } from 'src/app/services/monthly-oee-report-view.service';
import { OeeReport } from 'src/app/model/oee-report';
import { DowntimeReport } from 'src/app/model/downtime-report';
import { MonthlyDowntimeReportViewService } from 'src/app/services/monthly-downtime-report-view.service';
import { DailyOeeReportViewService } from 'src/app/services/daily-oee-report-view.service';
import { DailyDowntimeReportViewService } from 'src/app/services/daily-downtime-report-view.service';
import { ReasonService } from 'src/app/services/reason.service';
import { Reason } from 'src/app/model/reason';
import { TimeBetweenFailureService } from 'src/app/services/time-between-failure.service';
import { TimeToRepairService } from 'src/app/services/time-to-repair.service';
import * as pluginAnnotationsTBF from 'chartjs-plugin-annotation';
import * as pluginAnnotationsTTR from 'chartjs-plugin-annotation';
import { DailyLineDowntimeReportViewService } from 'src/app/services/daily-line-downtime-report-view.service';
import { WeeklyLineDowntimeReportViewService } from 'src/app/services/weekly-line-downtime-report-view.service';
import { MonthlyLineDowntimeReportViewService } from 'src/app/services/monthly-line-downtime-report-view.service';

@Component({
    selector: 'app-oee-report-grid',
    templateUrl: './oee-report-grid.component.html',
    styleUrls: ['./oee-report-grid.component.css'],
    providers: [
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ],
})

export class OeeReportGridComponent implements OnInit {

    //CHART
    public lineChartDataTBF: ChartDataSets[] = [{ data : []
    }];
    public lineChartDataTTR: ChartDataSets[] = [{ data : []
    }];
    
    public lineChartOptionsTBF: (ChartOptions & { annotation: any }) = {
        responsive: true,
        maintainAspectRatio : false,
        elements: { 
            point: 
            {
                radius: 0,
                hitRadius: 0,
                hoverRadius: 0,
                hoverBorderWidth: 0
            }
        },
        scales: {
          // We use this empty structure as a placeholder for dynamic theming.
          xAxes: [{
            type: 'time',
            distribution: 'series',
            time :{
              unit:'day',
            },
            display : false
          }],
          yAxes: [
            {
              id: 'y-axis-TBF',
              position: 'left',
            },
          ]
        },
        annotation: {
          annotations: [{
            type: 'line',
            mode: 'horizontal',
            scaleID: 'y-axis-TBF',
            value: 0,
            borderColor: 'blue',
            borderWidth: 2,
            label: {
              enabled: false,
              content: 'MTBF'
            }
          }]
        },
        tooltips: {
          callbacks: {
              label: function(tooltipItem, data) {
                  var value = tooltipItem.yLabel;
                 return value+" hour(s)";
              },

              title: function(tooltipItem, data){
                let formatted = moment(tooltipItem[0].xLabel).format('DD-MM-YYYY HH:MM:SS');
                return formatted;
              }
          } // end callbacks:
      }, //end tooltips
    };
    public lineChartOptionsTTR: (ChartOptions & { annotation: any }) = {
      responsive: true,
      maintainAspectRatio : false,
      elements: { 
        point: 
        {
            radius: 0,
            hitRadius: 0,
            hoverRadius: 0,
            hoverBorderWidth: 0
        }
      },
      scales: {
        // We use this empty structure as a placeholder for dynamic theming.
        xAxes: [{
          type: 'time',
          distribution: 'series',
          time :{
            unit:'day',
          },
          display : false,
        }],
        yAxes: [
          {
            id: 'y-axis-TTR',
            position: 'left',
          },
        ]
      },
      annotation: {
        annotations: [{
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-TTR',
          value: 0,
          borderColor: 'blue',
          borderWidth: 2,
          label: {
            enabled: false,
            content: 'MTTR'
          }
        }]
      },
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
                var value = tooltipItem.yLabel;
               return value+" hour(s)";
            },

            title: function(tooltipItem, data){
              let formatted = moment(tooltipItem[0].xLabel).format('DD-MM-YYYY HH:MM:SS');
              return formatted;
            }
        } // end callbacks:
    }, //end tooltips
  };
    public lineChartColors: Color[] = [
      { // red
        backgroundColor: 'transparent',
        borderColor: 'black',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'black'
      }
    ];
    public lineChartLegend = false;
    public lineChartTypeTBF = 'line';
    public lineChartTypeTTR = 'line';
      
    public lineChartPluginsTBF = [pluginAnnotationsTBF];
    public lineChartPluginsTTR = [pluginAnnotationsTTR];
    
    @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
      
    constructor(
        private cellService: CellService,
        private reasonService: ReasonService,
        private dailyOeeReportService : DailyOeeReportViewService,
        private dailyDowntimeReportService : DailyDowntimeReportViewService,
        private weeklyOeeReportService : WeeklyOeeReportViewService,
        private monthlyOeeReportService : MonthlyOeeReportViewService,
        private weeklyDowntimeReportService : WeeklyDowntimeReportViewService,
        private monthlyDowntimeReportService : MonthlyDowntimeReportViewService,
        
        private dailyLineDowntimeReportService : DailyLineDowntimeReportViewService,
        private weeklyLineDowntimeReportService : WeeklyLineDowntimeReportViewService,
        private monthlyLineDowntimeReportService : MonthlyLineDowntimeReportViewService,

        private TBFService : TimeBetweenFailureService,
        private TTRService : TimeToRepairService,
        ) { }

    user : Employee;

    oee : any;
    availability : any;
    quality : any;
    performance : any;

    entity = null;
    entityType = null;
    timePeriodic = 'daily';
    loading = true;
    weekYear = null;
    monthYear = null;
    date = moment();
    noResultFoundOee = true;
    updatedAtString = null;
    lineRepresentative = null;

    entityTypeOptions = [
      {
        id: 'line',
        text: 'Line'
      },
      {
        id: 'machine',
        text: 'Machine'
      }
    ];
    timePeriodicOptions = [
      {
        id: 'monthly',
        text: 'Monthly'
      },
      {
        id: 'weekly',
        text: 'Weekly'
      },
      {
        id: 'daily',
        text: 'Daily'
      }
    ];
    entityOptions = [];
    weekOptions = [];
    monthOptions = [];
    cells : Cell[];
    reasons : any[];
    reasonsFreq : any[];
    reasonsMinutes : any[];

    oeeReport : OeeReport;
    downtimeReport : DowntimeReport;

    MTBF = 0;
    MTTR = 0;

    maxValueFreq = 0;
    maxValueMinutes = 0;

    MTBFReady = false;
    MTTRReady = false;

    timerMachine: any;
    timerLine: any;
    now = moment();
    
    // function to convert unix time stamps to Date
    dateToText(obj: number): Date {
        return new Date(obj * 1000);
    }

    ngOnInit() {
        // just wait event from filter component
        //this.doRefresh();
        this.user = JSON.parse(localStorage.currentUser);
        this.getCells();
        this.getReasons();
    }

    ngOnDestroy(){
      this.stopTimerMachine();
      this.stopTimerLine();
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        // console.log(event, active);
    }

    getCells(){
      this.cellService.getCells().subscribe(cells => {
          this.cells = cells;
          this.loading = false;
      },error => {
        console.log("cannot retrieve Cells \n", error);
        setTimeout( ()=> this.getCells(), 5000);
      });
    }

    getReasons(){
      this.reasonService.getUnplannedReasons().subscribe(reasons => {
        this.reasons = reasons;
        this.reasonsFreq = reasons;
        this.reasonsMinutes = reasons;
        this.reasons.forEach((reason,idx) => {
          reason.freq = 0;
          reason.minutes = 0;
        });
      },error => {
        console.log("cannot retrieve Reasons \n", error);
        setTimeout( ()=> this.getReasons(), 5000);
      });
    }

    entityTypeChanged(newVal){
      this.maxValueFreq = 0;
      this.maxValueMinutes = 0;
      this.noResultFoundOee = true;
      this.entity = null;

      this.entityOptions = [];
      if(newVal == "machine"){
          this.entityOptions = this.cells;
      }else if(newVal == "line"){
        this.entityOptions = [{
          'id': '1',
          'name' : 'Line 1',
        }];
      }else{
        this.entityOptions = [];
      }
    }

    checkForMachine(){
      if(this.timePeriodic == "weekly"){
        if(this.entity != null && this.weekYear != null && this.entity != 'null' && this.weekYear != 'null'){
          let cellId = this.entity.id;
          let week = this.weekYear.week;
          let year = this.weekYear.year;

          let endWeek = moment(this.weekYear.endWeek).format('YYYY-MM-DD');
          let startWeek = moment(this.weekYear.startWeek).format('YYYY-MM-DD');
          if(this.now.isBetween(startWeek, endWeek)){
            this.timerMachine = null;
            this.startTimerMachine();
          }else{
            this.stopTimerMachine();
          }

          //GET TBF & MTBF
          this.TBFService.getTimeBetweenFailures(cellId, endWeek).subscribe(res =>{
            this.lineChartDataTBF[0].data = [];
            if(res.length > 0) {
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTBF = 0;
              res.forEach(TBF => {
                tempCount+= TBF.valueMinute;

              });
  
              tempMTBF = tempCount/dataLength;
              this.MTBF = Math.round((tempMTBF/60) * 100) / 100;
  
              let data = [];
              res.forEach(TBF => {
                let temp = {
                    x: TBF.timestamp,
                    y: Math.round((TBF.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              //UPDATE CHART
              this.lineChartDataTBF[0].data = data;
            }
            this.lineChartOptionsTBF.annotation.annotations[0].value = this.MTBF;
            this.MTBFReady = true;
          });

          //GET TTR & MTTR
          this.TTRService.getTimeToRepairs(cellId, endWeek).subscribe(res =>{
            this.lineChartDataTTR[0].data = [];
            if(res.length > 0){
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTTR = 0;
              res.forEach(TTR => {
                tempCount+= TTR.valueMinute;
              });
  
              tempMTTR = tempCount/dataLength;
              this.MTTR = Math.round((tempMTTR/60) * 100) / 100;
  
              let data = [];
              res.forEach(TTR => {
                let temp = {
                    x: TTR.timestamp,
                    y: Math.round((TTR.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              this.lineChartDataTTR[0].data = data;
            }
            this.lineChartOptionsTTR.annotation.annotations[0].value = this.MTTR;
            this.MTTRReady = true;
          });

          //GET WEEKLY OEE REPORT
          this.weeklyOeeReportService.getWeeklyOeeReportViews(cellId,week,year).subscribe(result => {
            this.noResultFoundOee = false;
            this.updatedAtString = moment(result.updatedAt).fromNow();
            this.oee = Math.round((result.oee*100) * 100) / 100;
            this.availability = Math.round((result.availability*100) * 100) / 100;
            if(result.totalCount < 0){
              this.performance = Math.round((100) * 100) / 100;
              this.quality = Math.round((100) * 100) / 100;
            }else{
              this.performance = Math.round((result.performance*100) * 100) / 100;
              this.quality = Math.round((result.quality*100) * 100) / 100;
            }
          });

          //GET WEEKLY DOWNTIME REPORT
          this.weeklyDowntimeReportService.getWeeklyDowntimeReportViews(cellId,week,year).subscribe(results => {
            if(results.length > 0){
              

              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                    reason.freq = result.freq;
                    reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });
        }else{
          this.noResultFoundOee = true;
        }
      }else if (this.timePeriodic == "monthly"){
        if(this.entity != null && this.monthYear != null && this.entity != 'null' && this.monthYear != 'null'){
          let cellId = this.entity.id;
          let month = this.monthYear.month;
          let year = this.monthYear.year;

          var firstDay = new Date(year, month-1, 1);
          let lastDay = new Date(year, month, 0);
          let startMonth = moment(firstDay).format('YYYY-MM-DD');
          let endMonth = moment(lastDay).format('YYYY-MM-DD');
          if(this.now.isBetween(startMonth, endMonth)){
            this.timerMachine = null;
            this.startTimerMachine();
          }else{
            this.stopTimerMachine();
          }
          //GET TBF & MTBF
          this.TBFService.getTimeBetweenFailures(cellId, endMonth).subscribe(res =>{
            this.lineChartDataTBF[0].data = [];
            if(res.length > 0) {
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTBF = 0;
              res.forEach(TBF => {
                tempCount+= TBF.valueMinute;
              });
  
              tempMTBF = tempCount/dataLength;
              this.MTBF = Math.round((tempMTBF/60) * 100) / 100;
  
              let data = [];
              res.forEach(TBF => {
                let temp = {
                    x: TBF.timestamp,
                    y: Math.round((TBF.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              //for simulation many data
              // let temp = JSON.stringify(data);
              // let temp1 = JSON.parse(temp);
              // var currentDate = moment(temp1[temp1.length-1].x);
              // for (let i = 1; i <= 100; i++) {
              //     let temp = {
              //       x: moment(currentDate).add(1, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSSZ"),
              //       y: Math.floor(Math.random() * 24)
              //     };
              //     currentDate = moment(currentDate).add(1, 'days');
              //     data.push(temp);              
              // }
              this.lineChartDataTBF[0].data = data;
            }
            this.lineChartOptionsTBF.annotation.annotations[0].value = this.MTBF;
            this.MTBFReady = true;
          });

          //GET TTR & MTTR
          this.TTRService.getTimeToRepairs(cellId, endMonth).subscribe(res =>{
            this.lineChartDataTTR[0].data = [];
            if(res.length > 0) {
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTTR = 0;
              res.forEach(TTR => {
                tempCount+= TTR.valueMinute;
              });
  
              tempMTTR = tempCount/dataLength;
              this.MTTR = Math.round((tempMTTR/60) * 100) / 100;
  
              let data = [];
              res.forEach(TTR => {
                let temp = {
                    x: TTR.timestamp,
                    y: Math.round((TTR.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              this.lineChartDataTTR[0].data = data;
            }
            this.lineChartOptionsTTR.annotation.annotations[0].value = this.MTTR;
            this.MTTRReady = true;  
          });

          //GET MONTHLY OEE REPORT
          this.monthlyOeeReportService.getMonthlyOeeReportViews(cellId,month,year).subscribe(result => {
            this.noResultFoundOee = false;
            this.updatedAtString = moment(result.updatedAt).fromNow();
            this.oee = Math.round((result.oee*100) * 100) / 100;
            this.availability = Math.round((result.availability*100) * 100) / 100;
            if(result.totalCount < 0){
              this.performance = Math.round((100) * 100) / 100;
              this.quality = Math.round((100) * 100) / 100;
            }else{
              this.performance = Math.round((result.performance*100) * 100) / 100;
              this.quality = Math.round((result.quality*100) * 100) / 100;
            }
          });

          //GET MONTHLY DOWNTIME REPORT
          this.monthlyDowntimeReportService.getMonthlyDowntimeReportViews(cellId,month,year).subscribe(results => {
            if(results.length > 0){
              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                    reason.freq = result.freq;
                    reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });
        }else{
          this.noResultFoundOee = true;
        }
      }else if(this.timePeriodic == "daily"){
        if(this.entity != null && this.date != null){
          let cellId = this.entity.id;
          let date = this.date.format('YYYY-MM-DD');

          if(this.now.format('YYYY-MM-DD') == date){
            this.timerMachine = null;
            this.startTimerMachine();
          }else{
            this.stopTimerMachine();
          }

          //GET TBF & MTBF
          this.TBFService.getTimeBetweenFailures(cellId, date).subscribe(res =>{
            this.lineChartDataTBF[0].data = [];
            if(res.length > 0){
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTBF = 0;
              res.forEach(TBF => {
                tempCount+= TBF.valueMinute;
              });
  
              tempMTBF = tempCount/dataLength;
              this.MTBF = Math.round((tempMTBF/60) * 100) / 100;
  
              let data = [];
              res.forEach(TBF => {
                let temp = {
                    x: TBF.timestamp,
                    y: Math.round((TBF.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              this.lineChartDataTBF[0].data = data;
            }
            this.lineChartOptionsTBF.annotation.annotations[0].value = this.MTBF;
            this.MTBFReady = true;

          });

          //GET TTR & MTTR
          this.TTRService.getTimeToRepairs(cellId, date).subscribe(res =>{
            this.lineChartDataTTR[0].data = [];
            if(res.length > 0) {
              let dataLength = res.length;
              let tempCount = 0;
              let tempMTTR = 0;
              res.forEach(TTR => {
                tempCount+= TTR.valueMinute;
              });
  
              tempMTTR = tempCount/dataLength;
              this.MTTR = Math.round((tempMTTR/60) * 100) / 100;
  
              let data = [];
              res.forEach(TTR => {
                let temp = {
                    x: TTR.timestamp,
                    y: Math.round((TTR.valueMinute/60) * 100) / 100
                };
                data.push(temp);
              });
              this.lineChartDataTTR[0].data = data;
            }
            this.lineChartOptionsTTR.annotation.annotations[0].value = this.MTTR;
            this.MTTRReady = true;
          });

          //GET DAILY OEE REPORT
          this.dailyOeeReportService.getDailyOeeReportViews(cellId,date).subscribe(result => {
            if(result){
              this.noResultFoundOee = false;
              this.updatedAtString = moment(result.updatedAt).fromNow();
              this.oee = Math.round((result.oee*100) * 100) / 100;
              this.availability = Math.round((result.availability*100) * 100) / 100;
              if(result.performance < 0 || result.quality < 0){
                this.performance = Math.round((100) * 100) / 100;
                this.quality = Math.round((100) * 100) / 100;
              }else{
                this.performance = Math.round((result.performance*100) * 100) / 100;
                this.quality = Math.round((result.quality*100) * 100) / 100;
              }
            }else{
              this.noResultFoundOee = true;
            }
          });

          //GET DAILY DOWNTIME REPORT
          this.dailyDowntimeReportService.getDailyDowntimeReportViews(cellId,date).subscribe(results => {
            if(results.length > 0){
              

              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                      reason.freq = result.freq;
                      reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });

        }else{
          this.noResultFoundOee = true;
        }
      }
    }

    checkForLine(){
      if(this.timePeriodic == "weekly"){
        if(this.entity != null && this.weekYear != null && this.entity != 'null' && this.weekYear != 'null'){
          let lineId = this.entity.id;
          let week = this.weekYear.week;
          let year = this.weekYear.year;

          let startWeek = moment(this.weekYear.startWeek).format('YYYY-MM-DD');
          let endWeek = moment(this.weekYear.endWeek).format('YYYY-MM-DD');

          if(this.now.isBetween(startWeek, endWeek)){
            this.timerLine = null;
            this.startTimerLine();
          }else{
            this.stopTimerLine();
          }

          //GET WEEKLY OEE REPORT
          this.weeklyOeeReportService.getWeeklyLineOeeReportViews(lineId,week,year).subscribe(result => {
            this.lineRepresentative = result.name;
            this.noResultFoundOee = false;
            this.updatedAtString = moment(result.updatedAt).fromNow();
            this.oee = Math.round((result.oee*100) * 100) / 100;
            this.availability = Math.round((result.availability*100) * 100) / 100;
            if(result.performance < 0 || result.quality < 0){
              this.performance = Math.round((100) * 100) / 100;
              this.quality = Math.round((100) * 100) / 100;
            }else{
              this.performance = Math.round((result.performance*100) * 100) / 100;
              this.quality = Math.round((result.quality*100) * 100) / 100;
            }
          });

          //GET WEEKLY DOWNTIME REPORT
          this.weeklyLineDowntimeReportService.getWeeklyLineDowntimeReportViews(lineId,week,year).subscribe(results => {
            if(results.length > 0){
              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                      reason.freq = result.freq;
                      reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });
        }else{
          this.noResultFoundOee = true;
        }
      }else if (this.timePeriodic == "monthly"){
        if(this.entity != null && this.monthYear != null && this.entity != 'null' && this.monthYear != 'null'){
          let lineId = this.entity.id;
          let month = this.monthYear.month;
          let year = this.monthYear.year;

          let firstDay = new Date(year, month-1, 1);
          let lastDay = new Date(year, month, 0);

          let startMonth = moment(firstDay).format('YYYY-MM-DD');
          let endMonth = moment(lastDay).format('YYYY-MM-DD');

          if(this.now.isBetween(startMonth, endMonth)){
            this.timerLine = null;
            this.startTimerLine();
          }else{
            this.stopTimerLine();
          }

          //GET MONTHLY OEE REPORT
          this.monthlyOeeReportService.getMonthlyLineOeeReportViews(lineId,month,year).subscribe(result => {
            this.lineRepresentative = result.name;
            this.noResultFoundOee = false;
            this.updatedAtString = moment(result.updatedAt).fromNow();
            this.oee = Math.round((result.oee*100) * 100) / 100;
            this.availability = Math.round((result.availability*100) * 100) / 100;
            if(result.performance < 0 || result.quality < 0){
              this.performance = Math.round((100) * 100) / 100;
              this.quality = Math.round((100) * 100) / 100;
            }else{
              this.performance = Math.round((result.performance*100) * 100) / 100;
              this.quality = Math.round((result.quality*100) * 100) / 100;
            }
          });

          //GET MONTHLY DOWNTIME REPORT
          this.monthlyLineDowntimeReportService.getMonthlyLineDowntimeReportViews(lineId,month,year).subscribe(results => {
            if(results.length > 0){
              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                      reason.freq = result.freq;
                      reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });
        }else{
          this.noResultFoundOee = true;
        }
      }else if(this.timePeriodic == "daily"){
        if(this.entity != null && this.date != null){
          let lineId = this.entity.id;
          let date = this.date.format('YYYY-MM-DD');

          if(this.now.format('YYYY-MM-DD') == date){
            this.timerLine = null;
            this.startTimerLine();
          }else{
            this.stopTimerLine();
          }

          //GET DAILY OEE REPORT
          this.dailyOeeReportService.getDailyLineOeeReportViews(lineId,date).subscribe(result => {
            if(result){
              this.lineRepresentative = result.name;
              this.noResultFoundOee = false;
              this.updatedAtString = moment(result.updatedAt).fromNow();
              this.oee = Math.round((result.oee*100) * 100) / 100;
              this.availability = Math.round((result.availability*100) * 100) / 100;
              if(result.performance < 0 || result.quality < 0){
                this.performance = Math.round((100) * 100) / 100;
                this.quality = Math.round((100) * 100) / 100;
              }else{
                this.performance = Math.round((result.performance*100) * 100) / 100;
                this.quality = Math.round((result.quality*100) * 100) / 100;
              }
            }else{
              this.noResultFoundOee = true;
            }
          });

          //GET DAILY DOWNTIME REPORT
          this.dailyLineDowntimeReportService.getDailyLineDowntimeReportViews(lineId,date).subscribe(results => {
            if(results.length > 0){
              results.forEach(result => {
                this.reasons.forEach(reason => {
                  if(reason.id == result.reasonId){
                      reason.freq = result.freq;
                      reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                  }
                });
              });

              this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
              this.reasonsFreq.forEach((reason,i) => {
                  if(this.maxValueFreq == 0){
                      this.maxValueFreq = reason.freq;
                  }
                  let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
              this.reasonsMinutes = this.reasons;
              let temp = JSON.stringify(this.reasonsMinutes);
              this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
              this.reasonsMinutes.forEach((reason,i) => {
                  if(this.maxValueMinutes == 0){
                      this.maxValueMinutes = reason.minutes;
                  }
                  let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
                  if(i<3){
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                      }
                  }else{
                      reason.styles = {
                          'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                      }
                  }
              });
            }
          });

        }else{
          this.noResultFoundOee = true;
        }
      }
    }

    checkForDisplay(){
      this.cleanData();

      this.noResultFoundOee = true;
      if(this.entityType == 'machine'){
        this.checkForMachine();
      }else if(this.entityType == 'line'){
        this.checkForLine();
      }
    }

    getTimePeriodic(newVal){
      this.cleanData();
      this.monthYear = null;
      this.weekYear = null;
      if(newVal == "weekly"){
        //GET AVAILABLE WEEKS
        this.weeklyOeeReportService.getAvailableWeeks().subscribe(results => {
          results.forEach(result => {
            let startWeek = moment(result.startWeek).format('DD MMM');
            let endWeek = moment(result.endWeek).format('DD MMM');
            result.optionText = "Week "+result.week+", "+result.year+" ("+startWeek+" - "+endWeek+")";
          });
          this.weekOptions = results;
        });
      }else if(newVal == "monthly"){
        //GET AVAILABLE MONTH YEAR
        this.monthlyOeeReportService.getAvailableMonthYears().subscribe(results => {
          results.forEach(result => {
            let month = moment().month(parseInt(result.month) - 1).format("MMMM");
            result.optionText = month+" "+result.year;
          });
          this.monthOptions = results;
        });
      }else if(newVal == 'daily'){
        this.checkForDisplay();
      }
    }

    cleanData(){
      this.stopTimerMachine();
      this.stopTimerLine();
      this.maxValueFreq = 0;
      this.maxValueMinutes = 0;
      this.MTBF = 0;
      this.MTTR = 0;
      this.MTBFReady = false;
      this.MTTRReady = false;
      this.noResultFoundOee = true;
      this.reasonsFreq.forEach(reason => {
        reason.freq = 0;
        reason.minutes = 0;
        reason.seconds = 0;
        reason.styles = {
          'background' : 'linear-gradient(90deg, Gainsboro 0%, transparent 0%)'
        }
      });
      this.reasonsMinutes.forEach(reason => {
        reason.freq = 0;
        reason.minutes = 0;
        reason.seconds = 0;
        reason.styles = {
          'background' : 'linear-gradient(90deg, Gainsboro 0%, transparent 0%)'
        }
      });
      this.reasons.forEach(reason => {
        reason.freq = 0;
        reason.minutes = 0;
        reason.seconds = 0;
      });
    }

    startTimerMachine() {
      if (this.timerMachine == null) {
          this.timerMachine = setInterval(() => {
            if(this.timePeriodic == "monthly"){
              this.updateDataMonthlyMachine();
            }else if(this.timePeriodic == "weekly"){
              this.updateDataWeeklyMachine();
            }else if(this.timePeriodic == "daily" && !this.noResultFoundOee){
              this.updateDataDailyMachine();
            }
          }, 10000);
      }
    }

    stopTimerMachine() {
      if (this.timerMachine != null) {
          clearInterval(this.timerMachine);
          this.timerMachine = null;
      }
    }

    startTimerLine() {
      if (this.timerLine == null) {
          this.timerLine = setInterval(() => {
            if(this.timePeriodic == "monthly"){
              this.updateDataMonthlyLine();
            }else if(this.timePeriodic == "weekly"){
              this.updateDataWeeklyLine();
            }else if(this.timePeriodic == "daily" && !this.noResultFoundOee){
              this.updateDataDailyLine();
            }
          }, 10000);
      }
    }

    stopTimerLine() {
      if (this.timerLine != null) {
          clearInterval(this.timerLine);
          this.timerLine = null;
      }
    }

    updateDataMonthlyMachine(){
      let cellId = this.entity.id;
      let month = this.monthYear.month;
      let year = this.monthYear.year;

      var firstDay = new Date(year, month-1, 1);
      let lastDay = new Date(year, month, 0);
      let startMonth = moment(firstDay).format('YYYY-MM-DD');
      let endMonth = moment(lastDay).format('YYYY-MM-DD');

      //GET TBF & MTBF
      this.TBFService.getTimeBetweenFailures(cellId, endMonth).subscribe(res =>{
        // this.lineChartDataTBF[0].data = [];
        if(res.length > 0) {
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTBF = 0;
          res.forEach(TBF => {
            tempCount+= TBF.valueMinute;
          });

          tempMTBF = tempCount/dataLength;
          this.MTBF = Math.round((tempMTBF/60) * 100) / 100;

          let data = [];
          res.forEach(TBF => {
            let temp = {
                x: TBF.timestamp,
                y: Math.round((TBF.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          this.lineChartDataTBF[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == 'TBFChart'){
              chart.chart.options['annotation']['annotations'][0].value = this.MTBF;
              chart.update();
            }
          });
        }
      });

      //GET TTR & MTTR
      this.TTRService.getTimeToRepairs(cellId, endMonth).subscribe(res =>{
        // this.lineChartDataTTR[0].data = [];
        if(res.length > 0) {
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTTR = 0;
          res.forEach(TTR => {
            tempCount+= TTR.valueMinute;
          });

          tempMTTR = tempCount/dataLength;
          this.MTTR = Math.round((tempMTTR/60) * 100) / 100;

          let data = [];
          res.forEach(TTR => {
            let temp = {
                x: TTR.timestamp,
                y: Math.round((TTR.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          this.lineChartDataTTR[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == "TTRChart"){
              chart.chart.options['annotation']['annotations'][0].value = this.MTTR;
              chart.update();
            }
          });
        }
      });

      //GET MONTHLY OEE REPORT
      this.monthlyOeeReportService.getMonthlyOeeReportViews(cellId,month,year).subscribe(result => {
        this.noResultFoundOee = false;
        this.updatedAtString = moment(result.updatedAt).fromNow();
        this.oee = Math.round((result.oee*100) * 100) / 100;
        this.availability = Math.round((result.availability*100) * 100) / 100;
        if(result.performance < 0 || result.quality < 0){
          this.performance = Math.round((100) * 100) / 100;
          this.quality = Math.round((100) * 100) / 100;
        }else{
          this.performance = Math.round((result.performance*100) * 100) / 100;
          this.quality = Math.round((result.quality*100) * 100) / 100;
        }
      });

      //GET MONTHLY DOWNTIME REPORT
      this.monthlyDowntimeReportService.getMonthlyDowntimeReportViews(cellId,month,year).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });
          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                reason.freq = result.freq;
                reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }

    updateDataWeeklyMachine(){
      let cellId = this.entity.id;
      let week = this.weekYear.week;
      let year = this.weekYear.year;

      let endWeek = moment(this.weekYear.endWeek).format('YYYY-MM-DD');

      //GET TBF & MTBF
      this.TBFService.getTimeBetweenFailures(cellId, endWeek).subscribe(res =>{
        this.lineChartDataTBF[0].data = [];
        if(res.length > 0) {
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTBF = 0;
          res.forEach(TBF => {
            tempCount+= TBF.valueMinute;

          });

          tempMTBF = tempCount/dataLength;
          this.MTBF = Math.round((tempMTBF/60) * 100) / 100;

          let data = [];
          res.forEach(TBF => {
            let temp = {
                x: TBF.timestamp,
                y: Math.round((TBF.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          //UPDATE CHART
          this.lineChartDataTBF[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == 'TBFChart'){
              chart.chart.options['annotation']['annotations'][0].value = this.MTBF;
              chart.update();
            }
          });
        }
      });

      //GET TTR & MTTR
      this.TTRService.getTimeToRepairs(cellId, endWeek).subscribe(res =>{
        this.lineChartDataTTR[0].data = [];
        if(res.length > 0){
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTTR = 0;
          res.forEach(TTR => {
            tempCount+= TTR.valueMinute;
          });

          tempMTTR = tempCount/dataLength;
          this.MTTR = Math.round((tempMTTR/60) * 100) / 100;

          let data = [];
          res.forEach(TTR => {
            let temp = {
                x: TTR.timestamp,
                y: Math.round((TTR.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          this.lineChartDataTTR[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == "TTRChart"){
              chart.chart.options['annotation']['annotations'][0].value = this.MTTR;
              chart.update();
            }
          });
        }
      });

      //GET WEEKLY OEE REPORT
      this.weeklyOeeReportService.getWeeklyOeeReportViews(cellId,week,year).subscribe(result => {
        this.noResultFoundOee = false;
        this.updatedAtString = moment(result.updatedAt).fromNow();
        this.oee = Math.round((result.oee*100) * 100) / 100;
        this.availability = Math.round((result.availability*100) * 100) / 100;
        if(result.performance < 0 || result.quality < 0){
          this.performance = Math.round((100) * 100) / 100;
          this.quality = Math.round((100) * 100) / 100;
        }else{
          this.performance = Math.round((result.performance*100) * 100) / 100;
          this.quality = Math.round((result.quality*100) * 100) / 100;
        }
      });

      //GET WEEKLY DOWNTIME REPORT
      this.weeklyDowntimeReportService.getWeeklyDowntimeReportViews(cellId,week,year).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });

          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                reason.freq = result.freq;
                reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }

    updateDataDailyMachine(){
      let cellId = this.entity.id;
      let date = this.date.format('YYYY-MM-DD');

      //GET TBF & MTBF
      this.TBFService.getTimeBetweenFailures(cellId, date).subscribe(res =>{
        this.lineChartDataTBF[0].data = [];
        if(res.length > 0){
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTBF = 0;
          res.forEach(TBF => {
            tempCount+= TBF.valueMinute;
          });

          tempMTBF = tempCount/dataLength;
          this.MTBF = Math.round((tempMTBF/60) * 100) / 100;

          let data = [];
          res.forEach(TBF => {
            let temp = {
                x: TBF.timestamp,
                y: Math.round((TBF.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          this.lineChartDataTBF[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == 'TBFChart'){
              chart.chart.options['annotation']['annotations'][0].value = this.MTBF;
              chart.update();
            }
          });
        }

      });

      //GET TTR & MTTR
      this.TTRService.getTimeToRepairs(cellId, date).subscribe(res =>{
        this.lineChartDataTTR[0].data = [];
        if(res.length > 0) {
          let dataLength = res.length;
          let tempCount = 0;
          let tempMTTR = 0;
          res.forEach(TTR => {
            tempCount+= TTR.valueMinute;
          });

          tempMTTR = tempCount/dataLength;
          this.MTTR = Math.round((tempMTTR/60) * 100) / 100;

          let data = [];
          res.forEach(TTR => {
            let temp = {
                x: TTR.timestamp,
                y: Math.round((TTR.valueMinute/60) * 100) / 100
            };
            data.push(temp);
          });
          this.lineChartDataTTR[0].data = data;
          this.charts.forEach(chart => {
            if(chart.chart.canvas.id == "TTRChart"){
              chart.chart.options['annotation']['annotations'][0].value = this.MTTR;
              chart.update();
            }
          });
        }
      });

      //GET DAILY OEE REPORT
      this.dailyOeeReportService.getDailyOeeReportViews(cellId,date).subscribe(result => {
        if(result){
          this.noResultFoundOee = false;
          this.updatedAtString = moment(result.updatedAt).fromNow();
          this.oee = Math.round((result.oee*100) * 100) / 100;
          this.availability = Math.round((result.availability*100) * 100) / 100;
          if(result.performance < 0 || result.quality < 0){
            this.performance = Math.round((100) * 100) / 100;
            this.quality = Math.round((100) * 100) / 100;
          }else{
            this.performance = Math.round((result.performance*100) * 100) / 100;
            this.quality = Math.round((result.quality*100) * 100) / 100;
          }
        }else{
          this.noResultFoundOee = true;
        }
      });

      //GET DAILY DOWNTIME REPORT
      this.dailyDowntimeReportService.getDailyDowntimeReportViews(cellId,date).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });

          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                  reason.freq = result.freq;
                  reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }

    updateDataMonthlyLine(){
      let lineId = this.entity.id;
      let month = this.monthYear.month;
      let year = this.monthYear.year;

      //GET MONTHLY OEE REPORT
      this.monthlyOeeReportService.getMonthlyLineOeeReportViews(lineId,month,year).subscribe(result => {
        this.lineRepresentative = result.name;
        this.noResultFoundOee = false;
        this.updatedAtString = moment(result.updatedAt).fromNow();
        this.oee = Math.round((result.oee*100) * 100) / 100;
        this.availability = Math.round((result.availability*100) * 100) / 100;
        if(result.performance < 0 || result.quality < 0){
          this.performance = Math.round((100) * 100) / 100;
          this.quality = Math.round((100) * 100) / 100;
        }else{
          this.performance = Math.round((result.performance*100) * 100) / 100;
          this.quality = Math.round((result.quality*100) * 100) / 100;
        }
      });

      //GET MONTHLY DOWNTIME REPORT
      this.monthlyLineDowntimeReportService.getMonthlyLineDowntimeReportViews(lineId,month,year).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });

          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                  reason.freq = result.freq;
                  reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }

    updateDataWeeklyLine(){
      let lineId = this.entity.id;
      let week = this.weekYear.week;
      let year = this.weekYear.year;

      let startWeek = moment(this.weekYear.startWeek).format('YYYY-MM-DD');
      let endWeek = moment(this.weekYear.endWeek).format('YYYY-MM-DD');

      //GET WEEKLY OEE REPORT
      this.weeklyOeeReportService.getWeeklyLineOeeReportViews(lineId,week,year).subscribe(result => {
        this.lineRepresentative = result.name;
        this.noResultFoundOee = false;
        this.updatedAtString = moment(result.updatedAt).fromNow();
        this.oee = Math.round((result.oee*100) * 100) / 100;
        this.availability = Math.round((result.availability*100) * 100) / 100;
        if(result.performance < 0 || result.quality < 0){
          this.performance = Math.round((100) * 100) / 100;
          this.quality = Math.round((100) * 100) / 100;
        }else{
          this.performance = Math.round((result.performance*100) * 100) / 100;
          this.quality = Math.round((result.quality*100) * 100) / 100;
        }
      });

      //GET WEEKLY DOWNTIME REPORT
      this.weeklyLineDowntimeReportService.getWeeklyLineDowntimeReportViews(lineId,week,year).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });

          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                  reason.freq = result.freq;
                  reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }

    updateDataDailyLine(){
      let lineId = this.entity.id;
      let date = this.date.format('YYYY-MM-DD');

      //GET DAILY OEE REPORT
      this.dailyOeeReportService.getDailyLineOeeReportViews(lineId,date).subscribe(result => {
        if(result){
          this.lineRepresentative = result.name;
          this.noResultFoundOee = false;
          this.updatedAtString = moment(result.updatedAt).fromNow();
          this.oee = Math.round((result.oee*100) * 100) / 100;
          this.availability = Math.round((result.availability*100) * 100) / 100;
          if(result.performance < 0 || result.quality < 0){
            this.performance = Math.round((100) * 100) / 100;
            this.quality = Math.round((100) * 100) / 100;
          }else{
            this.performance = Math.round((result.performance*100) * 100) / 100;
            this.quality = Math.round((result.quality*100) * 100) / 100;
          }
        }else{
          this.noResultFoundOee = true;
        }
      });

      //GET DAILY DOWNTIME REPORT
      this.dailyLineDowntimeReportService.getDailyLineDowntimeReportViews(lineId,date).subscribe(results => {
        if(results.length > 0){
          this.reasons.forEach((reason,idx) => {
              reason.freq = 0;
              reason.minutes = 0;
          });

          results.forEach(result => {
            this.reasons.forEach(reason => {
              if(reason.id == result.reasonId){
                  reason.freq = result.freq;
                  reason.minutes = Math.round((result.seconds/60) * 100) / 100;
              }
            });
          });

          this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
          this.reasonsFreq.forEach((reason,i) => {
              if(this.maxValueFreq == 0){
                  this.maxValueFreq = reason.freq;
              }
              let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
          this.reasonsMinutes = this.reasons;
          let temp = JSON.stringify(this.reasonsMinutes);
          this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
          this.reasonsMinutes.forEach((reason,i) => {
              if(this.maxValueMinutes == 0){
                  this.maxValueMinutes = reason.minutes;
              }
              let percentageForColor = (Math.round((reason.minutes/this.maxValueMinutes) * 100) / 100)*100;
              if(i<3){
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                  }
              }else{
                  reason.styles = {
                      'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                  }
              }
          });
        }
      });
    }
}
