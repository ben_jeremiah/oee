import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';


// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DDTHH:mm:ss.SSSZZ',
  },
  display: {
    dateInput: 'D MMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
    selector: 'app-downtime-record-filter',
    templateUrl: './downtime-record-filter.component.html',
    styleUrls: ['./downtime-record-filter.component.css'],
    providers: [
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ]
})
export class DowntimeRecordFilterComponent implements OnInit {

    @Output() evNew = new EventEmitter<boolean>();
    @Output() evRefresh = new EventEmitter<boolean>();
    @Output() evReset = new EventEmitter<boolean>();

    filterForm = new FormGroup({
        fltCell: new FormControl(''),
        momentStartDate: new FormControl(moment()),
        fltStartDate: new FormControl(''),
    });

    currentDate = moment();

    constructor() { }

    ngOnInit() {
        this.doPublish();

    }

    onNew() {
        this.evNew.emit(null);
        return false;
    }

    onRefresh() {
        this.doPublish();
        return false;
    }

    onReset() {
        this.filterForm.setValue({
            fltMachine: '',
            fltStartDate: '',
            momentStartDate: moment(),
        });
        this.doPublish();
        return false;
    }

    onChange() {
        this.doPublish();
        return false;
    }

    onKeypress(arg: any) {
        if (arg.keyCode === 13) {
            this.doPublish();
            return false;
        }
    }

    onDateInput(arg) {
        //console.log(arg);
        //console.log(this.filterForm.value.fltStartDate);
        this.doPublish();
    }

    doPublish () {
        let varDate = this.filterForm.value.momentStartDate;
        
        if(varDate != null) {
            varDate.set({'hour':0,'minute':0,'second':0,'millisecond':0});
            this.filterForm.value.fltStartDate = varDate.format('YYYY-MM-DDTHH:mm:ss.SSSZZ');
            console.log(this.filterForm.value.fltStartDate);
        }
        this.evRefresh.emit(this.filterForm.value);

    }
}
