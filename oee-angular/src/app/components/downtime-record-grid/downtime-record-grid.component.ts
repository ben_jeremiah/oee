import { Component, OnInit, ViewChild } from '@angular/core';
import { DowntimeRecordService } from '../../services/downtime-record.service';
import { DowntimeRecord } from '../../model/downtime-record';
import {MatTableModule} from '@angular/material/table'; 
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort, Sort} from '@angular/material/sort';
import { PagingResult } from 'src/app/model/paging-result';
import { ActivatedRoute } from '@angular/router';
import { Employee } from 'src/app/model/employee';
import { ReasonService } from '../../services/reason.service';
import { Reason } from 'src/app/model/reason';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/services/alert.service';

@Component({
    selector: 'app-downtime-record-grid',
    templateUrl: './downtime-record-grid.component.html',
    styleUrls: ['./downtime-record-grid.component.css']
})
export class DowntimeRecordGridComponent implements OnInit {

    constructor(
        private downtimeRecordService: DowntimeRecordService,
        private reasonService: ReasonService,
        private alertService: AlertService) { }

    downtimeRecords: DowntimeRecord[];
    pageSize = 10;
    pageIndex = 0;
    totalRecords = 0;
    currentFilter = {};
    sord = 'desc';
    sidx = 'startTime';
    columnsToDisplay = ['cellName', 'startTime','endTime','durationMinute','reason','action'];
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sorter: MatSort;  
    user : Employee;
    reasons : Reason[];

    activeData: DowntimeRecord;
    selectedReason = "";
    loading = true;
  
    // function to convert unix time stamps to Date
    dateToText(obj: number): Date {
        return new Date(obj * 1000);
    }

    ngAfterViewInit() {
        /*
        this.sorter.sortChange.subscribe(() => {
            this.pageIndex = 0;
            this.paginator.pageIndex = 0
        });
        */
    }

    ngOnInit() {
        // just wait event from filter component
        //this.doRefresh();
        this.user = JSON.parse(localStorage.currentUser);
        this.getReasons();
    }

    onFilterNew(arg: any) {
        console.log('New');
    }

    onFilterRefresh(arg: any) {
        console.log('Refresh');
        this.currentFilter = arg;
        this.doRefresh();
    }

    onFilterReset(arg: any) {
        // alert('Reset');
        console.log(arg);
    }

    onPageChange(arg: PageEvent) {
        console.log(arg);
        this.pageIndex = arg.pageIndex;
        this.pageSize = arg.pageSize;
        this.doRefresh();
    }

    onSortChange(arg: Sort){
        this.sord = arg.direction;
        this.sidx = arg.active;
        this.pageIndex = 0;
        this.doRefresh();
    }

    doRefresh() {
        let param = {};
        param['pageSize'] = this.pageSize;
        param['pageIndex'] = this.pageIndex;
        param['sord'] = this.sord;
        param['sidx'] = this.sidx;

        for (const key in this.currentFilter) {
            param[key] = this.currentFilter[key];
        }


        this.downtimeRecordService.selectPaging(param).subscribe((result: PagingResult<DowntimeRecord>) =>
        {
            this.totalRecords = result.totalRecords;
            this.downtimeRecords = result.records;
            this.loading = false;
        },error => {
            console.log("cannot retrieve downtime records \n", error);
        });
    }

    addReasonClicked(activeData){
        this.activeData = activeData; 
    }

    getReasons(){
        this.reasonService.getReasons().subscribe(reasons => {
            this.reasons = reasons
            this.loading = false;
        },
        error => {
            setTimeout( ()=> this.getReasons(), 5000);
            console.log("cannot retrieve reasons \n", error);
        });
    }

    saveReason(){
        this.loading = true;
        this.downtimeRecordService.updateDowntimeRecord(this.activeData)
        .pipe(first())
        .subscribe(
        data => {
            //   setTimeout(, 2000);
            this.alertService.success("Downtime Reason updated")
            this.doRefresh();
        },
        error => {
            this.alertService.error(error);
        });
    }
}
