import {Component, OnInit} from '@angular/core';
import {RuntimeVariableService} from '../../services/runtime-variable.service';
import {RuntimeVariable} from '../../model/runtime-variable';
import * as moment from 'moment';
import { CellService } from 'src/app/services/cell.service';
import { Cell } from 'src/app/model/cell';
import { DailyCellViewService } from 'src/app/services/daily-cell-view.service';

@Component({
    selector: 'app-machines-summary',
    templateUrl: './machines-summary.component.html',
    styleUrls: ['./machines-summary.component.css']
})
export class MachinesSummaryComponent implements OnInit {

    constructor(
        private runtimeVariableService: RuntimeVariableService,
        private dailyCellView: DailyCellViewService,
        ) {
    }
    cells = [];
    timer: any;

    statusMachine = {};
    loading = true;

    defaultClassValue = "col-sm-9 bg-dark text-white status-box";


    ngOnInit() {
        this.timer = null;
        this.fetchData();
        this.startTimer();
        this.getCells();
    }

    ngOnDestroy(){
        this.stopTimer();
    }

    getCells(){
        this.dailyCellView.getDailyCellViews().subscribe(cells => {
            this.loading = false;
            if(this.cells.length == 0){
                this.cells = cells;
                this.cells.forEach(cell => {
                    cell.runningTime = new Date(cell.runningDurationSecond * 1000).toISOString().substr(11, 8);
                    cell.stopTime = new Date(cell.stopDurationSecond * 1000).toISOString().substr(11, 8);

                    cell.oee = Math.round((cell.oee*100) * 100) / 100;
                });
            }else{
                cells.forEach(cell => {
                    let result = this.cells.find(obj => {
                        return obj.id === cell.id
                    })
                    result.runningTime = new Date(cell.runningDurationSecond * 1000).toISOString().substr(11, 8);
                    result.stopTime = new Date(cell.stopDurationSecond * 1000).toISOString().substr(11, 8);

                    result.oee = Math.round((cell.oee*100) * 100) / 100;
                });
            }
        }, error => {
            console.log("cannot retrieve cells \n", error);
        });
    }

    fetchData() {
        let result = this.runtimeVariableService.getRuntimeVariables();
        result.subscribe((arg) => {
            if(arg != null){
                this.statusMachine = {};
                for (let i = 0; i < arg.length; i++) {
                    let tagNameArr = arg[i].tagName.split(".");
                    let cellId = null;
                    if(tagNameArr[1].includes("Running") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = {
                            'statusValue' : "Running",
                            'classValue' : "col-sm-9 bg-success text-white status-box",
                        };
                    }else if(tagNameArr[1].includes("Downtime") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = {
                            'statusValue' : "Downtime",
                            'classValue' : "col-sm-9 bg-orange text-white status-box",
                        };
                    }else if(tagNameArr[1].includes("Idle") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = {
                            'statusValue' : "Idle",
                            'classValue' : "col-sm-9 bg-dark text-white status-box",
                        };
                    }else if(tagNameArr[1].includes("PartDelay") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = {
                            'statusValue' : "PartDelay",
                            'classValue' : "col-sm-9 bg-warning text-white status-box",
                        };
                    }else if(tagNameArr[1].includes("Emergency") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = {
                            'statusValue' : "Emergency",
                            'classValue' : "col-sm-9 bg-danger text-white status-box",
                        };
                    }
                }
    
                if(this.cells){
                    this.cells.forEach(cell => {
                        let status = this.statusMachine[cell.id];
                        if(status != undefined){
                            cell.status = status.statusValue;
                            cell.classValue = status.classValue;
                        }else{
                            cell.classValue = this.defaultClassValue;
                            cell.status = null;
                        }
                    });
                }
            }
        }, error => {
            console.log("cannot retrieve runtime data \n", error);
        });
    }

    startTimer() {
        if (this.timer == null) {
            this.timer = setInterval(() => {
                this.fetchData(); this.getCells();
            }, 5000);
        }
    }

    stopTimer() {
        if (this.timer != null) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }


}
