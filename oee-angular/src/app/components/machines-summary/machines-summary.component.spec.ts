import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesSummaryComponent } from './machines-summary.component';

describe('MachinesSummaryComponent', () => {
  let component: MachinesSummaryComponent;
  let fixture: ComponentFixture<MachinesSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinesSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
