import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../../model/employee';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../../services/employee.service';
import { NgForm } from '@angular/forms';
import { RoleService } from '../../services/role.service';
import { Role } from '../../model/role';
import {Router} from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  roles: Role[];

  @Input() employee: any;

  formType = "";

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService,
    private roleService: RoleService,
    private location: Location,
    private router: Router,
    private alertService : AlertService,
  ) { }

  ngOnInit() {
    if(this.route.snapshot.paramMap.get('id') != 'create'){
      this.getEmployee();
      this.formType = "update";
    }else{
      this.formType = "create";
      this.employee = {};
    }
    this.getRoles();
  }

  getEmployee(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(id)
      .subscribe(employee => {
        this.employee = employee;
        this.employee.oldPassword = this.employee.password;
        this.employee.password = "";
      });
  }

  getRoles(): void {
    this.roleService.getRoles().subscribe(roles => this.roles = roles);
  }

  goBack(): void {
    this.location.back();
  }

  save(form: NgForm): void {
    // let newEmployee: Employee = {...this.employee};
    // newEmployee.username = form.value["username"];
    //this.employee.username : form.value["username"];
    // console.log(this.employee);
    // console.log(newEmployee);
    if(this.formType == 'update'){
      if(this.employee.currentPassword != this.employee.oldPassword){
        this.alertService.error("Current Password is wrong!");
      }else{
        this.employeeService.updateEmployee(this.employee)
        .pipe(first())
        .subscribe(
        data => {
          this.router.navigate(['/employees']);
        },
        error => {
            this.alertService.error(error);
        });
      }
    }else if(this.formType == 'create'){
      this.employeeService.addEmployee(this.employee)
      .pipe(first())
      .subscribe(
      data => {
        this.router.navigate(['/employees']);
      },
      error => {
          this.alertService.error(error);
      });
    }
  }
}
