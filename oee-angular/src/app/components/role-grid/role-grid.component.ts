import { Component, OnInit } from '@angular/core';
import { Role } from '../../model/role';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-role-grid',
  templateUrl: './role-grid.component.html',
  styleUrls: ['./role-grid.component.css']
})
export class RoleGridComponent implements OnInit {

  constructor(private roleService: RoleService) { }

  roles: Role[];

  ngOnInit() {
    this.getRoles();
  }

  getRoles(): void {
    this.roleService.getRoles().subscribe(roles => this.roles = roles);
  }

  addRole(description: string): void {
    const newRole = { description } as Role;
    this.roleService.addRole(newRole)
      .subscribe(role => {
        this.roles.push(role)
      });
  }

  deleteRole(role: Role): void {
    this.roleService.deleteRole(role).subscribe(res => this.roles.filter(a => a !== role));
  }}
