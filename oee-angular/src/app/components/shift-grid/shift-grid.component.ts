import { Component, OnInit } from '@angular/core';
import { Shift } from '../../model/shift';
import { ShiftService } from '../../services/shift.service';

@Component({
  selector: 'app-shift-grid',
  templateUrl: './shift-grid.component.html',
  styleUrls: ['./shift-grid.component.css']
})
export class ShiftGridComponent implements OnInit {

  constructor(private shiftService: ShiftService) { }

  shifts: Shift[];

  ngOnInit() {
    this.getShifts();
  }

  getShifts(): void {
    this.shiftService.getShifts().subscribe(shifts => this.shifts = shifts);
  }

}