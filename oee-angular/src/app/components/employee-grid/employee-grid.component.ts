import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../model/employee';

@Component({
  selector: 'app-employee-grid',
  templateUrl: './employee-grid.component.html',
  styleUrls: ['./employee-grid.component.css']
})
export class EmployeeGridComponent implements OnInit {

  constructor(private employeeService: EmployeeService) { }

  employees: Employee[];

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeeService.getEmployees().subscribe(employees => this.employees = employees);
  }

  addEmployee(name: string, username: string, password: string, roleId: number): void {
    const newEmployee = { name, username, password, roleId } as Employee;
    this.employeeService.addEmployee(newEmployee)
      .subscribe(employee => {
        this.employees.push(employee)
      });
  }

  deleteEmployee(employee: Employee): void {
    this.employeeService.deleteEmployee(employee).subscribe(res => this.employees.filter(a => a !== employee));
  }
}
