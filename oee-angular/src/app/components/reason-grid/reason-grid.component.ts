import { Component, OnInit } from '@angular/core';
import { Reason } from '../../model/reason';
import { ReasonService } from '../../services/reason.service';

@Component({
  selector: 'app-reason-grid',
  templateUrl: './reason-grid.component.html',
  styleUrls: ['./reason-grid.component.css']
})
export class ReasonGridComponent implements OnInit {

  constructor(private reasonService: ReasonService) { }

  reasons: Reason[];

  ngOnInit() {
    this.getReasons();
  }

  getReasons(): void {
    this.reasonService.getReasons().subscribe(reasons => this.reasons = reasons);
  }

  addReason(description: string): void {
    const newReason = { description } as Reason;
    this.reasonService.addReason(newReason)
      .subscribe(reason => {
        this.reasons.push(reason)
      });
  }

  deleteReason(reason: Reason): void {
    this.reasonService.deleteReason(reason).subscribe(res => this.reasons.filter(a => a !== reason));
  }}
