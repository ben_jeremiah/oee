import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasonGridComponent } from './reason-grid.component';

describe('ReasonGridComponent', () => {
  let component: ReasonGridComponent;
  let fixture: ComponentFixture<ReasonGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReasonGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasonGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
