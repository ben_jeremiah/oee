import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { RoleService } from '../../services/role.service';
import { Role } from '../../model/role';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css']
})
export class RoleFormComponent implements OnInit {

  @Input() role: Role;

  constructor(
    private route: ActivatedRoute,
    private roleService: RoleService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getRole();
  }

  getRole(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.roleService.getRole(id)
      .subscribe(role => this.role = role);
  }

  goBack(): void {
    this.location.back();
  }

  save(form: NgForm): void {
    this.roleService.updateRole(this.role)
      .subscribe(() => this.goBack());
  }
}
