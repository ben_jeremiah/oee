import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ShiftService } from '../../services/shift.service';
import { Shift } from '../../model/shift';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shift-form',
  templateUrl: './shift-form.component.html',
  styleUrls: ['./shift-form.component.css']
})
export class ShiftFormComponent implements OnInit {

  @Input() shift: Shift;

  constructor(
    private route: ActivatedRoute,
    private shiftService: ShiftService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getShift();
  }

  getShift(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.shiftService.getShift(id)
      .subscribe(shift => this.shift = shift);
  }

  goBack(): void {
    this.location.back();
  }

  save(form: NgForm): void {
    if(form.value.startTime.length < 6){
      form.value.startTime +=":00";
      this.shift.startTime = form.value.startTime;
    }
    
    if(form.value.endTime.length < 6){
      form.value.endTime +=":00";
      this.shift.endTime = form.value.endTime;
    }
    
    this.shiftService.updateShift(this.shift)
      .subscribe(() => this.goBack());
  }
}
