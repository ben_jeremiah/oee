import { Component, OnInit, ViewChild, QueryList, ViewChildren } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import * as moment from 'moment';
import { MAT_DATE_FORMATS } from '@angular/material';
import { MY_FORMATS } from '../downtime-record-grid/downtime-record-filter.component';
import { ChartDataSets, ChartOptions, ChartPoint } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import { CellService } from 'src/app/services/cell.service';
import { Cell } from 'src/app/model/cell';
import * as pluginAnnotationsXChart from 'chartjs-plugin-annotation';
import * as pluginAnnotationsRChart from 'chartjs-plugin-annotation';
import { ShiftService } from 'src/app/services/shift.service';
import { Shift } from 'src/app/model/shift';
import { QualityReportService } from 'src/app/services/quality-report.service';
import { QualityReport } from 'src/app/model/quality-report';


@Component({
    selector: 'app-quality-report-grid',
    templateUrl: './quality-report-grid.component.html',
    styleUrls: ['./quality-report-grid.component.css'],
    providers: [
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
    ],
})

export class QualityReportGridComponent implements OnInit {

    //CHART
    public lineChartDataXChart: any = [{data: []}];
    public lineChartDataRChart: any = [{data : []}];
    
    public lineChartOptionsXChart: (ChartOptions & { annotation: any }) = {
        responsive: true,
        maintainAspectRatio : false,
        onClick: () => {
              // console.log(this.myChart.chart.getElementAtEvent(event));
              let target : any = this.charts.first.chart.getElementAtEvent(event);
              if(target.length > 0){
                this.selectedData = this.charts.first.datasets[0].data[target[0]._index];
                if(target[0]._index == 0){
                  this.selectedData.MRObject = null;
                }else{
                  this.selectedData.MRObject = this.charts.last.datasets[0].data[target[0]._index-1];
                }
              }
        },
        animation: {         
            onComplete: function(animation) {
                var scale = window.devicePixelRatio;                       

                var sourceCanvas = animation.chart.canvas;
                var copyWidth = animation.chart.scales['y-axis-XChart'].width - 9;
                var copyHeight = animation.chart.scales['y-axis-XChart'].height + animation.chart.scales['y-axis-XChart'].top + 10;

                var targetCanvas : any = document.getElementById("xChartAxis")
                var targetCtx = targetCanvas.getContext("2d");

                targetCtx.scale(scale, scale);
                targetCtx.canvas.width = copyWidth * scale;
                targetCtx.canvas.height = copyHeight * scale;

                targetCtx.canvas.style.width = `${copyWidth}px`;
                targetCtx.canvas.style.height = `${copyHeight}px`;
                targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);

                // var copyWidthRight = animation.chart.scales['y-axis-XChart'].width + 50;
                // var copyHeightRight = animation.chart.scales['y-axis-XChart-right'].height + animation.chart.scales['y-axis-XChart-right'].top + 10;
                
                // var targetCanvasRight : any = document.getElementById("xChartAxisRight");
                // var targetCtxRight = targetCanvasRight.getContext("2d");

                // targetCtxRight.scale(scale, scale);
                // targetCtxRight.canvas.width = copyWidthRight * scale;
                // targetCtxRight.canvas.height = (copyHeightRight * scale)+1;

                // targetCtxRight.canvas.style.width = `${copyWidthRight}px`;
                // targetCtxRight.canvas.style.height = `${copyHeightRight}px`;
                // targetCtxRight.drawImage(sourceCanvas, animation.chart.width-60, 0, copyWidthRight * scale, copyHeightRight * scale, 0, 0, copyWidthRight * scale, copyHeightRight * scale);

                var sourceCtx = sourceCanvas.getContext('2d');

                // Normalize coordinate system to use css pixels.
                sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
                this.loadingXChart = false;
          }
        },
        
        elements: { 
            point: 
            {
                radius: 5,
                hitRadius: 1,
                hoverRadius: 3,
                hoverBorderWidth: 1
            }
        },
        scales: {
          // We use this empty structure as a placeholder for dynamic theming.
          xAxes: [{
            type: 'linear',
            distribution: 'series',
            // time :{
            //   unit:'day',
            // },
            display : true,
            ticks: {
              min: 1,
              stepSize: 5,
            }
          }],
          yAxes: [
            {
              id: 'y-axis-XChart',
              position: 'left',
              ticks: {
                // stepSize: 1,
                // callback: function(value, index, values) {
                //   if(value % 10 == 0){
                //     return value;
                //   }else{
                //     return "";
                //   }
                // }
                // beginAtZero: true,
              }
            },
            // {
            //   id: 'y-axis-XChart-right',
            //   position: 'right',
            //   ticks: {
            //     // beginAtZero: true,
            //     stepSize: 1,
            //     callback: function(value, index, values) {
            //       console.log(value);
            //       if((value) == 85){
            //         return 'UCL = ' + (value);
            //       }else if((value) == 50){
            //         return 'X = '+ (value);
            //       }else if((value) == 15){
            //         return 'LCL = '+(value)
            //       }else{
            //         return ""
            //       }

            //     }
            //   }
            // },
          ]
        },
        annotation: {
          annotations: [{
            type: 'line',
            mode: 'horizontal',
            scaleID: 'y-axis-XChart',
            value: 0,
            borderColor: 'red',
            borderWidth: 2,
            label: {
              enabled: true,
              backgroundColor: 'rgba(0,0,0,0.3)',
              position: 'right',
              content: 'LCL'
            }
          },
          {
            type: 'line',
            mode: 'horizontal',
            scaleID: 'y-axis-XChart',
            value: 0,
            borderColor: 'red',
            borderWidth: 2,
            label: {
              enabled: true,
              backgroundColor: 'rgba(0,0,0,0.3)',
              position: 'right',
              content: 'UCL'
            }
          },
          {
            type: 'line',
            mode: 'horizontal',
            scaleID: 'y-axis-XChart',
            value: 0,
            borderColor: 'green',
            borderWidth: 2,
            label: {
              enabled: true,
              backgroundColor: 'rgba(0,0,0,0.3)',
              position: 'right',
              content: 'Mean'
            }
          }]
        },
        tooltips: {
          callbacks: {
              label: function(tooltipItem, data) {
                  var value = tooltipItem.yLabel;
                 return value+"";
              },

              // title: function(tooltipItem, data){
              //   // let formatted = moment(tooltipItem[0].xLabel).format('DD-MM-YYYY HH:MM:SS');
              //   // return tooltipItem[0].xLabel;
              // }
          } // end callbacks:
      }, //end tooltips
    };

    public lineChartOptionsRChart: (ChartOptions & { annotation: any }) = {
      responsive: true,
      maintainAspectRatio : false,
      onClick: () => {
            // let target : any = this.charts.first.chart.getElementAtEvent(event);
            // if(target.length > 0){
            //   this.selectedData = this.charts.first.data[0][target[0]._index];
            // }
      },
      animation: {         
          onComplete: function(animation) {
              var scale = window.devicePixelRatio;                       

              var sourceCanvas = animation.chart.canvas;
              var copyWidth = animation.chart.scales['y-axis-RChart'].width - 9;
              var copyHeight = animation.chart.scales['y-axis-RChart'].height + animation.chart.scales['y-axis-RChart'].top + 10;

              var targetCanvas : any = document.getElementById("rChartAxis")
              var targetCtx = targetCanvas.getContext("2d");

              targetCtx.scale(scale, scale);
              targetCtx.canvas.width = copyWidth * scale;
              targetCtx.canvas.height = copyHeight * scale;

              targetCtx.canvas.style.width = `${copyWidth}px`;
              targetCtx.canvas.style.height = `${copyHeight}px`;
              targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth * scale, copyHeight * scale, 0, 0, copyWidth * scale, copyHeight * scale);
              var sourceCtx = sourceCanvas.getContext('2d');

              // Normalize coordinate system to use css pixels.
              sourceCtx.clearRect(0, 0, copyWidth * scale, copyHeight * scale);
              this.loadingRChart = false;
        }
      },
      
      elements: { 
          point: 
          {
              radius: 5,
              hitRadius: 1,
              hoverRadius: 3,
              hoverBorderWidth: 1
          }
      },
      scales: {
        // We use this empty structure as a placeholder for dynamic theming.
        xAxes: [{
          type: 'linear',
          distribution: 'series',
          display : true,
          ticks: {
              min: 1,
              stepSize: 5
          }
        }],
        yAxes: [
          {
            id: 'y-axis-RChart',
            position: 'left',
            ticks: {
             
            }
          },
        ]
      },
      annotation: {
        annotations: [
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-RChart',
          value: 0,
          borderColor: 'red',
          borderWidth: 2,
          label: {
            enabled: true,
            backgroundColor: 'rgba(0,0,0,0.3)',
            position: 'right',
            content: 'UCL'
          }
        },
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-RChart',
          value: 0,
          borderColor: 'green',
          borderWidth: 2,
          label: {
            enabled: true,
            backgroundColor: 'rgba(0,0,0,0.3)',
            position: 'right',
            content: 'Mean'
          }
        },
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-RChart',
          value: 0,
          borderColor: 'red',
          borderWidth: 2,
          label: {
            enabled: true,
            backgroundColor: 'rgba(0,0,0,0.3)',
            position: 'right',
            content: 'LCL = 0'
          }
        }]
      },
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
                var value = tooltipItem.yLabel;
               return value+"";
            },

            // title: function(tooltipItem, data){
            //   // let formatted = moment(tooltipItem[0].xLabel).format('DD-MM-YYYY HH:MM:SS');
            //   // return tooltipItem[0].xLabel;
            // }
        } // end callbacks:
    }, //end tooltips
  };
    
    public lineChartColors: Color[] = [
      { // red
        backgroundColor: 'transparent',
        borderColor: 'rgba(148,159,177,0.5)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'black'
      }
    ];
    public lineChartLegend = false;
    public lineChartTypeXChart = 'line';
    public lineChartTypeRChart = 'line';
      
    public lineChartPluginsXChart = [pluginAnnotationsXChart];
    public lineChartPluginsRChart = [pluginAnnotationsRChart];
    
    @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective>;
      
    constructor(
      private cellService: CellService,
      private shiftService: ShiftService,
      private qualityReportService: QualityReportService,

        ) { }

    user : Employee;

    loading = true;
    loadingXChart = true;
    loadingRChart = true;

    entity = null;
    shift = null;
    dataType = null;

    date = moment();
    noResultFound = true;
    noResultFoundStat = true;
    lineRepresentative = null;

    entityOptions = [];
    shiftOptions = [];
    dataTypeOptions = [];

    cells = [];
    shifts : Shift[];
    
    XChartReady = false;
    MRChartReady = false;

    selectedData: any = null;

    timerXChart: any;
    
    total = null;
    CP = null;
    CPKUpper = null;
    CPKLower = null;
    UCLX = null;
    LCLX = null;
    meanX = null;
    UCLMR = null;
    meanMR = null;

    autoScroll = true;

    now = moment();
    
    // function to convert unix time stamps to Date
    dateToText(obj: number): Date {
        return new Date(obj * 1000);
    }

    ngOnInit() {
        // just wait event from filter component
        //this.doRefresh();
        this.user = JSON.parse(localStorage.currentUser);
        this.getShifts();
        this.getCells();

        
        // cek data lebih dari 30
        // kalau lebih dari 30 cek ada berapa bagian (1 bagian = 5 data = )
        // console.log(this.lineChartDataXChart);
        // if (this.timerXChart == null) {
        //   this.timerXChart = setInterval(() => {
        //     let temp = {
        //       x: this.lineChartDataXChart[0].data.length+1,
        //       y: this.lineChartDataXChart[0].data.length+1+65
        //     };

        //     var newwidth = $('.xChartAreaWrapper2').width() +35;
        //     $('.xChartAreaWrapper2').width(newwidth);
        //     $('.xChartAreaWrapper').animate({scrollLeft:newwidth});
        //     this.lineChartDataXChart[0].data.push(temp)
              
        //     }, 3000);
        // }
        // this.getReasons();

    }

    ngOnDestroy(){
      // this.stopTimerMachine();
      // this.stopTimerLine();
    }

    public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
        // console.log(event, active);
    }

    getCells(){
      this.cellService.getCells().subscribe(cells => {
          let usedCell = [];
          let usedCellObj = [];
          cells.forEach((cell,i) => {
            if(cell.processDataTypes.length != 0){
              usedCell.push(i);
            }
          });
          usedCell.forEach(id => {
            usedCellObj.push(cells[id]);
          });
          this.cells = usedCellObj;
          this.entityOptions = this.cells;
          this.loading = false;
      },error => {
        console.log("cannot retrieve Cells \n", error);
        setTimeout( ()=> this.getCells(), 5000);
      });
    }
    
    getShifts(){
      this.shiftService.getShifts().subscribe(shifts => {
          this.shifts = shifts;
          this.shiftOptions = this.shifts;
          this.shiftOptions.forEach(shift => {
            if(shift.startTime < this.now.format('HH:mm:ss') && shift.endTime >= this.now.format('HH:mm:ss')){
              this.shift = shift
            }
            shift.optionText = shift.description+" ("+shift.startTime+" - "+shift.endTime+")";
          });
      },error => {
        console.log("cannot retrieve Shifts \n", error);
        setTimeout( ()=> this.getShifts(), 5000);
      });
    }

    machineChanged(){
      this.dataType = null;
      this.dataTypeOptions = this.entity.processDataTypes;
    }

    onChartScroll($event){
      if($event.target.className == "xChartAreaWrapper"){
        $('.rChartAreaWrapper').scrollLeft($event.target.scrollLeft);
      }else{
        $('.xChartAreaWrapper').scrollLeft($event.target.scrollLeft);
      }
    }

    checkForDisplay(){
      this.cleanData();
      if(this.dataType != null && this.date != null && this.shift != null){
        let date = this.date.format('YYYY-MM-DD');
        let startTime = this.shift.startTime;
        let endTime = this.shift.endTime;
        let dataX = [];
        let dataR = [];

        if(startTime < this.now.format('HH:mm:ss') && endTime >= this.now.format('HH:mm:ss') && this.now.format("YYYY-MM-DD") == date){
          this.timerXChart = null;
          this.startTimerXChart();
        }else{
          this.stopTimerXChart();
        }

        this.qualityReportService.getQualityReports(this.dataType.id,date,startTime,endTime).subscribe(results => {
          if(results.length > 0){
            results.forEach((result, i) => {
              let tempX = {
                  x: i+1,
                  y: Math.round((result.value) * 100) / 100,
                  object : result,
              };
              dataX.push(tempX);
              
              if(result.valueDifference != null){
                if(i != 0){
                  let tempR = {
                      x: i+1,
                      y: Math.round((result.valueDifference) * 100) / 100,
                      object : result,
                  };
                  dataR.push(tempR);
                }
              }
                
            });

            // for (let i = 0; i < 900; i++) {
            //   let tempX = {
            //       x: i+1+results.length,
            //       y: Math.floor(Math.random() * (100 - 1)) + 1 
            //   };
            //   dataX.push(tempX);
              
            // }

            // handle max 30
            if(dataX.length >= 30){
              // check data after 30
              let dataAfter = dataX.length - 30;
              // check new Axis (chart step is 5) 
              // -1 because the first 5 is included in new width (1350)
              let newXAxis = Math.ceil(dataAfter/5)-1;
              let newWidth = 1350+(170*newXAxis);
              //MAX WIDTH IF EXCEED LINE WONT DRAW (PLUGIN PROBLEMS)
              if(newWidth > 16300){
                newWidth = 16300;
              }

              this.charts.first.chart.canvas.width = newWidth;
              let xEl = $('.xChartAreaWrapper2');
              xEl.width(newWidth);
              let rEl = $('.rChartAreaWrapper2');
              rEl.width(newWidth);

              $('.xChartAreaWrapper').animate({scrollLeft:newWidth});
            }
            
            this.charts.first.chart.data.datasets[0].data = dataX;
            this.charts.last.chart.data.datasets[0].data = dataR;
            this.noResultFound = false;
          }else{
            this.noResultFound = true;
          }
        });

        this.qualityReportService.getQualityReportStatistics(this.dataType.id,date,startTime,endTime).subscribe(result => {
          if(result != null){
            //[0] = Total
            //[1] = Mean X
            //[2] = Mean R
            this.total = result[0];
            this.meanX = result[1];
            this.meanMR = result[2];

            
            // Mean X
            this.charts.first.chart.options['annotation'].annotations[2].value = this.meanX;
            let meanXRounded = Math.round((this.meanX) * 100) / 100;
            this.charts.first.chart.options['annotation'].annotations[2].label.content = "Mean = "+ meanXRounded;

            // UCL X = Mean X + (E2 x Mean MR)
            const E2 = 2.660;
            this.UCLX = (this.meanX) + (E2 * this.meanMR);
            this.UCLX = Math.round((this.UCLX) * 100) / 100
            this.charts.first.chart.options['annotation'].annotations[1].value = this.UCLX;
            this.charts.first.chart.options['annotation'].annotations[1].label.content = "UCL = "+ this.UCLX;

            // LCL X = Mean X + (E2 x Mean MR)
            this.LCLX = (this.meanX) - (E2 * this.meanMR);
            this.LCLX = Math.round((this.LCLX) * 100) / 100
            this.charts.first.chart.options['annotation'].annotations[0].value = this.LCLX;
            this.charts.first.chart.options['annotation'].annotations[0].label.content = "LCL = "+ this.LCLX;

            // Mean MR
            let meanMRRounded = Math.round((this.meanMR) * 100) / 100;
            this.charts.last.chart.options['annotation'].annotations[1].value = this.meanMR;
            this.charts.last.chart.options['annotation'].annotations[1].label.content = "Mean = "+ meanMRRounded;

            // UCL MR = D4 x Mean MR 
            const D4 = 3.267;
            this.UCLMR = D4 * this.meanMR;
            this.UCLMR = Math.round((this.UCLMR) * 100) / 100
            this.charts.last.chart.options['annotation'].annotations[0].value = this.UCLMR;
            this.charts.last.chart.options['annotation'].annotations[0].label.content = "UCL = "+ this.UCLMR;

            // CP = (UCL - LCL)/ (6*sigma)
            // SigmaR = [R Bar (mean MR) * d2]
            const d2 = 1.128;
            let sigmaR = this.meanMR * d2;
            this.CP = (this.UCLX - this.LCLX) / (6 * sigmaR);
            console.log((this.UCLX - this.LCLX), (6*sigmaR));
            this.CP = Math.round((this.CP) * 100) / 100;

            // CPK Upper = (UCL - mean X) / (3*sigma)
            this.CPKUpper = (this.UCLX - this.meanX) / (3*sigmaR);
            console.log((this.UCLX - this.meanX), (3*sigmaR));
            this.CPKUpper = Math.round((this.CPKUpper) * 100) / 100;
            
            // CPK Lower = (mean X - LCL) / (3*sigma)
            this.CPKLower = (this.meanX - this.LCLX) / (3*sigmaR);
            console.log((this.meanX - this.LCLX), (3*sigmaR));
            this.CPKLower = Math.round((this.CPKLower) * 100) / 100;
            
            // Rounding
            this.meanX = Math.round((this.meanX) * 100) / 100;
            this.meanMR = Math.round((this.meanMR) * 100) / 100;

            this.XChartReady = true;
            this.MRChartReady = true;
            this.noResultFoundStat = false;
          }else{
            this.noResultFoundStat = true;
          }
        });
      }else{
        this.noResultFound = true;
        this.noResultFoundStat = true;
      }
    }

    updateData(){
      let date = this.date.format('YYYY-MM-DD');
      let startTime = this.shift.startTime;
      let endTime = this.shift.endTime;

      this.qualityReportService.getQualityReports(this.dataType.id,date,startTime,endTime).subscribe(results => {
        if(results.length > 0){
          let oldDataLength = this.charts.first.chart.data.datasets[0].data.length;
          let newDataLength = results.length;
          if(oldDataLength != newDataLength){
            let lengthDiff = Math.abs(newDataLength - oldDataLength);
            for (let i = newDataLength-1; i < (newDataLength-1)+lengthDiff; i++) {
              let newData = results[i];
              let tempX : any = {
                x: i+1,
                y: Math.round((newData.value) * 100) / 100,
                timestamp : newData.timestamp,
              };

              let tempR : any = {
                  x: i+1,
                  y: Math.round((newData.valueDifference) * 100) / 100,
                  timestamp : newData.timestamp,
              };
              this.charts.first.chart.data.datasets[0].data.push(tempX);
              this.charts.last.chart.data.datasets[0].data.push(tempR);
            }
          }
          
          // handle max 30
          if(this.charts.first.chart.data.datasets[0].data.length >= 30){
            // check data after 30
            let dataAfter = this.charts.first.chart.data.datasets[0].data.length - 30;
            // check new Axis (chart step is 5) 
            // -1 because the first 5 is included in new width (1350)
            let newXAxis = Math.ceil(dataAfter/5)-1;
            let newWidth = 1350+(170*newXAxis);
            //MAX WIDTH IF EXCEED LINE WONT DRAW (PLUGIN PROBLEMS)
            if(newWidth > 16300){
              newWidth = 16300;
            }

            this.charts.first.chart.canvas.width = newWidth;
            let xEl = $('.xChartAreaWrapper2');
            xEl.width(newWidth);
            let rEl = $('.rChartAreaWrapper2');
            rEl.width(newWidth);

            if(this.autoScroll){
              $('.xChartAreaWrapper').animate({scrollLeft:newWidth});
            }
          }
          this.noResultFound = false;
        }else{
          this.noResultFound = true;
        }
      });

      this.qualityReportService.getQualityReportStatistics(this.dataType.id,date,startTime,endTime).subscribe(result => {
        if(result != null){
          //[0] = Total
          //[1] = Mean X
          //[2] = Mean R
          this.total = result[0];
          this.meanX = result[1];
          this.meanMR = result[2];

          
          // Mean X
          let meanXRounded = Math.round((this.meanX) * 100) / 100;
          this.charts.first.chart.options['annotation'].annotations[2].value = this.meanX;
          this.charts.first.chart.options['annotation'].annotations[2].label.content = "Mean = "+ meanXRounded;

          // UCL X = Mean X + (E2 x Mean MR)
          const E2 = 2.660;
          this.UCLX = (this.meanX) + (E2 * this.meanMR);
          this.UCLX = Math.round((this.UCLX) * 100) / 100
          this.charts.first.chart.options['annotation'].annotations[1].value = this.UCLX;
          this.charts.first.chart.options['annotation'].annotations[1].label.content = "UCL = "+ this.UCLX;

          // LCL X = Mean X + (E2 x Mean MR)
          this.LCLX = (this.meanX) - (E2 * this.meanMR);
          this.LCLX = Math.round((this.LCLX) * 100) / 100
          this.charts.first.chart.options['annotation'].annotations[0].value = this.LCLX;
          this.charts.first.chart.options['annotation'].annotations[0].label.content = "LCL = "+ this.LCLX;

          // Mean MR
          let meanMRRounded = Math.round((this.meanMR) * 100) / 100;
          this.charts.last.chart.options['annotation'].annotations[1].value = this.meanMR;
          this.charts.last.chart.options['annotation'].annotations[1].label.content = "Mean = "+ meanMRRounded;

          // UCL MR = D4 x Mean MR 
          const D4 = 3.267;
          this.UCLMR = D4 * this.meanMR;
          this.UCLMR = Math.round((this.UCLMR) * 100) / 100
          this.charts.last.chart.options['annotation'].annotations[0].value = this.UCLMR;
          this.charts.last.chart.options['annotation'].annotations[0].label.content = "UCL = "+ this.UCLMR;

          // CP = (UCL - LCL)/ (6*sigma)
          // SigmaR = [R Bar (mean MR) * d2]
          const d2 = 1.128;
          let sigmaR = this.meanMR * d2;
          this.CP = (this.UCLX - this.LCLX) / (6 * sigmaR);

          this.CP = Math.round((this.CP) * 100) / 100;
          // CPK Upper = (UCL - mean X) / (3*sigma)
          this.CPKUpper = (this.UCLX - this.meanX) / (3*sigmaR);
          this.CPKUpper = Math.round((this.CPKUpper) * 100) / 100;
          
          // CPK Lower = (mean X - LCL) / (3*sigma)
          this.CPKLower = (this.meanX - this.LCLX) / (3*sigmaR);
          this.CPKLower = Math.round((this.CPKLower) * 100) / 100;
          
          // Rounding
          this.meanX = Math.round((this.meanX) * 100) / 100;
          this.meanMR = Math.round((this.meanMR) * 100) / 100;

          this.XChartReady = true;
          this.MRChartReady = true;
          this.noResultFoundStat = false;
        }else{
          this.noResultFoundStat = true;
        }
      });
    }

    cleanData(){
      this.stopTimerXChart();
      this.noResultFound = true;
      this.noResultFoundStat = true;
      this.XChartReady = false;
      this.MRChartReady = false;
      this.selectedData = null;
    }

    startTimerXChart() {
      if (this.timerXChart == null) {
          this.timerXChart = setInterval(() => {
            this.updateData();
          }, 7000);
      }
    }

    stopTimerXChart() {
      if (this.timerXChart != null) {
          clearInterval(this.timerXChart);
          this.timerXChart = null;
      }
    }

}
