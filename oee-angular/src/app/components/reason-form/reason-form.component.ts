import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ReasonService } from '../../services/reason.service';
import { Reason } from '../../model/reason';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reason-form',
  templateUrl: './reason-form.component.html',
  styleUrls: ['./reason-form.component.css']
})
export class ReasonFormComponent implements OnInit {

  @Input() reason: Reason;

  constructor(
    private route: ActivatedRoute,
    private reasonService: ReasonService,
    private location: Location
  ) { }

  ngOnInit() {
    this.getReason();
  }

  getReason(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.reasonService.getReason(id)
      .subscribe(reason => this.reason = reason);
  }

  goBack(): void {
    this.location.back();
  }

  save(form: NgForm): void {
    this.reasonService.updateReason(this.reason)
      .subscribe(() => this.goBack());
  }
}
