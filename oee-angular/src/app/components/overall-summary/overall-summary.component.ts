import {Component, OnInit} from '@angular/core';
import {RuntimeVariableService} from '../../services/runtime-variable.service';
import {RuntimeVariable} from '../../model/runtime-variable';
import * as moment from 'moment';
import { CellService } from 'src/app/services/cell.service';
import { Cell } from 'src/app/model/cell';
import { ReasonService } from 'src/app/services/reason.service';
import { OverallDowntimeRecordViewService } from 'src/app/services/overall-downtime-record-view.service';
import { OverallOeeReportViewService } from 'src/app/services/overall-oee-report-view.service';

@Component({
    selector: 'app-overall-summary',
    templateUrl: './overall-summary.component.html',
    styleUrls: ['./overall-summary.component.css']
})
export class OverallSummaryComponent implements OnInit {

    constructor(
        private runtimeVariableService: RuntimeVariableService,
        private cellService: CellService,
        private reasonService: ReasonService,
        private overallDowntimeRecordViewService: OverallDowntimeRecordViewService,
        private overallOeeReportViewService: OverallOeeReportViewService,
        ) {
    }

    oee= 0;
    availability = 0;
    quality= 0;
    performance= 0;

    cellPart1 = [];
    cellPart2 = [];
    cellPart3 = [];
    reasons = [];
    reasonsFreq = [];
    reasonsMinutes = [];
    updatedAtString = null;

    timer: any;

    statusMachine = {};
    maxValueFreq = 0;
    maxValueSeconds = 0;

    overallRepresentative = null;

    lastUpdatedAt: any;

    ngOnInit() {
        this.timer = null;
        this.getReasons();
        this.getCells();
        this.fetchData();
        this.startTimer();
        this.getOverallOeeReportView();

    }

    ngOnDestroy(){
        this.stopTimer();
    }

    getOverallOeeReportView(){
        this.overallOeeReportViewService.getOverallOeeReportView().subscribe(result => {
            this.overallRepresentative = result.name;
            this.lastUpdatedAt = result.updatedAt;
            this.updatedAtString = moment(result.updatedAt).fromNow();
            this.oee = Math.round((result.oee*100) * 100) / 100;
            this.availability = Math.round((result.availability*100) * 100) / 100;
            if(result.totalCount < 0){
              this.performance = Math.round((100) * 100) / 100;
              this.quality = Math.round((100) * 100) / 100;
            }else{
              this.performance = Math.round((result.performance*100) * 100) / 100;
              this.quality = Math.round((result.quality*100) * 100) / 100;
            }
        }, error => {
            this.updatedAtString = moment(this.lastUpdatedAt).fromNow();
            console.log("cannot retrieve OEE Report \n", error);
        });
    }

    getReasons(){
        this.reasonService.getUnplannedReasons().subscribe(reasons => {
            this.reasons = reasons;
            this.reasonsFreq = reasons;
            this.reasonsMinutes = reasons;
            this.reasons.forEach((reason) => {
                reason.freq = 0;
                reason.minutes = 0;
            });
            this.getDowntimeValue();
        }, error => {
            console.log("cannot retrieve Reasons \n", error);
            setTimeout( ()=> this.getReasons(), 5000);
        });
    }

    getDowntimeValue(){
        this.overallDowntimeRecordViewService.getOverallDowntimeRecordViews().subscribe(results => {
            this.reasons.forEach((reason,idx) => {
                reason.freq = 0;
                reason.minutes = 0;
            });
            results.forEach(result => {
                this.reasons.forEach(reason => {
                    if(reason.id == result.id){
                        reason.freq = result.freq;
                        reason.minutes = Math.round((result.seconds/60) * 100) / 100;
                        reason.seconds = result.seconds;
                    }
                });
            });
            this.reasonsFreq = this.reasons.sort((a, b) => b.freq - a.freq);
            this.reasonsFreq.forEach((reason,i) => {
                if(this.maxValueFreq == 0){
                    this.maxValueFreq = reason.freq;
                }
                let percentageForColor = (Math.round((reason.freq/this.maxValueFreq) * 100) / 100)*100;
                if(i<3){
                    reason.styles = {
                        'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                    }
                }else{
                    reason.styles = {
                        'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                    }
                }
            });
            this.reasonsMinutes = this.reasons;
            let temp = JSON.stringify(this.reasonsMinutes);
            this.reasonsMinutes = JSON.parse(temp).sort((a, b) => b.minutes - a.minutes);
            this.reasonsMinutes.forEach((reason,i) => {
                if(reason.minutes != 0){
                    if(this.maxValueSeconds == 0){
                        this.maxValueSeconds = reason.seconds;
                    }
                    let percentageForColor = ((reason.seconds/this.maxValueSeconds) * 100);
                    if(i<3){
                        reason.styles = {
                            'background' : 'linear-gradient(90deg, Salmon '+percentageForColor+'%, transparent 0%)'
                        }
                    }else{
                        reason.styles = {
                            'background' : 'linear-gradient(90deg, Gainsboro '+percentageForColor+'%, transparent 0%)'
                        }
                    }
                }else{
                    reason.styles = {
                        'background' : 'linear-gradient(90deg, Gainsboro 0%, transparent 0%)'
                    }
                }
            });
        }, error => {
            console.log("cannot retrieve Downtime Reports \n", error);
        });

    }

    getCells(){
        this.cellService.getCells().subscribe(cells => {
            for (let i = 0; i < cells.length; i++) {
                cells[i].status = this.statusMachine[name];
                if(i <= 6){
                    this.cellPart1.push(cells[i]);
                }else if(i <= 13 && i > 6){
                    this.cellPart2.push(cells[i]);
                }else{
                    this.cellPart3.push(cells[i]);
                }
            }
        }, error => {
            console.log("cannot retrieve cells \n", error);
            setTimeout( ()=> this.getCells(), 5000);
        });
    }

    fetchData() {
        let result = this.runtimeVariableService.getRuntimeVariables();
        result.subscribe((arg) => {
            if(arg != null){
                this.statusMachine = {};
                for (let i = 0; i < arg.length; i++) {
                    let tagNameArr = arg[i].tagName.split(".");
                    let cellId = null;
                    if(arg[i].tagName.includes("Running") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = "Running";
                    }else if(arg[i].tagName.includes("Downtime") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = "Downtime";
                    }else if(arg[i].tagName.includes("Idle") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = "Idle";
                    }else if( arg[i].tagName.includes("PartDelay") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = "PartDelay";
                    }else if(arg[i].tagName.includes("Emergency") && arg[i].value == '1'){
                        cellId = parseInt(tagNameArr[1].substring(1,3));
                        this.statusMachine[cellId] = "Emergency";
                    }
                }
    
                this.cellPart1.forEach(cell => {
                    let status = this.statusMachine[cell.id];
                    if(status != undefined){
                        cell.status = status;
                    }else{
                        cell.status = null;
                    }
                });
                this.cellPart2.forEach(cell => {
                    let status = this.statusMachine[cell.id];
                    if(status != undefined){
                        cell.status = status;
                    }else{
                        cell.status = null;
                    }
                });
                this.cellPart3.forEach(cell => {
                    let status = this.statusMachine[cell.id];
                    if(status != undefined){
                        cell.status = status;
                    }else{
                        cell.status = null;
                    }
                });
            }
        }, error => {
            console.log("cannot retrieve Runtime Data \n", error);
        });

    }

    startTimer() {
        if (this.timer == null) {
            this.timer = setInterval(() => {
                this.fetchData();
                this.getDowntimeValue();
                this.getOverallOeeReportView();
            }, 5000);
        }
    }

    stopTimer() {
        if (this.timer != null) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

}

