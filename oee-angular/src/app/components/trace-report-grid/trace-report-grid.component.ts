import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/model/employee';

@Component({
    selector: 'app-trace-report-grid',
    templateUrl: './trace-report-grid.component.html',
    styleUrls: ['./trace-report-grid.component.css'],
})

export class TraceReportGridComponent implements OnInit {

      
    constructor(
        ) { }

    user : Employee;
    loading = true;
  
    // function to convert unix time stamps to Date
    dateToText(obj: number): Date {
        return new Date(obj * 1000);
    }

    ngOnInit() {
        // just wait event from filter component
        //this.doRefresh();
        this.user = JSON.parse(localStorage.currentUser);
        this.loading = false;
    }

}
