import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  user: Employee;
  
  constructor(private router: Router
    ) { 
  }

  timer : any;
  alarmWarning : boolean;
  activeUrl : any;


  ngOnInit() {
    this.user = JSON.parse(localStorage.currentUser);
    this.timer = null;
    var href = window.location.href;
    var res =  href.replace(location.origin, "");
    this.activeUrl = res;
  }

  clickMenu(event){
    var target = event.target || event.srcElement || event.currentTarget;
    var href = target.href;
    var res =  href.replace(location.origin, "");
    this.activeUrl = res;
  }

}
