import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RuntimeVariable } from '../model/runtime-variable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RuntimeVariableService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/runtimeData';

  getRuntimeVariable(id: number): Observable<RuntimeVariable> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<RuntimeVariable>(url).pipe();
  }

  getRuntimeVariables(): Observable<Array<RuntimeVariable>> {
      return this.http.get<Array<RuntimeVariable>>(this.apiUrl);
  }

  private log(message: string) {
    console.log(message);
  }
}
