import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { OeeReport } from '../model/oee-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class WeeklyOeeReportViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/weeklyOeeReportViews';

  getWeeklyOeeReportViews(cellId: number, week: string, year: string): Observable<OeeReport> {
    return this.http.get<OeeReport>(this.apiUrl+"/cell/"+cellId+"/"+week+"/"+year);
  }

  getWeeklyLineOeeReportViews(lineId: number, week: string, year: string): Observable<OeeReport> {
    return this.http.get<OeeReport>(this.apiUrl+"/line/"+lineId+"/"+week+"/"+year);
  }

  getAvailableWeeks(): Observable<Array<OeeReport>> {
    return this.http.get<Array<OeeReport>>(this.apiUrl+"/getAvailableWeeks");
  }
  private log(message: string) {
    console.log(message);
  }
}
