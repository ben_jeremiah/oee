import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { DowntimeReport } from '../model/downtime-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class WeeklyLineDowntimeReportViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/weeklyLineDowntimeReportViews';

  getWeeklyLineDowntimeReportViews(cellId: number, week: string, year: string): Observable<Array<DowntimeReport>> {
    return this.http.get<Array<DowntimeReport>>(this.apiUrl+"/"+cellId+"/"+week+"/"+year);
  }

  private log(message: string) {
    console.log(message);
  }
}
