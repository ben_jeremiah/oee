import { TestBed } from '@angular/core/testing';

import { DailyDowntimeReportViewService } from './daily-downtime-report-view.service';

describe('DailyDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyDowntimeReportViewService = TestBed.get(DailyDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
