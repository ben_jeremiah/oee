import { TestBed } from '@angular/core/testing';

import { DailyCellViewService } from './daily-cell-view.service';

describe('DailyCellViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyCellViewService = TestBed.get(DailyCellViewService);
    expect(service).toBeTruthy();
  });
});
