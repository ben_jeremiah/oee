import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Role } from '../model/role';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/roles';

  getRole(id: number): Observable<Role> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Role>(url).pipe();
  }

  getRoles(): Observable<Array<Role>> {
    return this.http.get<Array<Role>>(this.apiUrl);
  }

  addRole(newRole: Role): Observable<Role> {
    return this.http.post<Role>(this.apiUrl, newRole, httpOptions).pipe(
      map(resp => newRole as Role),
      tap(position => this.log(`Role with id =${position.id} was created`)));
  }

  updateRole(position: Role): Observable<Role> {
    const url = `${this.apiUrl}/${position.id}`;
    return this.http.put<any>(url, position, httpOptions).pipe(
      tap(logging => this.log(`Role with id = ${position.id}`))
    );
  }

  deleteRole(position: Role | number): Observable<Object> {
    const id = typeof position === 'number' ? position : position.id;
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete(url, httpOptions).pipe();
  }

  private log(message: string) {
    console.log(message);
  }
}
