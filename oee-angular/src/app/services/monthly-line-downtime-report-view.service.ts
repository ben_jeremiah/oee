import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { DowntimeReport } from '../model/downtime-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MonthlyLineDowntimeReportViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/monthlyLineDowntimeReportViews';

  getMonthlyLineDowntimeReportViews(cellId: number, month: string, year: string): Observable<Array<DowntimeReport>> {
    return this.http.get<Array<DowntimeReport>>(this.apiUrl+"/"+cellId+"/"+month+"/"+year);
  }


  private log(message: string) {
    console.log(message);
  }
}
