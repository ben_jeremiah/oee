import { TestBed } from '@angular/core/testing';

import { WeeklyLineDowntimeReportViewService } from './weekly-line-downtime-report-view.service';

describe('WeeklyLineDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeeklyLineDowntimeReportViewService = TestBed.get(WeeklyLineDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
