import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Reason } from '../model/reason';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ReasonService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/reasons';

  getReason(id: number): Observable<Reason> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Reason>(url).pipe();
  }

  getReasons(): Observable<Array<Reason>> {
    return this.http.get<Array<Reason>>(this.apiUrl);
  }

  getUnplannedReasons(): Observable<Array<Reason>> {
    return this.http.get<Array<Reason>>(this.apiUrl+"/unplannedReasons");
  }

  addReason(newReason: Reason): Observable<Reason> {
    return this.http.post<Reason>(this.apiUrl, newReason, httpOptions).pipe(
      map(resp => newReason as Reason),
      tap(position => this.log(`Reason with id =${position.id} was created`)));
  }

  updateReason(position: Reason): Observable<Reason> {
    const url = `${this.apiUrl}/${position.id}`;
    return this.http.put<any>(url, position, httpOptions).pipe(
      tap(logging => this.log(`Reason with id = ${position.id}`))
    );
  }

  deleteReason(position: Reason | number): Observable<Object> {
    const id = typeof position === 'number' ? position : position.id;
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete(url, httpOptions).pipe();
  }

  private log(message: string) {
    console.log(message);
  }
}
