import { TestBed } from '@angular/core/testing';

import { TimeBetweenFailureService } from './time-between-failure.service';

describe('WeeklyDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service:TimeBetweenFailureService = TestBed.get(TimeBetweenFailureService);
    expect(service).toBeTruthy();
  });
});
