import { TestBed } from '@angular/core/testing';

import { MonthlyOeeReportViewService } from './monthly-oee-report-view.service';

describe('WeeklyOeeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service:MonthlyOeeReportViewService = TestBed.get(MonthlyOeeReportViewService);
    expect(service).toBeTruthy();
  });
});
