import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { QualityReport } from '../model/quality-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class QualityReportService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/qualityReports';

  getQualityReports(processDataTypeId: number, date: string, shiftStart: string, shiftEnd: string): Observable<Array<QualityReport>> {
    return this.http.get<Array<QualityReport>>(this.apiUrl+"/"+processDataTypeId+"/"+date+"/"+shiftStart+"/"+shiftEnd);
  }

  getQualityReportStatistics(processDataTypeId: number, date: string, shiftStart: string, shiftEnd: string): Observable<QualityReport> {
    return this.http.get<QualityReport>(this.apiUrl+"/statistics/"+processDataTypeId+"/"+date+"/"+shiftStart+"/"+shiftEnd);
  }

  private log(message: string) {
    console.log(message);
  }
}
