import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Reason } from '../model/reason';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OverallDowntimeRecordViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/overallDowntimeRecordViews';

  getOverallDowntimeRecordView(id: number): Observable<Reason> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Reason>(url).pipe();
  }

  getOverallDowntimeRecordViews(): Observable<Array<Reason>> {
    return this.http.get<Array<Reason>>(this.apiUrl);
  }

  private log(message: string) {
    console.log(message);
  }
}
