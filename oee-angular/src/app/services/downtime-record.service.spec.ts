import { TestBed } from '@angular/core/testing';

import { DowntimeRecordService } from './downtime-record.service';

describe('DowntimeRecordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DowntimeRecordService = TestBed.get(DowntimeRecordService);
    expect(service).toBeTruthy();
  });
});
