import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Shift } from '../model/shift';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ShiftService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/shifts';

  getShift(id: number): Observable<Shift> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Shift>(url).pipe();
  }

  getShifts(): Observable<Array<Shift>> {
    return this.http.get<Array<Shift>>(this.apiUrl);
  }

  updateShift(position: Shift): Observable<Shift> {
    const url = `${this.apiUrl}/${position.id}`;
    return this.http.put<any>(url, position, httpOptions).pipe(
      tap(logging => this.log(`Shift with id = ${position.id}`))
    );
  }

  private log(message: string) {
    console.log(message);
  }
}
