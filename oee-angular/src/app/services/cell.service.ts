import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Cell } from '../model/cell';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CellService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/cells';

  getCell(id: number): Observable<Cell> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Cell>(url).pipe();
  }

  getCells(): Observable<Array<Cell>> {
    return this.http.get<Array<Cell>>(this.apiUrl);
  }

  addCell(newCell: Cell): Observable<Cell> {
    return this.http.post<Cell>(this.apiUrl, newCell, httpOptions).pipe(
      map(resp => newCell as Cell),
      tap(position => this.log(`Cell with id =${position.id} was created`)));
  }

  updateCell(position: Cell): Observable<Cell> {
    const url = `${this.apiUrl}/${position.id}`;
    return this.http.put<any>(url, position, httpOptions).pipe(
      tap(logging => this.log(`Cell with id = ${position.id}`))
    );
  }

  deleteCell(position: Cell | number): Observable<Object> {
    const id = typeof position === 'number' ? position : position.id;
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete(url, httpOptions).pipe();
  }

  private log(message: string) {
    console.log(message);
  }
}
