import { TestBed } from '@angular/core/testing';

import { MonthlyLineDowntimeReportViewService } from './monthly-line-downtime-report-view.service';

describe('MonthlyLineDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service:MonthlyLineDowntimeReportViewService = TestBed.get(MonthlyLineDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
