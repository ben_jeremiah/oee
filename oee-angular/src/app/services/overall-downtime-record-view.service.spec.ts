import { TestBed } from '@angular/core/testing';

import { OverallDowntimeRecordViewService } from './overall-downtime-record-view.service';

describe('OverallDowntimeRecordViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OverallDowntimeRecordViewService = TestBed.get(OverallDowntimeRecordViewService);
    expect(service).toBeTruthy();
  });
});
