import { TestBed } from '@angular/core/testing';

import { WeeklyOeeReportViewService } from './weekly-oee-report-view.service';

describe('WeeklyOeeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeeklyOeeReportViewService = TestBed.get(WeeklyOeeReportViewService);
    expect(service).toBeTruthy();
  });
});
