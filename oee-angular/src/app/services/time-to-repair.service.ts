import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { TimeToRepair } from '../model/time-to-repair';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TimeToRepairService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/timeToRepairs';

  getTimeToRepairs(cellId: number,date: string): Observable<Array<TimeToRepair>> {
    return this.http.get<Array<TimeToRepair>>(this.apiUrl+"/"+cellId+"/"+date);
  }


  private log(message: string) {
    console.log(message);
  }
}
