import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { TimeBetweenFailure } from '../model/time-between-failure';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TimeBetweenFailureService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/timeBetweenFailures';

  getTimeBetweenFailures(cellId: number, date: string): Observable<Array<TimeBetweenFailure>> {
    return this.http.get<Array<TimeBetweenFailure>>(this.apiUrl+"/"+cellId+"/"+date);
  }


  private log(message: string) {
    console.log(message);
  }
}
