import { TestBed } from '@angular/core/testing';

import { MonthlyDowntimeReportViewService } from './monthly-downtime-report-view.service';

describe('MonthlyDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service:MonthlyDowntimeReportViewService = TestBed.get(MonthlyDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
