import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Cell } from '../model/cell';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DailyCellViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/dailyCellViews';

  getDailyCellView(id: number): Observable<Cell> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Cell>(url).pipe();
  }

  getDailyCellViews(): Observable<Array<Cell>> {
    return this.http.get<Array<Cell>>(this.apiUrl);
  }

  private log(message: string) {
    console.log(message);
  }
}
