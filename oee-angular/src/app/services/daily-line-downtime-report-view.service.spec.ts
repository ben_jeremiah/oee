import { TestBed } from '@angular/core/testing';

import { DailyLineDowntimeReportViewService } from './daily-line-downtime-report-view.service';

describe('DailyLineDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyLineDowntimeReportViewService = TestBed.get(DailyLineDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
