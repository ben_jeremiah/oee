import { TestBed } from '@angular/core/testing';

import { DailyOeeReportViewService } from './daily-oee-report-view.service';

describe('DailyOeeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyOeeReportViewService = TestBed.get(DailyOeeReportViewService);
    expect(service).toBeTruthy();
  });
});
