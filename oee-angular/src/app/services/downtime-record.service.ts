import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { DowntimeRecord } from '../model/downtime-record';
import { PagingResult } from '../model/paging-result';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class DowntimeRecordService {

    constructor(private http: HttpClient) { }

    private apiUrl = environment.apiUrl + '/downtimeRecords';

    // getDowntimeRecord(id: number): Observable<DowntimeRecord> {
    //     const url = `${this.apiUrl}/${id}`;
    //     return this.http.get<DowntimeRecord>(url).pipe();
    // }

    // getDowntimeRecords(): Observable<Array<DowntimeRecord>> {
    //     return this.http.get<Array<DowntimeRecord>>(this.apiUrl);
    // }

    // addDowntimeRecord(newDowntimeRecord: DowntimeRecord): Observable<DowntimeRecord> {
    //     return this.http.post<DowntimeRecord>(this.apiUrl, newDowntimeRecord, httpOptions).pipe(
    //         map(resp => newDowntimeRecord as DowntimeRecord),
    //         tap(workOrder => this.log(`DowntimeRecord with id =${workOrder.id} was created`)));
    // }

    updateDowntimeRecord(workOrder: DowntimeRecord): Observable<DowntimeRecord> {
        const url = `${this.apiUrl}/updateDowntimeRecordReason/${workOrder.id}`;
        return this.http.put<any>(url, workOrder, httpOptions).pipe(
            tap(logging => this.log(`DowntimeRecord with id = ${workOrder.id} was updated`))
        );
    }

    // deleteDowntimeRecord(workOrder: DowntimeRecord | number): Observable<Object> {
    //     const id = typeof workOrder === 'number' ? workOrder : workOrder.id;
    //     const url = `${this.apiUrl}/${id}`;

    //     return this.http.delete(url, httpOptions).pipe();
    // }

    private log(message: string) {
        console.log(message);
    }

    selectPaging(arg: any) {
        // generate query param
        let url = this.apiUrl + '/page?t=' + new Date().valueOf().toString();
        for (const key in arg) {
            url = url + '&' + key + '=' + encodeURIComponent(arg[key]);
        }
        console.log(url);
        return this.http.get<PagingResult<DowntimeRecord>>(url).pipe();
    }


}
