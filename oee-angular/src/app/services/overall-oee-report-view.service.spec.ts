import { TestBed } from '@angular/core/testing';

import { OverallOeeReportViewService } from './overall-oee-report-view.service';

describe('OverallOeeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OverallOeeReportViewService = TestBed.get(OverallOeeReportViewService);
    expect(service).toBeTruthy();
  });
});
