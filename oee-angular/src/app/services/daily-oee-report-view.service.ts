import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { OeeReport } from '../model/oee-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DailyOeeReportViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/dailyOeeReportViews';

  getDailyOeeReportViews(cellId: number, date: string): Observable<OeeReport> {
    return this.http.get<OeeReport>(this.apiUrl+"/cell/"+cellId+"/"+date);
  }

  getDailyLineOeeReportViews(lineId: number, date: string): Observable<OeeReport> {
    return this.http.get<OeeReport>(this.apiUrl+"/line/"+lineId+"/"+date);
  }

  private log(message: string) {
    console.log(message);
  }
}
