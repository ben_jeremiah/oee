import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { OeeReport } from '../model/oee-report';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class OverallOeeReportViewService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/overallOeeReportViews';

  getOverallOeeReportView(): Observable<OeeReport> {
    return this.http.get<OeeReport>(this.apiUrl);
  }

  private log(message: string) {
    console.log(message);
  }
}
