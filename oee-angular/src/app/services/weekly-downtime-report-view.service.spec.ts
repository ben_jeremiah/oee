import { TestBed } from '@angular/core/testing';

import { WeeklyDowntimeReportViewService } from './weekly-downtime-report-view.service';

describe('WeeklyDowntimeReportViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeeklyDowntimeReportViewService = TestBed.get(WeeklyDowntimeReportViewService);
    expect(service).toBeTruthy();
  });
});
