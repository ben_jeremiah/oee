import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap } from "rxjs/operators";
import { Employee } from '../model/employee';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  private apiUrl = environment.apiUrl + '/employees';

  getEmployee(id: number): Observable<Employee> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Employee>(url).pipe();
  }

  getEmployees(): Observable<Array<Employee>> {
    return this.http.get<Array<Employee>>(this.apiUrl);
  }

  addEmployee(newEmployee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.apiUrl, newEmployee, httpOptions).pipe(
      map(resp => newEmployee as Employee),
      tap(employee => this.log(`Employee with id =${employee.id} was created`)));
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    const url = `${this.apiUrl}/${employee.id}`;
    return this.http.put<any>(url, employee, httpOptions).pipe(
      tap(logging => this.log(`Employee with id = ${employee.id} was updated`))
    );
  }

  deleteEmployee(employee: Employee | number): Observable<Object> {
    const id = typeof employee === 'number' ? employee : employee.id;
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete(url, httpOptions).pipe();
  }

  // authenticate(username: string, password: string) { 
  //   this.log("authenticate called");
  // }

  ok(body?) {
    return of(new HttpResponse({ status: 200, body }))
}

  private log(message: string) {
    console.log(message);
  }
}
