import { TestBed } from '@angular/core/testing';

import { QualityReportService } from './quality-report.service';

describe('QualityReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service:QualityReportService = TestBed.get(QualityReportService);
    expect(service).toBeTruthy();
  });
});
