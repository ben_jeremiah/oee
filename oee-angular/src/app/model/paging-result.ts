export interface PagingResult<T> {
    success: boolean;
    message: string;
    stackTrace: string;
    page: number;
    totalRecords: number;
    records: Array<T>;
}