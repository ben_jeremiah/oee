export interface Reason {
    id: number;
    description: string;

    freq : number;
    seconds : number;
    minutes : number;

}