export interface TimeToRepair {
    id: number;
    timestamp: Date;
    valueMinute: number;

    cellId: number;
}