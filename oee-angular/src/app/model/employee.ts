export interface Employee {
    id: number;
    name: string;
    roleId: number;
    roleDescription: string;

    // Specific for registering an employee as user on application
    username: string;
    password: string;
    token: string;
}