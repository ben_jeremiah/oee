export interface DowntimeReport {
    id: number;
    reasonId : number;
    description: number;
    cellId: number;
    lineId: number;
    
    freq: number;
    seconds: number;

    //daily
    date: Date;
    //weekly
    week : string;
    startWeek : Date;
    endWeek: Date;
    //monthly
    month: string;
    //monthly and weekly
    year: string;

}