export interface DowntimeRecord {
    id: number;
    startTime : Date;
    endTtime : Date;
    cellId : number;
    cellName : string;
    reasonId : number;
    reasonDescription : string;
    durationMinute: number;
}