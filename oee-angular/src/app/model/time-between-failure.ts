export interface TimeBetweenFailure {
    id: number;
    timestamp: Date;
    valueMinute: number;

    cellId: number;
}