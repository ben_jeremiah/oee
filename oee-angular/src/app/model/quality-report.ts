export interface QualityReport {
    id: number;
    timestamp : Date;
    processDataTypeId: number;
    value: number;
    valueDifference: number;
}