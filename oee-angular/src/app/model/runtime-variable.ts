import * as moment from 'moment';

export class RuntimeVariable {
    public tagName: string;
    public dataTimeStamp: string;
    public timeStamp: Date;
    public value: string;
    public quality: boolean;
    public opcDataType: string;

}
