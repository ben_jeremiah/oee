export interface Cell {
    id: number;
    name: string;
    description: string;
    status: number;
    notes: string;

    classValue : string;
    runningDurationSecond : number;
    stopDurationSecond : number;
    runningTime : string;
    stopTime : string;
    oee : number;
    oeeRecord : number;

    processDataTypes: any;
}