import { Time } from '@angular/common';

export interface Shift {
    id: number;
    description: string;
    startTime : Time;
    endTime : Time;
}