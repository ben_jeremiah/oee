export interface OeeReport {
    id: number;
    cellId : number;
    lineId : number;
    name: number;
    
    totalCount : number;
    oee: number;
    availability: number;
    performance : number;
    quality: number;
    oeeRecord: number;
    updatedAt : Date;

    optionText: string;

    //daily
    date: Date;
    //weekly
    week : string;
    startWeek : Date;
    endWeek: Date;
    //monthly
    month: string;
    //monthly and weekly
    year: string;

}