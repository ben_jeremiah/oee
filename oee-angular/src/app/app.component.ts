import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { Employee } from './model/employee';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  baseUrl = environment.baseUrl;

  currentUser: Employee;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
}

logout() {
  this.authenticationService.logout();
  this.router.navigate(['/login']);
}
}
