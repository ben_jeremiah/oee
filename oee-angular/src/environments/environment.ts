// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Change like this if karaf
// apiUrl: 'http://localhost:8181/cip'

// Change like this if test using json-server
// apiUrl: 'http://localhost:3000'

// export const environment = {
//   production: false,
//   apiUrl: 'http://demoset03.cinovasi.com:8181/cip',
//   baseUrl: 'http://localhost:4200/vocip'
// };


export const environment = {
    production: false,
    apiUrl: 'http://localhost:8181/cxf',
    // apiUrl: 'http://demoset03.cinovasi.com:8181/cip',
    baseUrl: 'http://localhost:4200/oee-ahm'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
