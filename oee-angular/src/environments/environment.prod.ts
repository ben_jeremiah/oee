export const environment = {
  production: true,

  //Just to use in local computer, not yet configured for real production
  apiUrl: 'http://localhost:8181/cxf',
  baseUrl: 'http://localhost:8181/oee-ahm'
};
