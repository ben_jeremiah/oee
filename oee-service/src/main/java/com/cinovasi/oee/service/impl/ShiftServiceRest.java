package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Shift;
import com.cinovasi.oee.core.service.ShiftService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class ShiftServiceRest {
	Logger LOG = LoggerFactory.getLogger(ShiftServiceRest.class);

	@Inject
    @Reference
	ShiftService shiftService;


	@Context
	UriInfo uri;

	@GET
	@Path("{id}")
	public Response getShift(@PathParam("id") Integer id) {
		Shift shift = shiftService.getShift(id);
		return shift == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(shift).build();
	}

	// @POST
	// public Response addShift(Shift shift) {
	// 	shiftService.addShift(shift);
	// 	URI taskURI = uri.getRequestUriBuilder().path(ShiftServiceRest.class, "getShift").build(shift.getId());
	// 	return Response.created(taskURI).build();
	// }

	@GET
	public Collection<Shift> getShifts() {
		return shiftService.getShifts();
	}

	@PUT
	@Path("{id}")
	public void updateShift(@PathParam("id") Integer id, Shift shift) {
		shiftService.updateShift(shift);
	}

}
