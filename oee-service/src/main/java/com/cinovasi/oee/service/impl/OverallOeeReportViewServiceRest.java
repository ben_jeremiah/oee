package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.OverallOeeReportView;
import com.cinovasi.oee.core.service.OverallOeeReportViewService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class OverallOeeReportViewServiceRest {
	Logger LOG = LoggerFactory.getLogger(OverallOeeReportViewServiceRest.class);

	@Inject
    @Reference
	OverallOeeReportViewService overallOeeReportViewService;


	@Context
	UriInfo uri;

	@GET
	public OverallOeeReportView getOverallOeeReportView() {
		return overallOeeReportViewService.getOverallOeeReportView();
	}

}
