package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.HistoricalDataDTO;
import com.cinovasi.oee.core.dto.PagingQuery;
import com.cinovasi.oee.core.dto.PagingResult;
import com.cinovasi.oee.core.entities.DowntimeRecord;
import com.cinovasi.oee.service.utils.PagingQueryJAXRS;
import com.cinovasi.oee.core.service.DowntimeRecordService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class DowntimeRecordServiceRest {
	Logger LOG = LoggerFactory.getLogger(DowntimeRecordServiceRest.class);

	@Inject
    @Reference
	DowntimeRecordService downtimeRecordService;


	@Context
	UriInfo uri;

	// @GET
	// @Path("{id}")
	// public Response getDowntimeRecord(@PathParam("id") Long id) {
	// 	DowntimeRecord workOrder = downtimeRecordService.getDowntimeRecord(id);
	// 	return workOrder == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(workOrder).build();
	// }

	// @POST
	// public Response addDowntimeRecord(DowntimeRecord workOrder) {
	// 	downtimeRecordService.addDowntimeRecord(workOrder);
	// 	URI taskURI = uri.getRequestUriBuilder().path(DowntimeRecordServiceRest.class, "getWorkOrder")
	// 			.build(workOrder.getId());
	// 	return Response.created(taskURI).build();
	// }

	// @GET
	// public Collection<DowntimeRecord> getDowntimeRecords() {
	// 	return downtimeRecordService.getDowntimeRecords();
	// }

	@GET
	@Path("page")
	public PagingResult<DowntimeRecord> selectPaging() {
		MultivaluedMap<String, String> query = uri.getQueryParameters();
		PagingQuery pq = PagingQueryJAXRS.extractFromQueryParam(query);
		return downtimeRecordService.selectPaging(pq);
	}

	@PUT
	@Path("{id}")
	public void updateDowntimeRecord(@PathParam("id") Long id, DowntimeRecord downtimeRecord) {
		downtimeRecord.setId(id);
		downtimeRecordService.updateDowntimeRecord(downtimeRecord);
	}

	@PUT
	@Path("updateDowntimeRecordReason/{id}")
	public void updateDowntimeRecordReason(@PathParam("id") Long id, DowntimeRecord downtimeRecord) {
		downtimeRecord.setId(id);
		downtimeRecordService.updateDowntimeRecordReason(downtimeRecord);
	}

	// @DELETE
	// @Path("{id}")
	// public void deleteDowntimeRecord(@PathParam("id") Long id) {
	// 	downtimeRecordService.deleteDowntimeRecord(id);
	// }
	
	

}
