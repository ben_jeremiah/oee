package com.cinovasi.oee.service.utils;

import java.util.Properties;

import javax.ws.rs.core.MultivaluedMap;

import com.cinovasi.oee.core.dto.PagingQuery;

public class PagingQueryJAXRS extends PagingQuery {

	public static PagingQuery extractFromQueryParam(MultivaluedMap<String, String> param)
    {
        PagingQuery pq = new PagingQuery();
        Properties searchParam = new Properties();

        for(String key : param.keySet())
        {
        	String value = param.getFirst(key);
        	
            if (key.indexOf("flt", 0) != -1)
            {
                searchParam.setProperty(key,  value);
            }

            if ("pageIndex".equals(key))
            {
                pq.setPageIndex(Integer.parseInt(value));
            }

            if ("pageSize".equals(key))
            {
                pq.setPageSize(Integer.parseInt(value));
            }

            if ("sord".equals(key))
            {
                pq.setSortDirection("asc".equals(value.toLowerCase()) ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc);
            }

            if ("sidx".equals(key ))
            {
                pq.setSortColumn(value);
            }
        }

        pq.setSearchParam(searchParam);

        return pq;
    }
}
