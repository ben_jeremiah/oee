package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.MonthlyOeeReportView;
import com.cinovasi.oee.core.service.MonthlyOeeReportViewService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class MonthlyOeeReportViewServiceRest {
	Logger LOG = LoggerFactory.getLogger(MonthlyOeeReportViewServiceRest.class);

	@Inject
    @Reference
	MonthlyOeeReportViewService monthlyOeeReportService;


	@Context
	UriInfo uri;

	@GET
	@Path("cell/{cellId}/{month}/{year}")
	public MonthlyOeeReportView getMonthlyOeeReportViews(@PathParam("cellId") Integer cellId, @PathParam("month") String month, @PathParam("year") String year) {
		return monthlyOeeReportService.getMonthlyOeeReportViewByCellAndMonthYear(cellId,month,year);
	}

	@GET
	@Path("line/{lineId}/{month}/{year}")
	public MonthlyOeeReportView getMonthlyLineOeeReportViews(@PathParam("lineId") Integer lineId, @PathParam("month") String month, @PathParam("year") String year) {
		return monthlyOeeReportService.getMonthlyLineOeeReportViewByLineAndMonthYear(lineId,month,year);
	}

	@GET
	@Path("getAvailableMonthYears")
	public Collection<MonthlyOeeReportView> getAvailableMonthYears() {
		return monthlyOeeReportService.getAvailableMonthYears();
	}

}
