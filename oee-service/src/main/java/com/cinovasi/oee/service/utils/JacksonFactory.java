package com.cinovasi.oee.service.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

public class JacksonFactory {

	public static ObjectMapper createObjectMapper()
	{
		ObjectMapper om = new ObjectMapper();
		om.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS); // serialize datetime as ISO 8601

		Hibernate5Module h5m = new Hibernate5Module();
		h5m.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
		om.registerModule(h5m);
		
		return om;
	}
}
