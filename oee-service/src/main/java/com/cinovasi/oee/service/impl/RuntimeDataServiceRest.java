package com.cinovasi.oee.service.impl;

import java.util.Collection;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.HistoricalDataDTO;
import com.cinovasi.oee.core.dto.HistoricalQueryParam;
import com.cinovasi.oee.core.dto.RuntimeDataDTO;
import com.cinovasi.oee.core.service.RuntimeDataService;
// import com.cinovasi.oee.core.service.RuntimeHistoryService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
// @Path("/runtimeData")
//@Tag(name = "runtimeData")
public class RuntimeDataServiceRest {
	Logger LOG = LoggerFactory.getLogger(RuntimeDataServiceRest.class);

	@Inject
    @Reference
	RuntimeDataService runtimeDataService;

	// @Inject
    // @Reference
	// RuntimeHistoryService historyService;

	@Context
	UriInfo uri;

	@GET
	@Path("/")
    @Operation(
            summary = "Get all OPC values",
            description = "Get current OPC values from all registered tags",
            responses = {
                @ApiResponse(
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = RuntimeDataDTO.class))),
                    responseCode = "200"
                )
            }
        )	
	public Collection<RuntimeDataDTO> getAll() {
		return runtimeDataService.getData();
	}

	// @POST
	// @Path("/historical")
    // @Operation(
    //         summary = "Get historical OPC values",
    //         description = "Get historical OPC values of the requested tag names, for a specified time window",
    //         responses = {
    //             @ApiResponse(
    //                 content = @Content(array = @ArraySchema(schema = @Schema(implementation = HistoricalDataDTO.class))),
    //                 responseCode = "200"
    //             )
    //         }
    //     )	
	// public Collection<HistoricalDataDTO> getHistorical(
	// 		@Parameter(description="The query object to specify the desired data", required=true)
	// 		HistoricalQueryParam param
	// 		) {
	// 	return historyService.getHistorical(param);
	// }
}
