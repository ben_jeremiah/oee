package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Employee;
import com.cinovasi.oee.core.service.EmployeeService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class EmployeeServiceRest {
	Logger LOG = LoggerFactory.getLogger(EmployeeServiceRest.class);

	@Inject
    @Reference
	EmployeeService employeeService;


	@Context
	UriInfo uri;

	@GET
	@Path("{id}")
	public Response getEmployee(@PathParam("id") Integer id) {
		Employee employee = employeeService.getEmployee(id);
		return employee == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(employee).build();
	}

	@POST
	public Response addEmployee(Employee employee) {
		employeeService.addEmployee(employee);
		URI taskURI = uri.getRequestUriBuilder().path(EmployeeServiceRest.class, "getEmployee").build(employee.getId());
		return Response.created(taskURI).build();
	}

	@POST
	@Path("authenticate")
	public Response authenticate(Employee rawEmployee) {
		//TypeReference<Map<String, String>> mapTypeReference = new TypeReference<Map<String, String>>() {    };
		//Map<String, String> loginData = mapper.readValue(requestPost, mapTypeReference);
		LOG.info("EmployeeServiceRest - username : " + rawEmployee.getUsername());
		LOG.info("EmployeeServiceRest - password : " + rawEmployee.getPassword());
		Employee employee = employeeService.authenticateEmployee(rawEmployee.getUsername(), rawEmployee.getPassword());
		return employee == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(employee).build();
	}

	@GET
	public Collection<Employee> getEmployees() {
		return employeeService.getEmployees();
	}

	@PUT
	@Path("{id}")
	public void updateEmployee(@PathParam("id") Integer id, Employee employee) {
		employee.setId(id);
		employeeService.updateEmployee(employee);
	}

	@DELETE
	@Path("{id}")
	public void deleteEmployee(@PathParam("id") Integer id) {
		employeeService.deleteEmployee(id);
	}

}
