package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Reason;
import com.cinovasi.oee.core.service.ReasonService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class ReasonServiceRest {
	Logger LOG = LoggerFactory.getLogger(ReasonServiceRest.class);

	@Inject
    @Reference
	ReasonService reasonService;


	@Context
	UriInfo uri;

	@GET
	@Path("{id}")
	public Response getReason(@PathParam("id") Integer id) {
		Reason reason = reasonService.getReason(id);
		return reason == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(reason).build();
	}

	@POST
	public Response addReason(Reason reason) {
		reasonService.addReason(reason);
		URI taskURI = uri.getRequestUriBuilder().path(ReasonServiceRest.class, "getReason").build(reason.getId());
		return Response.created(taskURI).build();
	}

	@GET
	public Collection<Reason> getReasons() {
		return reasonService.getReasons();
	}

	@GET
	@Path("unplannedReasons")
	public Collection<Reason> getunplannedReasons() {
		return reasonService.getUnplannedReasons();
	}

	@PUT
	@Path("{id}")
	public void updateReason(@PathParam("id") Integer id, Reason reason) {
		reasonService.updateReason(reason);
	}

	@DELETE
	@Path("{id}")
	public void deleteReason(@PathParam("id") Integer id) {
		reasonService.deleteReason(id);
	}

}
