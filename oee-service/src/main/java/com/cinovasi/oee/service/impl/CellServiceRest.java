package com.cinovasi.oee.service.impl;

import java.net.URI;
import java.util.Collection;
// import java.util.Map;
// import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Reference;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.service.CellService;

@Bean
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@CrossOriginResourceSharing(allowAllOrigins = true)
public class CellServiceRest {
	Logger LOG = LoggerFactory.getLogger(CellServiceRest.class);

	@Inject
    @Reference
	CellService cellService;


	@Context
	UriInfo uri;

	@GET
	@Path("{id}")
	public Response getCell(@PathParam("id") Integer id) {
		Cell cell = cellService.getCell(id);
		return cell == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(cell).build();
	}

	@POST
	public Response addCell(Cell cell) {
		cellService.addCell(cell);
		URI taskURI = uri.getRequestUriBuilder().path(CellServiceRest.class, "getCell").build(cell.getId());
		return Response.created(taskURI).build();
	}

	@GET
	public Collection<Cell> getCells() {
		return cellService.getCells();
	}

	@PUT
	@Path("{id}")
	public void updateCell(@PathParam("id") Integer id, Cell cell) {
		cellService.updateCell(cell);
	}

	@DELETE
	@Path("{id}")
	public void deleteCell(@PathParam("id") Integer id) {
		cellService.deleteCell(id);
	}

}
