
    create table alarm (
        id  bigserial not null,
        alarm_time timestamp,
        alarm_priority_id int8,
        primary key (id)
    );

    create table alarm_priority (
        id  bigserial not null,
        description varchar(255),
        priority_code int4,
        primary key (id)
    );

    create table area (
        id  serial not null,
        description varchar(255),
        site_id int4,
        primary key (id)
    );

    create table cell (
        id  serial not null,
        cell_type int4,
        description varchar(255),
        line_id int4,
        primary key (id)
    );

    create table cell_group (
        id  serial not null,
        cell_type int4,
        description varchar(255),
        primary key (id)
    );

    create table cell_tag (
        id  bigserial not null,
        tag_key int4,
        tag_value varchar(255),
        cell_id int4,
        primary key (id)
    );

    create table cell_work_order (
        cell_id int4 not null,
        wo_id int8,
        primary key (cell_id)
    );

    create table cip_calculation_context (
        id  serial not null,
        end_time timestamp,
        start_time timestamp,
        status int4,
        wo_id int8,
        primary key (id)
    );

    create table cleaning_matrix (
        id  bigserial not null,
        cleaning_procedure_id int8,
        equipment_id int4,
        line_id int4,
        station_id int4,
        primary key (id)
    );

    create table cleaning_matrix_detail (
        id  bigserial not null,
        concentrate float4,
        flow int4,
        step_sequence int4,
        temperature int4,
        time int4,
        cleaning_matrix_id int8,
        cleaning_step_id int4,
        product_id int8,
        primary key (id)
    );

    create table cleaning_parameter (
        id  bigserial not null,
        description varchar(255),
        primary key (id)
    );

    create table cleaning_procedure (
        id  bigserial not null,
        description varchar(255),
        primary key (id)
    );

    create table cleaning_procedure_detail (
        id  bigserial not null,
        step_sequence int4,
        cleaning_procedure_id int8,
        cleaning_step_id int4,
        primary key (id)
    );

    create table cleaning_step (
        id  serial not null,
        description varchar(255),
        primary key (id)
    );

    create table employee (
        id  serial not null,
        employee_name varchar(255),
        employee_no varchar(255),
        LastAccess timestamp,
        password varchar(255),
        token varchar(255),
        username varchar(255),
        position int4,
        primary key (id)
    );

    create table line (
        id  serial not null,
        description varchar(255),
        area_id int4,
        primary key (id)
    );

    create table position (
        id  serial not null,
        description varchar(255),
        primary key (id)
    );

    create table product (
        id  bigserial not null,
        price float8,
        product_name varchar(255),
        product_type_id int4,
        unit_of_measurement_id int4,
        primary key (id)
    );

    create table product_consumption (
        id  bigserial not null,
        quantity int4,
        subtotal_cost float8,
        product int8,
        work_order int8,
        primary key (id)
    );

    create table product_type (
        id  serial not null,
        description varchar(255),
        primary key (id)
    );

    create table site (
        id  serial not null,
        description varchar(255),
        primary key (id)
    );

    create table unit_of_measurement (
        id  serial not null,
        description varchar(255),
        primary key (id)
    );

    create table work_order (
        id  bigserial not null,
        end_time timestamp,
        start_time timestamp,
        total_cost float8,
        wo_date timestamp,
        wo_no varchar(255),
        wo_status int4,
        equipment_id int4,
        line_id int4,
        station_id int4,
        primary key (id)
    );

    create table work_order_detail (
        id  bigserial not null,
        concentrate float4,
        flow int4,
        step_sequence int4,
        temperature int4,
        time int4,
        cleaning_step_id int4,
        work_order_id int8,
        primary key (id)
    );

    alter table alarm 
        add constraint FKcg96fjehx26bsx274ixglhvbw 
        foreign key (alarm_priority_id) 
        references alarm_priority;

    alter table area 
        add constraint FKqi8fhnmfptwx8k68xxmw2wx8a 
        foreign key (site_id) 
        references site;

    alter table cell 
        add constraint FKhra53ekdyimrnfe03ii9ifrof 
        foreign key (line_id) 
        references line;

    alter table cell_tag 
        add constraint FK5i9lj8pd8p41j4x1yv6jxlqha 
        foreign key (cell_id) 
        references cell;

    alter table cleaning_matrix 
        add constraint FK6q0red7plj1gf1c0qhm0wepvl 
        foreign key (cleaning_procedure_id) 
        references cleaning_procedure;

    alter table cleaning_matrix 
        add constraint FK8nui2aofjhqrt0eyj3th3rv6b 
        foreign key (equipment_id) 
        references cell;

    alter table cleaning_matrix 
        add constraint FKid1bpyc92q363r46rrasaww74 
        foreign key (line_id) 
        references line;

    alter table cleaning_matrix 
        add constraint FKqw626ipeh7evrgplunkb0x8ss 
        foreign key (station_id) 
        references cell;

    alter table cleaning_matrix_detail 
        add constraint FKoqf3fp766ncuanq7wbvxulwnv 
        foreign key (cleaning_matrix_id) 
        references cleaning_matrix;

    alter table cleaning_matrix_detail 
        add constraint FKtf18wornbevg68m91xtj764wg 
        foreign key (cleaning_step_id) 
        references cleaning_step;

    alter table cleaning_matrix_detail 
        add constraint FKdrakeive7uqe63wjs33n41wk4 
        foreign key (product_id) 
        references product;

    alter table cleaning_procedure_detail 
        add constraint FKsixpsah4ctogf21r1pyvj3o6o 
        foreign key (cleaning_procedure_id) 
        references cleaning_procedure;

    alter table cleaning_procedure_detail 
        add constraint FKaau6slj2a50capldnak5hpxpi 
        foreign key (cleaning_step_id) 
        references cleaning_step;

    alter table employee 
        add constraint FK7r4mt1rwghd3enpivpadinsdt 
        foreign key (position) 
        references position;

    alter table line 
        add constraint FKgy9vc11nv0css25wd924c644w 
        foreign key (area_id) 
        references area;

    alter table product 
        add constraint FKlabq3c2e90ybbxk58rc48byqo 
        foreign key (product_type_id) 
        references product_type;

    alter table product 
        add constraint FK45iyjd3w6r65ksjlhm0tycpj3 
        foreign key (unit_of_measurement_id) 
        references unit_of_measurement;

    alter table product_consumption 
        add constraint FKaonk2ovog2rv1henenqs3wy58 
        foreign key (product) 
        references product;

    alter table product_consumption 
        add constraint FKmvlhikajh2lvknlambrlrya02 
        foreign key (work_order) 
        references work_order;

    alter table work_order 
        add constraint FKua3yknnu7lfcxnynutwthxo6 
        foreign key (equipment_id) 
        references cell;

    alter table work_order 
        add constraint FK6gd0qul6fje3ha4qlijkxdgnj 
        foreign key (line_id) 
        references line;

    alter table work_order 
        add constraint FKgbjb0ckmpkr65gwvh6plykpmd 
        foreign key (station_id) 
        references cell;

    alter table work_order_detail 
        add constraint FK5vcqinsj2a0f43w3alk2455nq 
        foreign key (cleaning_step_id) 
        references cleaning_step;

    alter table work_order_detail 
        add constraint FKm0ia1imutoolp7eoxcek1rub5 
        foreign key (work_order_id) 
        references work_order;
