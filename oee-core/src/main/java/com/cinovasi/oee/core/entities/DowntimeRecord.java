package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.cinovasi.oee.core.constants.Tag;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "downtime_record")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DowntimeRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
    private Date endTime;

    @Column(name = "duration_second")
    private Integer durationSecond;

    @Column(name = "tag_trigger")
    private String tagTrigger;

    @Column(name = "previous_downtime_id")
    private Long previousDowntimeId;
    
    @ManyToOne
	@JoinColumn(name = "cell_id")
	@JsonIgnore
    private Cell cell;
    
    @ManyToOne
	@JoinColumn(name = "reason_id")
	@JsonIgnore
    private Reason reason;
    
    @ManyToOne
	@JoinColumn(name = "context_id")
	@JsonIgnore
	private OeeCalculationContext context;

    @Transient
	private Integer cellId;
	@Transient
    private String cellName;
    
    @Transient
	private Integer reasonId;
	@Transient
    private String reasonDescription;
    @Transient
	private Long contextId;
    @Transient
	private Long durationMinute;
    
    public DowntimeRecord(){
        super();
    }

    public DowntimeRecord( Date startTime, Date endTime, Cell cell, Reason reason) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.cell = cell;
        this.reason = reason;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

	public Integer getCellId() {
		return cell != null ? cell.getId() : cellId;
    }
    
	public void setCellId(Integer cellId) {
		this.cellId = cellId;
    }
    
	public Long getContextId() {
		return context != null ? context.getId() : contextId;
    }
    
	public void setContextId(Long contextId) {
		this.contextId = contextId;
	}

	public String getCellName() {
		return cell != null ? cell.getName() : cellName;
	}

	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

    public Integer getReasonId() {
        if(reason != null){
            return reason.getId();
        }else{
            if(reasonId != null){
                return reasonId;
            }else{
                return null;
            }
        }
	}

	public void setReasonId(Integer reasonId) {
		this.reasonId = reasonId;
	}

	public String getReasonDescription() {
		return reason != null ? reason.getDescription() : null;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

    public Long getDurationMinute() {
		if(startTime != null && endTime != null)
		{
			return (endTime.getTime() - startTime.getTime())/60000;
		}
		else return null;
	}


	public void setDurationMinute(Long durationMinute) {
		this.durationMinute = durationMinute;
	}

    public OeeCalculationContext getContext() {
        return context;
    }

    public void setContext(OeeCalculationContext context) {
        this.context = context;
    }

    public Integer getDurationSecond() {
        return durationSecond;
    }

    public void setDurationSecond(Integer durationSecond) {
        this.durationSecond = durationSecond;
    }

    public String getTagTrigger() {
        return tagTrigger;
    }

    public void setTagTrigger(String tagTrigger) {
        this.tagTrigger = tagTrigger;
    }

    public Long getPreviousDowntimeId() {
        return previousDowntimeId;
    }

    public void setPreviousDowntimeId(Long previousDowntimeId) {
        this.previousDowntimeId = previousDowntimeId;
    }
    
    
}