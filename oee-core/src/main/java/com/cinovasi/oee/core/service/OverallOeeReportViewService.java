package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.OverallOeeReportView;

@WebService(name = "overallOeeReportViewService")
public interface OverallOeeReportViewService {

    OverallOeeReportView getOverallOeeReportView();

}