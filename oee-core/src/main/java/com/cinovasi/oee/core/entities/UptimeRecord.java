package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.cinovasi.oee.core.constants.Tag;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "uptime_record")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class UptimeRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "start_time")
	private Date startTime;

	@Column(name = "end_time")
    private Date endTime;

    @Column(name = "duration_second")
    private Long durationSecond;
    
    @ManyToOne
	@JoinColumn(name = "cell_id")
	@JsonIgnore
    private Cell cell;
    
    @ManyToOne
	@JoinColumn(name = "context_id")
	@JsonIgnore
	private OeeCalculationContext context;

    @Transient
	private Integer cellId;
	@Transient
    private String cellName;
    
    @Transient
	private Long contextId;
    @Transient
	private Long durationMinute;
    
    public UptimeRecord(){
        super();
    }

    public UptimeRecord( Date startTime, Date endTime, Cell cell, Reason reason) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.cell = cell;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }


	public Integer getCellId() {
		return cell != null ? cell.getId() : cellId;
    }
    
	public void setCellId(Integer cellId) {
		this.cellId = cellId;
    }
    
	public Long getContextId() {
		return context != null ? context.getId() : contextId;
    }
    
	public void setContextId(Long contextId) {
		this.contextId = contextId;
	}

	public String getCellName() {
		return cell != null ? cell.getName() : cellName;
	}

	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

    public Long getDurationMinute() {
		if(startTime != null && endTime != null)
		{
			return (endTime.getTime() - startTime.getTime())/60000;
		}
		else return null;
	}


	public void setDurationMinute(Long durationMinute) {
		this.durationMinute = durationMinute;
	}

    public OeeCalculationContext getContext() {
        return context;
    }

    public void setContext(OeeCalculationContext context) {
        this.context = context;
    }

    public Long getDurationSecond() {
        return durationSecond;
    }

    public void setDurationSecond(Long durationSecond) {
        this.durationSecond = durationSecond;
    }
    
    
}