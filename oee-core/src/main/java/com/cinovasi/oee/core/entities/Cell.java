package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "cell")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cell implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "line_id")
	private Integer lineId;

	@Column(name = "notes")
	private String notes;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cell")
	private Set<CellTag> cellTags;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cell")
	private Set<ProcessDataType> processDataTypes;
	
	public Cell() {
		super();
	}

	public Cell(String name, String description, Integer lineId, String notes, Set<CellTag> cellTags,Set<ProcessDataType> processDataTypes) {
		this.name = name;
		this.description = description;
		this.lineId = lineId;
		this.notes = notes;
		this.cellTags = cellTags;
		this.processDataTypes = processDataTypes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Set<CellTag> getCellTags() {
		return cellTags;
	}

	public void setCellTags(Set<CellTag> cellTags) {
		this.cellTags = cellTags;
	}

	public Set<ProcessDataType> getProcessDataTypes() {
		return processDataTypes;
	}

	public void setProcessDataTypes(Set<ProcessDataType> processDataTypes) {
		this.processDataTypes = processDataTypes;
	}
}