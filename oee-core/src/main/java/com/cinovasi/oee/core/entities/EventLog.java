package com.cinovasi.oee.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "t_event_log")
public class EventLog {
	private int id;
	private Date evTimestamp;
	private int cellId;
	private String event;
	private String noWo;
	private int totalCounter;
	private int badCounter;
	private String notes;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date upadtedDate;
	private int reason;

	private int goodCount;

	public EventLog() {

	}

	public EventLog(int id, Date evTimestamp, int cellId, String event, String noWo, int totalCounter, int badCounter,
			String notes, String createdBy, Date createdDate, String updatedBy, Date upadtedDate, int reason) {
		this.id = id;
		this.evTimestamp = evTimestamp;
		this.cellId = cellId;
		this.event = event;
		this.noWo = noWo;
		this.totalCounter = totalCounter;
		this.badCounter = badCounter;
		this.notes = notes;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.upadtedDate = upadtedDate;
		this.reason = reason;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "ev_timestamp")
	public Date getEvTimestamp() {
		return evTimestamp;
	}

	public void setEvTimestamp(Date evTimestamp) {
		this.evTimestamp = evTimestamp;
	}

	@Column(name = "asset_id")
	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	@Column(name = "event")
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	@Column(name = "no_wo")
	public String getNoWo() {
		return noWo;
	}

	public void setNoWo(String noWo) {
		this.noWo = noWo;
	}

	@Column(name = "total_counter")
	public int getTotalCounter() {
		return totalCounter;
	}

	public void setTotalCounter(int totalCounter) {
		this.totalCounter = totalCounter;
	}

	@Column(name = "bad_counter")
	public int getBadCounter() {
		return badCounter;
	}

	public void setBadCounter(int badCounter) {
		this.badCounter = badCounter;
	}

	@Column(name = "notes")
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name = "updated_date")
	public Date getUpadtedDate() {
		return upadtedDate;
	}

	public void setUpadtedDate(Date upadtedDate) {
		this.upadtedDate = upadtedDate;
	}

	@Column(name = "reason")
	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}

	@Transient
	public int getGoodCount() {
		return goodCount;
	}

	public void setGoodCount(int goodCount) {
		this.goodCount = goodCount;
	}

}
