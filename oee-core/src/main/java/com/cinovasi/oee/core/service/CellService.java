package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.Cell;

@WebService(name = "cellService")
public interface CellService {

    Cell getCell(Integer id);

    Collection<Cell> getCells();
    
    void addCell(Cell cell);

    void updateCell(Cell cell);

    void deleteCell(Integer id);
}