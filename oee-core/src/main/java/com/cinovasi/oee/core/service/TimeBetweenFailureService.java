package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.TimeBetweenFailure;

@WebService(name = "timeBetweenFailureService")
public interface TimeBetweenFailureService {
	void addTimeBetweenFailure(TimeBetweenFailure TBF);

	Collection<TimeBetweenFailure> getTBFBeforeTime(Integer cellId,String date);
}
