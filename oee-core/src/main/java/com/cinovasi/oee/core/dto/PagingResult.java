package com.cinovasi.oee.core.dto;

import java.util.List;

public class PagingResult<T> {

    private boolean success;
    private String message;
    private String stackTrace;
    private int page;
    private long totalRecords;
    private List<T> records;
    private Object sum;
    private Object avg;
    private Object max;
    private Object min;

    public boolean getSuccess() { return success;}
    public String getMessage() { return message;}
    public String getStackTrace() { return stackTrace;}
    public int getPage() { return page;}
    public long getTotalRecords() { return totalRecords;}
    public List<T> getRecords() { return records;}
    public Object getSum() { return sum;}
    public Object getAvg() { return avg;}
    public Object getMax() { return max;}
    public Object getMin() { return min;}
    
    public void setSuccess(boolean val) { success = val;}
    public void setMessage(String val) { message = val;}
    public void setStackTrace(String val) { stackTrace = val;}
    public void setPage(int val) { page = val;}
    public void setTotalRecords(long val) { totalRecords = val;}
    public void setRecords(List<T> val) { records = val;}
    public void setSum(Object val) { sum = val;}
    public void setAvg(Object val) { avg = val;}
    public void setMax(Object val) { max = val;}
    public void setMin(Object val) { min = val;}
    
    public PagingResult()
    {
    	totalRecords = 0L;
        page = 0;
        message = "OK";
        success = true;
    }
	
}
