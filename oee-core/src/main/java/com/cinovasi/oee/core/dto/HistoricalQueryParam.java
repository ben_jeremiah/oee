package com.cinovasi.oee.core.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoricalQueryParam implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date startTime;
	private Date endTime;
	private int increment; // in second
	private List<String> variables;

	public HistoricalQueryParam() {
		variables = new ArrayList<String>();
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getIncrement() {
		return increment;
	}

	public void setIncrement(int increment) {
		this.increment = increment;
	}

	public List<String> getVariables() {
		return variables;
	}

	public void setVariables(List<String> variables) {
		this.variables = variables;
	}


}