package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.DailyLineDowntimeReportView;

@WebService(name = "dailyLineDowntimeReportViewService")
public interface DailyLineDowntimeReportViewService {

    Collection<DailyLineDowntimeReportView> getDailyLineDowntimeReportViewByLineAndDate(Integer lineId, String date);

}