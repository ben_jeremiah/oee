package com.cinovasi.oee.core.dto;

import java.util.Properties;

public class PagingQuery {

    public enum Direction
    {
        Asc,
        Desc
    }
	
	protected int pageIndex;
	protected int pageSize;
	protected Properties searchParam;
	protected String sortColumn;
	protected Direction sortDirection;
    
    public int getPageIndex() { return pageIndex;}
    public void setPageIndex(int val) { pageIndex = val;}
    public int getPageSize() { return pageSize;}
    public void setPageSize(int val) { pageSize = val;}
    public Properties getSearchParam() { return searchParam;}
    public void setSearchParam(Properties val) { searchParam = val;}
    public String getSortColumn() { return sortColumn;}
    public void setSortColumn(String val) { sortColumn = val;}
    public Direction getSortDirection() { return sortDirection;}
    public void setSortDirection(Direction val) { sortDirection = val;}

    public PagingQuery()
    {
        searchParam = new Properties();
    }

    public boolean hasParam(String param)
    {
        if (this.searchParam.containsKey(param) && this.searchParam.get(param) != null &&
        		((String)this.searchParam.get(param)).length() > 0)
            return true;
        else
            return false;
    }
    
    public String getParam(String key)
    {
    	if (hasParam(key))
    	{
    		return searchParam.getProperty(key);
    	}
    	else return null;
    }
    
    public static PagingQuery extractFromRequestParam(java.util.Map param)
    {
        PagingQuery pq = new PagingQuery();
        Properties searchParam = new Properties();
        java.util.Set<String> keys = param.keySet();
        java.util.Iterator<String> it = keys.iterator();
        
        while(it.hasNext())        	
        {
        	String key = it.next();
        	String[] vals = (String[])param.get(key);
        	String value = vals[0];
        	
        	
            if (key.indexOf("flt", 0) != -1)
            {
                searchParam.setProperty(key,  value);
            }

            if ("pageIndex".equals(key))
            {
                pq.setPageIndex(Integer.parseInt(value));
            }

            if ("pageSize".equals(key))
            {
                pq.setPageSize(Integer.parseInt(value));
            }

            if ("sord".equals(key))
            {
                pq.setSortDirection("asc".equals(value.toLowerCase()) ? PagingQuery.Direction.Asc : PagingQuery.Direction.Desc);
            }

            if ("sidx".equals(key ))
            {
                pq.setSortColumn(value);
            }
        }

        pq.setSearchParam(searchParam);

        return pq;
    }
    

}
