package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.MonthlyLineDowntimeReportView;

@WebService(name = "monthlyLineDowntimeReportViewService")
public interface MonthlyLineDowntimeReportViewService {

    Collection<MonthlyLineDowntimeReportView> getMonthlyLineDowntimeReportViewByLineAndMonthYear(Integer lineId, String month, String year);

}