package com.cinovasi.oee.core.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserNotificationCompositeKey implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7206829016686576425L;

	@Column(name = "User_Id", nullable = false)
	private int userId;

	@Column(name = "NFM_Template", nullable = false)
	private String nfmTemplate;

	public UserNotificationCompositeKey() {
	}

	public UserNotificationCompositeKey(int userId, String nfmTemplate) {
		setUserId(userId);
		setNfmTemplate(nfmTemplate);
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNfmTemplate() {
		return nfmTemplate;
	}

	public void setNfmTemplate(String nfmTemplate) {
		this.nfmTemplate = nfmTemplate;
	}
	@Override
	public boolean equals(Object obj){
		return super.equals(obj);
	}
	@Override
	public int hashCode(){
		return super.hashCode();
	}
}
