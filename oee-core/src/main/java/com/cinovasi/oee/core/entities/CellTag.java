package com.cinovasi.oee.core.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.cinovasi.oee.core.constants.Tag;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "cell_tag")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class CellTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cell_id")
    private Cell cell;

    @Column(name = "tag_key")
    private int tagKey;

    @Column(name = "tag_name")
    private String tagName;

    @Transient
    private int cellId;

    public CellTag() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CellTag(int cellId, Tag tagKey, String tagName) {
        super();
        this.cellId = cellId;
        this.tagKey = tagKey.getTag();
        this.tagName = tagName;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public Tag getTagKey() {
        return Tag.fromTagCode(tagKey);
    }

    public void setTagKey(Tag tagKey) {
        this.tagKey = tagKey.getTag();
    }

    public int getCellId() {
        return cell != null ? cell.getId() : cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }



}