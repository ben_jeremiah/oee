package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.DailyOeeReportView;

@WebService(name = "dailyOeeReportViewService")
public interface DailyOeeReportViewService {

    DailyOeeReportView getDailyOeeReportViewByCellAndDate(Integer cellId, String date);
    
    DailyOeeReportView getDailyLineOeeReportViewByLineAndDate(Integer lineId, String date);
}