package com.cinovasi.oee.core.entities.key;

import java.io.Serializable;
// import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlRootElement;

// import com.cinovasi.oee.core.helper.Tag;

// @Embeddable
// @XmlRootElement
// @Access(AccessType.FIELD)
public class CellTagId implements Serializable {

    private static final long serialVersionUID = 1L;

    // @Column(name = "id", nullable = false)
    private int id; // corresponds to primary key type of Cell

    // @Column(name = "tag_key", nullable = false)
    private int tagKey;

    public CellTagId() {
        super();
    }

    public CellTagId(int id, int tagKey) {
        super();
        this.id = id;
        this.tagKey = tagKey;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTagKey() {
        return tagKey;
    }

    public void setTagKey(int tagKey) {
        this.tagKey = tagKey;
    }

    // @Override
    // public int hashCode() {
    //     final int prime = 31;
    //     int result = 1;
    //     result = prime * result + this.getId();
    //     result = prime * result + this.getTagKey();
    //     return result;
    // }
    
    // @Override
    // public boolean equals(Object other) {
    //     if ((this == other))
    //     return true;
    //     if ((other == null))
    //     return false;
    //     if (!(other instanceof CellTagId))
    //     return false;
    //     CellTagId castOther = (CellTagId) other;
        
    //     return this.getId() == castOther.getId() && this.getTagKey() == castOther.getTagKey();
    // }
    
    // @Override
    // public boolean equals(Object obj) {
    // if ( this == obj ) {
    // return true;
    // }
    // if ( obj == null || getClass() != obj.getClass() ) {
    // return false;
    // }
    // CellTagId pk = (CellTagId) obj;
    // return Objects.equals( id, pk.id ) &&
    // Objects.equals( tagKey, pk.tagKey );
    // // return super.equals(obj);
    // }

    // @Override
    // public int hashCode() {
    // return Objects.hash( id, tagKey );
    // }

    // @Override
    // public int hashCode() {
    // return super.hashCode();
    // }

    // @Override
    // public boolean equals(Object obj) {
    // if (this == obj)
    // return true;
    // if (obj == null)
    // return false;
    // if (getClass() != obj.getClass())
    // return false;
    // CellTagId other = (CellTagId) obj;
    // if (id == null) {
    // if (other.id != null)
    // return false;
    // } else if (!id.equals(other.id))
    // return false;

    // if (tagKey == null) {
    // if (other.tagKey != null)
    // return false;
    // } else if (!tagKey.equals(other.tagKey))
    // return false;
    // return true;
    // }

}