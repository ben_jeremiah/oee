package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.Employee;

@WebService(name = "employeeService")
public interface EmployeeService {
	Employee getEmployee(Integer id);

	Employee authenticateEmployee(String username, String password);

	Collection<Employee> getEmployees();

	void addEmployee(Employee user);

	void updateEmployee(Employee user);

	void deleteEmployee(Integer id);
}
