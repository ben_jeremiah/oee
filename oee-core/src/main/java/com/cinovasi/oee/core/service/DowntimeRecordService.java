package com.cinovasi.oee.core.service;

import java.util.Collection;
import java.util.Date;

import javax.jws.WebService;

import com.cinovasi.oee.core.dto.PagingQuery;
import com.cinovasi.oee.core.dto.PagingResult;
import com.cinovasi.oee.core.entities.DowntimeRecord;
import com.cinovasi.oee.core.entities.EventLog;

@WebService(name = "downtimeRecordService")
public interface DowntimeRecordService {

    Collection<DowntimeRecord> findAll();

    DowntimeRecord getDowntimeRecord(Long id);

    PagingResult<DowntimeRecord> selectPaging(PagingQuery pq);

    Long countAll();

    Collection<DowntimeRecord> findAllModifiedSince(Date timepoint);

    Collection<DowntimeRecord> findOEE();

    Collection<DowntimeRecord> findDowntimeRecord (Date dateSt, String cellId, String shift);

    DowntimeRecord[] findDowntimeRecordReport(Date stDate, String cellId, String shift);

    Long countAllModifiedSince(Date timepoint);

    DowntimeRecord findLatestRecordByContext(long contextId);

    void calculateDowntime(EventLog log);

    void saveDowntimeRecord(DowntimeRecord record);

    void updateDowntimeRecord(DowntimeRecord record);

    void updateDowntimeRecordReason(DowntimeRecord record);

    DowntimeRecord findLatestRecordByCell(long cellId);


}