package com.cinovasi.oee.core.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author frederick.sugiharto
 * @since 22-Jun-2017
 * @version 1.0.0
 */
public class RuntimeDataDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6549669380430032897L;
	private String tagName = "";
	private String opcDataType = "";
	private String value = "";
	private String quality = "";
	private Date dataTimeStamp;
	private String errMsg = "";

	public RuntimeDataDTO() {

	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getOpcDataType() {
		return opcDataType;
	}

	public void setOpcDataType(String opcDataType) {
		this.opcDataType = opcDataType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Date getDataTimeStamp() {
		return dataTimeStamp;
	}

	public void setDataTimeStamp(Date data_timestamp) {
		this.dataTimeStamp = data_timestamp;
	}

	public void setErrMsg(String err_msg) {
		this.errMsg = err_msg;
	}

	public String getErrMsg() {
		return errMsg;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append("TagName:" + getTagName() + ";");
		sb.append("Value:" + getValue() + ";");
		sb.append("DataType:" + getOpcDataType() + ";");
		sb.append("Quality:" + getQuality() + ";");
		sb.append("Timestamp:" + getDataTimeStamp() + ";");
		sb.append("ErrMsg:" + getErrMsg() + ";");
		sb.append("}");

		return sb.toString();
	}
}