package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.QualityReport;

@WebService(name = "qualityReportService")
public interface QualityReportService {
	QualityReport getQualityReport(Integer id);

	Collection<QualityReport> getQualityReports();

	void addQualityReport(QualityReport user);

	Collection<QualityReport> getQualityReportWhenShift(Integer processDataTypeId, String date, String shiftStart, String shiftEnd);

	Object getQualityReportStatWhenShift(Integer processDataTypeId, String date, String shiftStart, String shiftEnd);

	QualityReport findPreviousReport(Integer processDataTypeId);
	
	// void updateQualityReport(QualityReport user);

	// void deleteQualityReport(Integer id);
}
