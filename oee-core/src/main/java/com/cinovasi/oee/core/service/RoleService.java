package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.Role;

@WebService(name = "roleService")
public interface RoleService {

    Role getRole(Integer id);

    Collection<Role> getRoles();

    void addRole(Role cell);

    void updateRole(Role cell);

    void deleteRole(Integer id);
}