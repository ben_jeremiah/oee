package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.dto.RuntimeDataDTO;

@WebService(name = "runtimeDataService")
public interface RuntimeDataService {

    Collection<RuntimeDataDTO> getData();
    
}