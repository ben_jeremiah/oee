package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "v_daily_cell")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyCellView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "date")
	private Date date;

	@Column(name = "stop_duration_second")
	private Integer stopDurationSecond;

	@Column(name = "running_duration_second")
	private Integer runningDurationSecond;

	@Column(name = "oee")
	private Double oee;

	@Column(name = "oee_record")
	private Long oeeRecord;

	public DailyCellView() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getStopDurationSecond() {
		return stopDurationSecond;
	}

	public void setStopDurationSecond(Integer stopDurationSecond) {
		this.stopDurationSecond = stopDurationSecond;
	}

	public Integer getRunningDurationSecond() {
		return runningDurationSecond;
	}

	public void setRunningDurationSecond(Integer runningDurationSecond) {
		this.runningDurationSecond = runningDurationSecond;
	}

	public Double getOee() {
		return oee;
	}

	public void setOee(Double oee) {
		this.oee = oee;
	}

	public Long getOeeRecord() {
		return oeeRecord;
	}

	public void setOeeRecord(Long oeeRecord) {
		this.oeeRecord = oeeRecord;
	}



}
