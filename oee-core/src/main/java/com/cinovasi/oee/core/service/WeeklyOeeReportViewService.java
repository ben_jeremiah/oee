package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.WeeklyOeeReportView;

@WebService(name = "weeklyOeeReportViewService")
public interface WeeklyOeeReportViewService {

    WeeklyOeeReportView getWeeklyOeeReportViewByCellAndWeekYear(Integer cellId, String week, String year);

    WeeklyOeeReportView getWeeklyLineOeeReportViewByLineAndWeekYear(Integer lineId, String week, String year);

    Collection<WeeklyOeeReportView> getAvailableWeeks();
    
}