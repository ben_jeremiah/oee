package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.ProcessDataType;

@WebService(name = "processDataTypeService")
public interface ProcessDataTypeService {

    ProcessDataType getProcessDataType(Integer id);

    Collection<ProcessDataType> getProcessDataTypes();

    Collection<ProcessDataType> getProcessDataTypesByCell(Integer cellId);
    
    // void addProcessDataType(ProcessDataType cell);

    // void updateProcessDataType(ProcessDataType cell);

    // void deleteProcessDataType(Integer id);
}