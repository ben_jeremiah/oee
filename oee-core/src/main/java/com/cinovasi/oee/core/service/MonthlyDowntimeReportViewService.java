package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.MonthlyDowntimeReportView;

@WebService(name = "monthlyDowntimeReportViewService")
public interface MonthlyDowntimeReportViewService {

    Collection<MonthlyDowntimeReportView> getMonthlyDowntimeReportViewByCellAndMonthYear(Integer cellId, String month, String year);

}