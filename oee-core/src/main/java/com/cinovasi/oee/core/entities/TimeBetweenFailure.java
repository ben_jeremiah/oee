package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "time_between_failure")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeBetweenFailure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "timestamp")
	private Date timestamp;

	@Column(name = "value_minute")
    private Integer valueMinute;

    @ManyToOne
	@JoinColumn(name = "cell_id")
	@JsonIgnore
    private Cell cell;

    @Transient
	private Integer cellId;
	@Transient
    private String cellName;

    public TimeBetweenFailure() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getValueMinute() {
        return valueMinute;
    }

    public void setValueMinute(final Integer valueMinute) {
        this.valueMinute = valueMinute;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(final Cell cell) {
        this.cell = cell;
    }

    public Integer getCellId() {
        return cell != null ? cell.getId() : cellId;
    }

    public void setCellId(final Integer cellId) {
		this.cellId = cellId;
    }
}