package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.OverallDowntimeRecordView;

@WebService(name = "overallDowntimeRecordViewService")
public interface OverallDowntimeRecordViewService {

    OverallDowntimeRecordView getOverallDowntimeRecordView(Integer id);

    Collection<OverallDowntimeRecordView> getOverallDowntimeRecordViews();

}