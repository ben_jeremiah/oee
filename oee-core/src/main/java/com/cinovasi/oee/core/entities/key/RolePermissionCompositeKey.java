package com.cinovasi.oee.core.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RolePermissionCompositeKey implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7206829016686576425L;

	@Column(name = "Permission_Id", nullable = false)
	private int permissionId;

	@Column(name = "Role_Id", nullable = false)
	private int roleId;

	public RolePermissionCompositeKey() {
	}

	public RolePermissionCompositeKey(int roleId, int permissionId) {
		setRoleId(roleId);
		setPermissionId(permissionId);
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPermissionId() {
		return permissionId;
	}

	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	@Override
	public boolean equals(Object obj){
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
