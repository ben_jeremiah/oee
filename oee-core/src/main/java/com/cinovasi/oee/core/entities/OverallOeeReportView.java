package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "v_overall_oee_report")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class OverallOeeReportView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name = "cell_id")
	private Integer cellId;

	@Column(name = "line_id")
	private Integer lineId;

	@Column(name = "name")
	private String name;
	
	@Column(name = "total_count")
	private Integer totalCount;
	
	@Column(name = "month")
	private String month;

	@Column(name ="year")
	private String year;
	
	@Column(name ="oee")
	private Double oee;

	@Column(name ="availability")
	private Double availability;

	@Column(name ="performance")
	private Double performance;

	@Column(name ="quality")
	private Double quality;
	
	@Column(name ="oee_record")
	private Double oeeRecord;

	@Column(name ="updated_at")
	private Date updatedAt;

	public OverallOeeReportView() {
		super();
	}
	
	public OverallOeeReportView(String month, String year) {
		this.month = month;
		this.year = year;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getOee() {
		return oee;
	}

	public void setOee(Double oee) {
		this.oee = oee;
	}

	public Double getAvailability() {
		return availability;
	}

	public void setAvailability(Double availability) {
		this.availability = availability;
	}

	public Double getPerformance() {
		return performance;
	}

	public void setPerformance(Double performance) {
		this.performance = performance;
	}

	public Double getQuality() {
		return quality;
	}

	public void setQuality(Double quality) {
		this.quality = quality;
	}

	public Double getOeeRecord() {
		return oeeRecord;
	}

	public void setOeeRecord(Double oeeRecord) {
		this.oeeRecord = oeeRecord;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getCellId() {
		return cellId;
	}

	public void setCellId(Integer cellId) {
		this.cellId = cellId;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

}
