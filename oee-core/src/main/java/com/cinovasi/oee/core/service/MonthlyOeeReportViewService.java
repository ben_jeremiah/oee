package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.MonthlyOeeReportView;

@WebService(name = "monthlyOeeReportViewService")
public interface MonthlyOeeReportViewService {

    MonthlyOeeReportView getMonthlyOeeReportViewByCellAndMonthYear(Integer cellId, String month, String year);

    MonthlyOeeReportView getMonthlyLineOeeReportViewByLineAndMonthYear(Integer lineId, String month, String year);

    Collection<MonthlyOeeReportView> getAvailableMonthYears();
    
}