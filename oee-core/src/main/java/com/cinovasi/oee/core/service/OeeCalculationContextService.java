package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.OeeCalculationContext;

@WebService(name = "oeeCalculationContextService")
public interface OeeCalculationContextService {

    OeeCalculationContext findById(long id);

    Collection<OeeCalculationContext> findAll();

    OeeCalculationContext findUnfinishedCalculationByCell (int cellId);

    void addOeeCalculationContext(OeeCalculationContext oeeCC);

	void updateOeeCalculationContext(OeeCalculationContext oeeCC);

}