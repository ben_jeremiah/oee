package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "process_data_type")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProcessDataType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name")
	private String name;
	
	@JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cell_id")
    private Cell cell;
	
	@Transient
	private Integer cellId;
	
	public ProcessDataType() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
	}
	
	public Integer getCellId() {
        return cell != null ? cell.getId() : cellId;
    }

    public void setCellId(Integer cellId) {
        this.cellId = cellId;
    }
}