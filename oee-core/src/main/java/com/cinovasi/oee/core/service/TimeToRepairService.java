package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.TimeToRepair;

@WebService(name = "timeToRepairService")
public interface TimeToRepairService {
	void addTimeToRepair(TimeToRepair TTR);

	Collection<TimeToRepair> getTTRBeforeTime(Integer cellId,String date);
}
