package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Entity(name = "oee_calculation_context")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class OeeCalculationContext implements Serializable{
	private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
	public enum Status
	{
		NOT_STARTED(0), STARTED(1), FINISHED(2);

		private final int code;
	   Status(int code)  { this.code = code; }
	   public int getCode() { return code; }
	}

    // @Column(name = "material_id")
    // private Integer materialId;
    
    @Column(name = "start_time")
    private Date startTime;
    
    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "standard_rate")
    private double standardRate; // minutes

    @Column(name = "total_count")
    private int totalCount;

    @Column(name = "bad_count")
    private int badCount;

    @Column(name = "good_count")
    private int goodCount;

    @Column(name = "shift")
    private String shift;

    @Column(name = "status")
    private Status status;

    @Column(name = "planned_downtime")
    private double plannedDownTime; // from DownTimeRecord with category = planned  (minutes)
    
	@Column(name = "unplanned_downtime")
    private double unplannedDownTime; // from DownTimeRecord with category = unplanned  (minutes)

	@Column(name = "stop_duration")
    private double stopDuration; //plannedDowntime + unplannedDownTime
    
    /* calculation */
	@Column(name = "net_available_time")
    private double netAvailableTime;

	@Column(name = "net_operating_time")
    private double netOperatingTime;

	@Column(name = "ideal_operating_time")
    private double idealOperatingTime;

	@Column(name = "lost_quality_time")
    private double lostQualityTime;

    @Column(name = "availability")
    private double availability;

    @Column(name = "performance")
    private double performance;

    @Column(name = "quality")
    private double quality;

    @Column(name = "oee")
    private double oee;

    @Column(name = "updated_at")
    private Date updatedAt;

    @ManyToOne
	@JoinColumn(name = "cell_id")
	@JsonIgnore
    private Cell cell;

    @Transient
    private double scheduledProductionTime; // = endTime - startTime (minutes) 
    @Transient
    private int tempBadCount;
    @Transient
    private boolean addBadCommand;
    @Transient
    private DowntimeRecord tempDownRecord;
    @Transient
    private double deltaTime;
    @Transient
    // private static int speedMesin = 52;
    private static int speedMesin = 10;
    @Transient
	private Integer cellId;
	@Transient
    private String cellName;

    public OeeCalculationContext() {
        init();
    }

    public OeeCalculationContext(int id, Date startTime, Date endTime, double standardRate,
            int totalCount, int badCount, int goodCount, String shift, Status status, double plannedDownTime,
            double unplannedDownTime, double stopDuration, double netAvailableTime, double netOperatingTime,
            double idealOperatingTime, double lostQualityTime, double availability, double performance, double quality,
            double oee, Cell cell) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.standardRate = standardRate;
        this.totalCount = totalCount;
        this.badCount = badCount;
        this.goodCount = goodCount;
        this.shift = shift;
        this.status = status;
        this.plannedDownTime = plannedDownTime;
        this.unplannedDownTime = unplannedDownTime;
        this.stopDuration = stopDuration;
        this.netAvailableTime = netAvailableTime;
        this.netOperatingTime = netOperatingTime;
        this.idealOperatingTime = idealOperatingTime;
        this.lostQualityTime = lostQualityTime;
        this.availability = availability;
        this.performance = performance;
        this.quality = quality;
        this.oee = oee;
        this.cell = cell;
    }
    

    private void init() {
        this.badCount = 0;
        this.goodCount = 0;
        this.totalCount = 0;
        /*this.standardRate = 0.0;*/
        this.plannedDownTime = 0.0;
        this.unplannedDownTime = 0.0;
        this.tempBadCount = 0;
        this.addBadCommand = false;
        this.startTime = new Date();
        this.status = Status.NOT_STARTED;
    }

    public Date getStartTime() {
        return startTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getGoodCount() {
        return goodCount;
    }

    /**
     * @param goodCount the goodCount to set
     */
    public void setGoodCount(int goodCount) {
        this.goodCount = goodCount;
    }

    public int getBadCount() {
        return badCount;
    }

    /**
     * @param badCount the badCount to set
     */
    public void setBadCount(int badCount) {
        this.badCount = badCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount the totalCount to set
     */
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public double getStandardRate() {
        return standardRate;
    }

    public void setStandardRate(double standardRate) {
        this.standardRate = standardRate;
    }

    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    // public Integer getMaterialId() {
    //     return materialId;
    // }

    // public void setMaterialId(Integer materialId) {
    //     this.materialId = materialId;
    // }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
    	this.status = status;
    }

    public double getScheduledProductionTime() {
		return scheduledProductionTime;
	}

	public void setScheduledProductionTime(double  scheduledProductionTime) {
		this.scheduledProductionTime = scheduledProductionTime;
	}

	public double getPlannedDownTime() {
		return plannedDownTime;
	}

	public void setPlannedDownTime(double plannedDownTime) {
		this.plannedDownTime = plannedDownTime;
	}

	public double getUnplannedDownTime() {
		return unplannedDownTime;
	}

	public void setUnplannedDownTime(double unplannedDownTime) {
		this.unplannedDownTime = unplannedDownTime;
	}
	
	public double getStopDuration() {
		return stopDuration = plannedDownTime + unplannedDownTime;
	}

	public void setStopDuration(double stop_duration) {
		//this.stop_duration = stop_duration;
	}

	public double getNetAvailableTime() {
		//netAvailableTime = scheduledProductionTime - plannedDownTime;
		//logger.info("Net Available Time: " + netAvailableTime);
		return netAvailableTime;
	}

	public void setNetAvailableTime(double netAvailableTime) {
		this.netAvailableTime = netAvailableTime;
	}

	public double getNetOperatingTime() {
		/*if (endTime == null){
			netOperatingTime = deltaTime - unplannedDownTime;
		}else{
			netOperatingTime = getNetAvailableTime() - unplannedDownTime;
		}*/
//		logger.info("Net Operating Time: " + netOperatingTime);
		return netOperatingTime;
	}

	public void setNetOperatingTime(double netOperatingTime) {
		// cannot set netOperatingTime directly
		this.netOperatingTime = netOperatingTime;
	}

	public double getIdealOperatingTime() {
		idealOperatingTime = totalCount * standardRate;
//		logger.info("Ideal operating time: " + idealOperatingTime);
		return idealOperatingTime;
	}

	public void setIdealOperatingTime(double idealOperatingTime) {
		// cannot set idealOperatingTime directly
		//this.idealOperatingTime = idealOperatingTime;
	}

	public double getLostQualityTime() {
		lostQualityTime = badCount * standardRate;
//		logger.info("Lost quality time: " + lostQualityTime);
		return lostQualityTime;
	}

	public void setLostQualityTime(double lostQualityTime) {
		// cannot set lostQualityTime directly
		// this.lostQualityTime = lostQualityTime;
	}

    public double getQuality() {
    	if(getTotalCount() > 0)
    	{
    		double good = totalCount - badCount;
    		quality = good / totalCount;
    	}else if(getTotalCount() == 0){
            quality = 0;
        }else{
            quality = -1;
        }
//    	logger.info("Quality: " + quality);
        return quality;
    }

    /**
     * @param quality the quality to set
     */
    public void setQuality(double quality) {
        // cannot set quality directly
    	//this.quality = quality;
    }

    public double getPerformance() {
    	if(getNetOperatingTime() != 0)
    	{
    		performance = getTotalCount() / (speedMesin * getNetOperatingTime());
    	}
    	else performance = 0;
//    	logger.info("Performance: " + performance);
    	return performance;
    }

    /**
     * @param performance the performance to set
     */
    public void setPerformance(double performance) {
    	// cannot set performance directly
        //this.performance = performance;
    }

    public double getAvailability() {
		if(getNetAvailableTime() != 0)
		{
			availability = getNetOperatingTime()/getNetAvailableTime();
		}
		else availability = 0;
//		logger.info("Avalability: " + availability);
    	return availability;
    }

    /**
     * @param availability the availability to set
     */
    public void setAvailability(double availability) {
        // cannot set availibility directly
    	//this.availability = availability;
    }

    public double getOee() {
        /*if (status == Status.STARTED) endTime = new Date();
//        logger.info("Date time: " + endTime.toString());
        if (endTime != null && startTime != null)
            setScheduledProductionTime((endTime.getTime() - startTime.getTime()) / 1000 / 60);*/
            if(getQuality() == -1){
                oee = getAvailability();
            }else{
                oee = getAvailability() * getPerformance() * getQuality();
            }
//        logger.info("OEE: " + oee);
        return oee;
    }
    
    public void setOee(double oee) {
        // cannot set oee directly
    	//this.oee = oee;
    }
    
    @Transient
    public double getDeltaTime() {
		return deltaTime;
	}

	public void setDeltaTime(double deltaTime) {
		this.deltaTime = deltaTime;
    }

    
    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }
    
    public Integer getCellId() {
		return cell != null ? cell.getId() : cellId;
    }
    
	public void setCellId(Integer cellId) {
		this.cellId = cellId;
	}

	public String getCellName() {
		return cell != null ? cell.getName() : cellName;
	}

	public void setCellName(String cellName) {
		this.cellName = cellName;
	}

	public void start(Date startTime)
    {
    	if(this.status == Status.NOT_STARTED)
    	{
	    	this.startTime = startTime;
	    	this.status = Status.STARTED;
    	}
    }

    public void stop(Date endTime)
    {
    	if(getStatus() == Status.STARTED)
    	{
	    	this.endTime = endTime;
	    	this.status = Status.FINISHED;
    	}
    }
    
	/*@Override
	public void update(Observable o, Object arg) {
		// store variables
        if (arg instanceof CellWatcher) {
            CellWatcher watcher = (CellWatcher) arg;
            FieldPointLiveValue fplv = watcher.getUpdateFp();
            String tagName = fplv.getFieldPoint().getTagName();
            if (tagName.startsWith(TagName.MACHINE_PREFIX)) {
                if (tagName.endsWith(TagName.STARTJOBCMD) && fplv.getIntegerValue() == 1) {
                    start(new Date());
                }
                if (tagName.endsWith(TagName.ENDJOBCMD) && fplv.getIntegerValue() == 1) {
                    stop(new Date());
                    setScheduledProductionTime((getEndTime().getTime() - getStartTime().getTime()) / 1000 / 60);
                }
                if (tagName.endsWith(TagName.CURWO)) {
                    setWoNo(fplv.getStringValue());
                }
                if (tagName.endsWith(TagName.QTYFILLEDSACHETS)) {
                    setTotalCount(fplv.getIntegerValue());
                    setGoodCount(getTotalCount() - getBadCount());
                }
                if (tagName.endsWith(TagName.TARGETJOBPRODRATE)) {
                    setStandardRate(fplv.getFloatValue());
                }
                if (tagName.endsWith(TagName.BADPRODUCT_ADDPRODCMD)) {
                    addBadCommand = fplv.getIntegerValue() == 1;
                    cummulateBadCount();
                }
                if (tagName.endsWith(TagName.BADPRODUCT_ADDPRODQTY)) {
                    tempBadCount = fplv.getIntegerValue();
                    cummulateBadCount();
                }
                if (tagName.endsWith(TagName.CURSTATE)) {
                    if (fplv.getIntegerValue() == 0) {
                        if (tempDownRecord == null) {
                            tempDownRecord = new DowntimeRecord();
                            tempDownRecord.setCellId(getCellId());
                            tempDownRecord.setContextId(id);
                            if (woNo != null && woNo.length() > 0)tempDownRecord.setWoNo(getWoNo());
                            tempDownRecord.setStartTime(new Date());
                        }
                    } else {
                        if (tempDownRecord != null) {
                            tempDownRecord.setEndTime(new Date());
//                            Get downtime
                            double curDowntime = (tempDownRecord.getEndTime().getTime() - tempDownRecord.getStartTime().getTime())
                                    /1000 / 60;
                            unplannedDownTime += curDowntime;
//                            Save Downtime record
                            EntityManager em = IsmPersistenceManager.getInstance().createEntityManager();
                            EntityTransaction tx = em.getTransaction();
                            try {
                                tx.begin();
                                em.persist(tempDownRecord);
                                tx.commit();
                            } catch (Exception e) {
                                em.flush();
                            } finally {
                                em.close();
                            }
                            tempDownRecord = null;
                        }
                    }
                }
            }
        }
		if(status == Status.STARTED)
		{
		    endTime = new Date();
            setScheduledProductionTime((new Date().getTime() - getStartTime().getTime()) / 1000 / 60);
		}
		
		// recalculate OEE here
		getOee();
		
	}*/
	
	public void updateCalculationValue(OeeCalculationContext rhs)
	{
		// EntityManager em = IsmPersistenceManager.getInstance().createEntityManager();
        // EntityTransaction tx = em.getTransaction();
        
		// //this.setStartTime(rhs.getStartTime());
		// //this.setEndTime(rhs.getEndTime());
		// //this.setStandardRate(rhs.getStandardRate());
		// //this.setTargetQuantity(rhs.getTargetQuantity());

		// this.setTotalCount(rhs.getTotalCount());
		// this.setBadCount(rhs.getBadCount());
		// this.setGoodCount(rhs.getGoodCount());
		
		// //this.setScheduledProductionTime(rhs.getScheduledProductionTime());
		// this.setPlannedDownTime(rhs.getPlannedDownTime());
		// this.setUnplannedDownTime(rhs.getUnplannedDownTime());
		// this.setNetOperatingTime(rhs.getNetOperatingTime());
		
		// this.getOee();
		
		// try {
        //     tx.begin();
        //     em.merge(this);
        //     tx.commit();
        // } catch (Exception e) {
        //     em.flush();
        // }
	}
	
	
	private void cummulateBadCount() {
        if (addBadCommand && tempBadCount > 0) {
            badCount += tempBadCount;
            tempBadCount = 0;
            addBadCommand = false;
        }
        setGoodCount(getTotalCount() - getBadCount());
//        logger.info("Total count: " + getTotalCount() +
//                ", Bad count: " + getBadCount() +
//                ", Good count: " + getGoodCount());
    }
    
    @Transient
    private static transient Logger logger = LogManager.getLogger(OeeCalculationContext.class);

    public static Logger getLogger() {
        return logger;
    }

    public static void setLogger(Logger logger) {
        OeeCalculationContext.logger = logger;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}