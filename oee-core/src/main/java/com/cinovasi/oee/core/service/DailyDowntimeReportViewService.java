package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.DailyDowntimeReportView;

@WebService(name = "dailyDowntimeReportViewService")
public interface DailyDowntimeReportViewService {

    Collection<DailyDowntimeReportView> getDailyDowntimeReportViewByCellAndDate(Integer cellId, String date);

}