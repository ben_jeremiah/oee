package com.cinovasi.oee.core.service;

import java.util.Collection;
import java.util.Date;

import javax.jws.WebService;

import com.cinovasi.oee.core.dto.PagingQuery;
import com.cinovasi.oee.core.dto.PagingResult;
import com.cinovasi.oee.core.entities.UptimeRecord;
import com.cinovasi.oee.core.entities.EventLog;

@WebService(name = "uptimeRecordService")
public interface UptimeRecordService {

    Collection<UptimeRecord> findAll();

    UptimeRecord getUptimeRecord(Long id);

    PagingResult<UptimeRecord> selectPaging(PagingQuery pq);

    Long countAll();

    void saveUptimeRecord(UptimeRecord record);

    void updateUptimeRecord(UptimeRecord record);

    UptimeRecord findLatestRecordByContext(long contextId);

}