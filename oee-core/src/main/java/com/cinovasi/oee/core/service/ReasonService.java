package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.Reason;

@WebService(name = "reasonService")
public interface ReasonService {

    Reason getReason(Integer id);

    Collection<Reason> getReasons();

    Collection<Reason> getUnplannedReasons();

    void addReason(Reason cell);

    void updateReason(Reason cell);

    void deleteReason(Integer id);
}