package com.cinovasi.oee.core.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cinovasi.oee.core.entities.RuntimeHistory;

public class HistoricalDataDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Date timeStamp;
	private Map<String, RuntimeHistory> variables;

	public HistoricalDataDTO() {
		variables = new HashMap<String, RuntimeHistory>();
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Map<String, RuntimeHistory> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, RuntimeHistory> variables) {
		this.variables = variables;
	}

}