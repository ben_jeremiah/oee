package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Entity(name = "quality_report")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualityReport implements Serializable{
	private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "value")
    private Double value;

    @Column(name = "value_difference")
    private Double valueDifference;

    @ManyToOne
	@JoinColumn(name = "process_data_type_id")
	@JsonIgnore
    private ProcessDataType processDataType;

    @Transient
	private Integer processDataTypeId;

    public QualityReport() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public ProcessDataType getProcessDataType() {
        return processDataType;
    }

    public void setProcessDataType(ProcessDataType processDataType) {
        this.processDataType = processDataType;
    }

    public Integer getProcessDataTypeId() {
		return processDataType != null ? processDataType.getId() : processDataTypeId;
	}

	public void setProcessDataTypeId(Integer processDataTypeId) {
		this.processDataTypeId = processDataTypeId;
	}

    public Double getValueDifference() {
        return valueDifference;
    }

    public void setValueDifference(Double valueDifference) {
        this.valueDifference = valueDifference;
    }
    
}