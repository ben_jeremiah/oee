package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.DailyCellView;

@WebService(name = "dailyCellViewService")
public interface DailyCellViewService {

    DailyCellView getDailyCellView(Integer id);

    Collection<DailyCellView> getDailyCellViews();


}