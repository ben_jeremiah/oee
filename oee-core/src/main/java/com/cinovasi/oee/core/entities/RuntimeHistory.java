// EXAMPLE FROM VOCIP
package com.cinovasi.oee.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.cinovasi.oee.core.helper.BooleanToIntegerConverter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name="runtime_history")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuntimeHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Column(name = "time_stamp")
	private long timeStamp;
	@Column(name = "tag_name")
	private String tagName;
	@Column(name = "data_type")
	private String dataType;
	@Column(name = "value")
	private String value;
	@Column(name = "int_value")
	private Integer intValue;
	@Column(name = "float_value")
	private Double floatValue;
	@Convert(converter=BooleanToIntegerConverter.class)
	@Column(name = "bool_value")
	private Boolean boolValue;
	@Column(name = "string_value")
	private String stringValue;
	@Column(name = "date_value")
	private Date dateValue;
	@Convert(converter=BooleanToIntegerConverter.class)
	@Column(name = "quality")
	private Boolean quality;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getIntValue() {
		return intValue;
	}
	public void setIntValue(Integer intValue) {
		this.intValue = intValue;
	}
	public Double getFloatValue() {
		return floatValue;
	}
	public void setFloatValue(Double floatValue) {
		this.floatValue = floatValue;
	}
	public Boolean getBoolValue() {
		return boolValue;
	}
	public void setBoolValue(Boolean boolValue) {
		this.boolValue = boolValue;
	}
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public Date getDateValue() {
		return dateValue;
	}
	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}
	public Boolean getQuality() {
		return quality;
	}
	public void setQuality(Boolean quality) {
		this.quality = quality;
	}
	
	
	
}
