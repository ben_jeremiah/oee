package com.cinovasi.oee.core.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserRoleCompositeKey implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7206829016686576425L;

	@Column(name = "User_Id", nullable = false)
	private int userId;

	@Column(name = "Role_Id", nullable = false)
	private int roleId;

	public UserRoleCompositeKey() {
	}

	public UserRoleCompositeKey(int userId, int roleId) {
		setUserId(userId);
		setRoleId(roleId);
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	@Override
	public boolean equals(Object obj){
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
