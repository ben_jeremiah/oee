package com.cinovasi.oee.core.constants;

public enum Tag {

    RUNNING(1, "RUNNING"),
    DOWNTIME(2, "DOWNTIME"),
    IDLE(3, "IDLE"),
    EMERGENCY(4, "EMERGENCY"),
    PART_DELAY(5, "PART_DELAY"),
    COUNT_FINISH_GOOD(6, "COUNT_FINISH_GOOD"),
    OEE(7, "OEE"),
    AVAILABILITY(8, "AVAILABILITY"),
    PERFORMANCE(9, "PERFORMANCE"),
    QUALITY(10, "QUALITY"),
    BEARING_DEPTH(11, "BEARING_DEPTH"),
    BARCODE_ENGINE(12, "BARCODE_ENGINE"),
    PRESSURE_LOAD_CELL(13, "PRESSURE_LOAD_CELL"),
    TORQUE_OK(14, "TORQUE_OK"),
    NUT_RUNNER_TORQUE(15, "NUT_RUNNER_TORQUE"),
    NUT_RUNNER_ANGLE(16, "NUT_RUNNER_ANGLE"),
    TIRE_PRESSURE(17, "TIRE_PRESSURE"),
    TIGHTENING_OK(18, "TIGHTENING_OK"),
    BARCODE_FRAME(19, "BARCODE_FRAME"),
    AIR_PRESSURE(20, "AIR_PRESSURE"),
    ;
	
    private int tag;
    private String tagPhrase;

    Tag(int tag, String tagPhrase) {
        this.tag = tag;
        this.tagPhrase = tagPhrase;
    }

    public int getTag() {
        return tag;
    }

    public String getTagPhrase() {
        return tagPhrase;
    }

    public static Tag fromTagCode(int tagCode) {
        for (Tag c : Tag.values()) {
            if (c.tag == tagCode)
                return c;
        }

        return null;
    }
}