package com.cinovasi.oee.core.helper;

import javax.persistence.AttributeConverter;

public class BooleanToIntegerConverter implements AttributeConverter<Boolean, Integer> {

	@Override
	public Integer convertToDatabaseColumn(Boolean attribute) {
		if(attribute == null) return null;
		return attribute ? 1 : 0;
	}

	@Override
	public Boolean convertToEntityAttribute(Integer dbData) {
		if(dbData == null) return null;
		return dbData == 0 ? false : true;
	}

}
