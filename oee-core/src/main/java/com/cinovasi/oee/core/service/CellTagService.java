package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.CellTag;

@WebService(name = "cellTagService")
public interface CellTagService {
    CellTag getCellTag(Long id);

    Collection<CellTag> getCellTags();

    void addCellTag(CellTag cellTag);

    void updateCellTag(CellTag cellTag);

    void deleteCellTag(Long id);
    
    Collection<CellTag> getNonLineTags();
    
}
