package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "v_weekly_downtime_report")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeeklyDowntimeReportView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name = "reason_id")
	private Integer reasonId;

	@Column(name = "description")
	private String description;

	@Column(name = "cell_id")
	private Integer cellId;

	@Column(name ="year")
	private String year;
	
	@Column(name = "week")
	private String week;

	@Column(name = "start_week")
	private Date startWeek;

	@Column(name = "end_week")
	private Date endWeek;

	@Column(name = "freq")
	private Long freq;
	
	@Column(name = "seconds")
	private Long seconds;
	
	public WeeklyDowntimeReportView() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getFreq() {
		return freq;
	}

	public void setFreq(Long freq) {
		this.freq = freq;
	}

	public Long getSeconds() {
		return seconds;
	}

	public void setSeconds(Long seconds) {
		this.seconds = seconds;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public Date getStartWeek() {
		return startWeek;
	}

	public void setStartWeek(Date startWeek) {
		this.startWeek = startWeek;
	}

	public Date getEndWeek() {
		return endWeek;
	}

	public void setEndWeek(Date endWeek) {
		this.endWeek = endWeek;
	}

	public Integer getCellId() {
		return cellId;
	}

	public void setCellId(Integer cellId) {
		this.cellId = cellId;
	}

	public Integer getReasonId() {
		return reasonId;
	}

	public void setReasonId(Integer reasonId) {
		this.reasonId = reasonId;
	}


}
