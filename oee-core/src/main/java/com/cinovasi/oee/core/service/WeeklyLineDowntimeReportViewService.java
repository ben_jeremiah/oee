package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.WeeklyLineDowntimeReportView;

@WebService(name = "weeklyLineDowntimeReportViewService")
public interface WeeklyLineDowntimeReportViewService {

    Collection<WeeklyLineDowntimeReportView> getWeeklyLineDowntimeReportViewByLineAndWeekYear(Integer lineId, String week, String year);

}