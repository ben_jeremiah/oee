package com.cinovasi.oee.core.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class IsmPersistenceManager {

	public static final boolean DEBUG = true;

	private static final IsmPersistenceManager singleton = new IsmPersistenceManager();

	protected EntityManagerFactory emf;

	public static IsmPersistenceManager getInstance() {

		return singleton;
	}

	private IsmPersistenceManager() {
	}

	public EntityManagerFactory getEntityManagerFactory() {

		if (emf == null) {
			createEntityManagerFactory();
		}
		return emf;
	}

	public void closeEntityManagerFactory() {

		if (emf != null) {
			emf.close();
			emf = null;
			if (DEBUG) {
				System.out.println("n*** Persistence finished at "
						+ new java.util.Date());
			}
		}
	}

	protected void createEntityManagerFactory() {

		this.emf = Persistence.createEntityManagerFactory("puIsm");
		if (DEBUG) {
			System.out.println("n*** Persistence started at " + new java.util.Date());
		}
	}

	public EntityManager createEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}

}
