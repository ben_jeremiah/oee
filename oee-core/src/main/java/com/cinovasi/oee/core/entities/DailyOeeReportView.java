package com.cinovasi.oee.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "v_daily_oee_report")
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyOeeReportView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	@Column(name = "cell_id")
	private Integer cellId;

	@Column(name = "line_id")
	private Integer lineId;

	@Column(name = "name")
	private String name;

	@Column(name ="date")
	private Date date;
	
	@Column(name ="oee")
	private Double oee;

	@Column(name ="availability")
	private Double availability;

	@Column(name ="performance")
	private Double performance;

	@Column(name ="quality")
	private Double quality;
	
	@Column(name ="oee_record")
	private Double oeeRecord;

	@Column(name ="updated_at")
	private Date updatedAt;

	public DailyOeeReportView() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getOee() {
		return oee;
	}

	public void setOee(Double oee) {
		this.oee = oee;
	}

	public Double getAvailability() {
		return availability;
	}

	public void setAvailability(Double availability) {
		this.availability = availability;
	}

	public Double getPerformance() {
		return performance;
	}

	public void setPerformance(Double performance) {
		this.performance = performance;
	}

	public Double getQuality() {
		return quality;
	}

	public void setQuality(Double quality) {
		this.quality = quality;
	}

	public Double getOeeRecord() {
		return oeeRecord;
	}

	public void setOeeRecord(Double oeeRecord) {
		this.oeeRecord = oeeRecord;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Integer getCellId() {
		return cellId;
	}

	public void setCellId(Integer cellId) {
		this.cellId = cellId;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

}
