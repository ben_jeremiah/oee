package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.WeeklyDowntimeReportView;

@WebService(name = "weeklyDowntimeReportViewService")
public interface WeeklyDowntimeReportViewService {

    Collection<WeeklyDowntimeReportView> getWeeklyDowntimeReportViewByCellAndWeekYear(Integer cellId, String week, String year);

}