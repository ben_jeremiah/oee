package com.cinovasi.oee.core.service;

import java.util.Collection;

import javax.jws.WebService;

import com.cinovasi.oee.core.entities.Shift;

@WebService(name = "shiftService")
public interface ShiftService {

    Shift getShift(Integer id);

    Collection<Shift> getShifts();

    void updateShift(Shift shift);

}