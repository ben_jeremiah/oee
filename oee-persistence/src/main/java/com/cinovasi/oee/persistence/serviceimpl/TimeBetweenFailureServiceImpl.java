package com.cinovasi.oee.persistence.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.TimeBetweenFailure;
import com.cinovasi.oee.core.service.TimeBetweenFailureService;

@Bean
@Service
@Transactional
public class TimeBetweenFailureServiceImpl implements TimeBetweenFailureService {

	Logger LOG = LoggerFactory.getLogger(TimeBetweenFailureServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	public void addTimeBetweenFailure(TimeBetweenFailure tbf) {
		tbf.setCell(em.find(Cell.class, tbf.getCellId()));
		em.persist(tbf);
		em.flush();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<TimeBetweenFailure> getTBFBeforeTime(Integer cellId, String date) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<TimeBetweenFailure> criteria = cb.createQuery(TimeBetweenFailure.class);
		Root<TimeBetweenFailure> root = criteria.from(TimeBetweenFailure.class);
		Join<TimeBetweenFailure,Cell> cell = root.join("cell");

		Date dateFormatted = null;

		try {
			dateFormatted = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date+" 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		criteria.select(root).where(
			cb.and(
				cb.equal(cell.get("id"),cellId),
				cb.lessThanOrEqualTo(root.get("timestamp"), dateFormatted)
			));
		
		
		Collection<TimeBetweenFailure> res = null;
		try{
			res = em.createQuery(criteria).getResultList();
		}catch (NoResultException nre){
		//Ignore this because as per your logic this is ok!
		}
		
		if(res != null){
			return res;
		}else{
			return null;
		}
	}

}
