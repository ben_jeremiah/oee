package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.CellTag;
// import com.cinovasi.oee.core.entities.CipSession;
import com.cinovasi.oee.core.service.CellTagService;

// @OsgiServiceProvider(classes = { CellTagService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("CellTagServiceImpl")

@Bean
@Service
@Transactional
public class CellTagServiceImpl implements CellTagService {

	Logger LOG = LoggerFactory.getLogger(CellTagServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public CellTag getCellTag(Long id) {
		return em.find(CellTag.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<CellTag> getCellTags() {
		CriteriaQuery<CellTag> query = em.getCriteriaBuilder().createQuery(CellTag.class);
		return em.createQuery(query.select(query.from(CellTag.class))).getResultList();
	}

	@Override
	public void addCellTag(CellTag cellTag) {
		cellTag.setCell(em.find(Cell.class, cellTag.getCellId()));
		em.persist(cellTag);
		em.flush();
	}
	
	@Override
	public void updateCellTag(CellTag cellTag){
		cellTag.setCell(em.find(Cell.class, cellTag.getCellId()));
		em.merge(cellTag);
	}

	@Override
	public void deleteCellTag(Long id) {
		em.remove(getCellTag(id));
	}

	@Override
	public Collection<CellTag> getNonLineTags() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<CellTag> criteria = cb.createQuery(CellTag.class);
		
		Root<CellTag> root = criteria.from(CellTag.class);
		criteria.select(root).where(cb.isNull(root.get("cell")));
		
        TypedQuery<CellTag> query = em.createQuery(criteria);
        return query.getResultList();
	}

}
