package com.cinovasi.oee.persistence.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.OeeCalculationContext;
import com.cinovasi.oee.core.service.OeeCalculationContextService;

@Bean
@Service
@Transactional
public class OeeCalculationContextImpl implements OeeCalculationContextService {

    Logger LOG = LoggerFactory.getLogger(OeeCalculationContextImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
    public OeeCalculationContext findById(long id) {
        return em.find(OeeCalculationContext.class, id);
    }

    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
    public Collection<OeeCalculationContext> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OeeCalculationContext> cq = cb.createQuery(OeeCalculationContext.class);
        Root<OeeCalculationContext> rb = cq.from(OeeCalculationContext.class);
        cq.select(rb);

        TypedQuery<OeeCalculationContext> tb = em.createQuery(cq);
        Collection<OeeCalculationContext> result = tb.getResultList();
        return result;
    }
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
    public OeeCalculationContext findUnfinishedCalculationByCell (int cellId) {
    	CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<OeeCalculationContext> cq = cb.createQuery(OeeCalculationContext.class);
        Root<OeeCalculationContext> root = cq.from(OeeCalculationContext.class);

		Join<OeeCalculationContext,Cell> cell = root.join("cell");

        cq.where(cb.and(cb.isNull(root.get("endTime"))), cb.equal(cell.get("id"), cellId));
        

        TypedQuery<OeeCalculationContext> tq = em.createQuery(cq);
        if (!tq.getResultList().isEmpty()) {
            OeeCalculationContext obj = tq.getResultList().get(0);
            return obj;
        } else
            return null;
    }

    @Override
	public void addOeeCalculationContext(OeeCalculationContext oeeCC) {
        oeeCC.setCell(em.find(Cell.class, oeeCC.getCellId()));
		em.persist(oeeCC);
		em.flush();
	}

	@Override
	public void updateOeeCalculationContext(OeeCalculationContext oeeCC) {
        oeeCC.setCell(em.find(Cell.class, oeeCC.getCellId()));
		em.merge(oeeCC);
	}
}