package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.OverallOeeReportView;
import com.cinovasi.oee.core.service.OverallOeeReportViewService;

@Bean
@Service
@Transactional
public class OverallOeeReportViewServiceImpl implements OverallOeeReportViewService {

	Logger LOG = LoggerFactory.getLogger(OverallOeeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public OverallOeeReportView getOverallOeeReportView() {
		CriteriaQuery<OverallOeeReportView> query = em.getCriteriaBuilder().createQuery(OverallOeeReportView.class);
		return em.createQuery(query.select(query.from(OverallOeeReportView.class))).getSingleResult();
	}

}