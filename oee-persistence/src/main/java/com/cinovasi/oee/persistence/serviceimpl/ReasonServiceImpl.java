package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Reason;
import com.cinovasi.oee.core.service.ReasonService;

// @OsgiServiceProvider(classes = { ReasonService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("ReasonServiceImpl")

@Bean
@Service
@Transactional
public class ReasonServiceImpl implements ReasonService {

	Logger LOG = LoggerFactory.getLogger(ReasonServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Reason getReason(Integer id) {
		return em.find(Reason.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Reason> getReasons() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Reason> criteria = cb.createQuery(Reason.class);
		Root<Reason> root = criteria.from(Reason.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Reason> getUnplannedReasons() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Reason> criteria = cb.createQuery(Reason.class);
		Root<Reason> root = criteria.from(Reason.class);
		criteria.select(root).where(cb.equal(root.get("type"),1));
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

	@Override
	public void addReason(Reason shift) {
		em.persist(shift);
		em.flush();
	}

	@Override
	public void updateReason(Reason shift) {
		em.merge(shift);

	}

	@Override
	public void deleteReason(Integer id) {
		em.remove(getReason(id));
	}
}