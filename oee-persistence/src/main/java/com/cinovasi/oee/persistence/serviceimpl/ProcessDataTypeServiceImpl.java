package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.ProcessDataType;
import com.cinovasi.oee.core.service.ProcessDataTypeService;

// @OsgiServiceProvider(classes = { ProcessDataTypeService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("ProcessDataTypeServiceImpl")

@Bean
@Service
@Transactional
public class ProcessDataTypeServiceImpl implements ProcessDataTypeService {

	Logger LOG = LoggerFactory.getLogger(ProcessDataTypeServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public ProcessDataType getProcessDataType(Integer id) {
		return em.find(ProcessDataType.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<ProcessDataType> getProcessDataTypes() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProcessDataType> criteria = cb.createQuery(ProcessDataType.class);
		Root<ProcessDataType> root = criteria.from(ProcessDataType.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<ProcessDataType> getProcessDataTypesByCell(Integer cellId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ProcessDataType> criteria = cb.createQuery(ProcessDataType.class);
		Root<ProcessDataType> root = criteria.from(ProcessDataType.class);

		Join<ProcessDataType,Cell> cell = root.join("cell");

        criteria.where(cb.equal(cell.get("id"), cellId)).orderBy(cb.asc(root.get("id")));
        

        TypedQuery<ProcessDataType> tq = em.createQuery(criteria);

		return tq.getResultList();
	}

}