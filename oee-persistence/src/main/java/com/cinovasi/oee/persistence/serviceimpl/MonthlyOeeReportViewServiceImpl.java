package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.MonthlyOeeReportView;
import com.cinovasi.oee.core.service.MonthlyOeeReportViewService;

@Bean
@Service
@Transactional
public class MonthlyOeeReportViewServiceImpl implements MonthlyOeeReportViewService {

	Logger LOG = LoggerFactory.getLogger(MonthlyOeeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public MonthlyOeeReportView getMonthlyOeeReportViewByCellAndMonthYear(Integer cellId, String month, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MonthlyOeeReportView> criteria = cb.createQuery(MonthlyOeeReportView.class);
		Root<MonthlyOeeReportView> root = criteria.from(MonthlyOeeReportView.class);
		
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("cellId"),cellId),
				cb.and(
					cb.equal(root.get("month"), month),
					cb.equal(root.get("year"), year)
				)
			)
		);


		return em.createQuery(criteria).getSingleResult();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<MonthlyOeeReportView> getAvailableMonthYears() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MonthlyOeeReportView> criteria = cb.createQuery(MonthlyOeeReportView.class);
		Root<MonthlyOeeReportView> root = criteria.from(MonthlyOeeReportView.class);
		criteria.multiselect(root.get("month"), root.get("year")).where(cb.and(cb.isNotNull(root.get("month")),
		cb.isNotNull(root.get("year"))));
		criteria.groupBy(root.get("month"), root.get("year")).orderBy(cb.desc(root.get("year")),cb.desc(root.get("month")));;

		Collection<MonthlyOeeReportView> res = em.createQuery(criteria).getResultList();

		return res;
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public MonthlyOeeReportView getMonthlyLineOeeReportViewByLineAndMonthYear(Integer lineId, String month, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MonthlyOeeReportView> criteria = cb.createQuery(MonthlyOeeReportView.class);
		Root<MonthlyOeeReportView> root = criteria.from(MonthlyOeeReportView.class);
		
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("lineId"),lineId),
				cb.and(
					cb.equal(root.get("month"), month),
					cb.equal(root.get("year"), year)
				)
			)
		).orderBy(cb.asc(root.get("availability")));


		return em.createQuery(criteria).setMaxResults(1).getSingleResult();
	}

}