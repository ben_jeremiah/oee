package com.cinovasi.oee.persistence.serviceimpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.PagingQuery;
import com.cinovasi.oee.core.dto.PagingResult;
import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.DowntimeRecord;
import com.cinovasi.oee.core.entities.EventLog;
import com.cinovasi.oee.core.entities.OeeCalculationContext;
import com.cinovasi.oee.core.entities.Reason;
import com.cinovasi.oee.core.service.DowntimeRecordService;

@Bean
@Service
@Transactional
public class DowntimeRecordServiceImpl implements DowntimeRecordService {

    Logger LOG = LoggerFactory.getLogger(DowntimeRecordServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DowntimeRecord getDowntimeRecord(Long id) {
		return em.find(DowntimeRecord.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
    public Collection<DowntimeRecord> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
		Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);
		cq.select(rb);

		TypedQuery<DowntimeRecord> tb = em.createQuery(cq);
		List<DowntimeRecord> result = tb.getResultList();
		return result;
	}

    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public PagingResult<DowntimeRecord> selectPaging(PagingQuery param) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DowntimeRecord> criteria = cb.createQuery(DowntimeRecord.class);
		CriteriaQuery<Long> countCriteria = cb.createQuery(Long.class);

		Root<DowntimeRecord> root = criteria.from(DowntimeRecord.class);
		Root<DowntimeRecord> countRoot = countCriteria.from(DowntimeRecord.class);
		criteria.select(root);
		countCriteria.select(cb.count(countRoot));

		List<Predicate> predicates = new ArrayList<Predicate>();
		List<Predicate> countPredicates = new ArrayList<Predicate>();
		
		if (!param.getSearchParam().isEmpty()) {
			if(param.hasParam("fltCell"))
			{
				String toSearch = param.getParam("fltCell").toLowerCase();
				Join<DowntimeRecord, Cell> equ = root.join("cell");
				predicates.add(cb.like(cb.lower(equ.get("name")), String.format("%%%s%%",toSearch)));
				Join<DowntimeRecord, Cell> countEqu = countRoot.join("cell");
				countPredicates.add(cb.like(cb.lower(countEqu.get("name")), String.format("%%%s%%",toSearch)));
			}
			
			if(param.hasParam("fltStartDate"))
			{
                String toSearch = param.getParam("fltStartDate");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                try
                {
					Date lo = df.parse(toSearch);
	                Calendar cal = Calendar.getInstance();
	                cal.setTime(lo);
	                cal.add(Calendar.DAY_OF_MONTH, 1);
	                Date hi = cal.getTime();
	                
	                predicates.add(cb.greaterThanOrEqualTo(root.get("startTime"), lo));
	                predicates.add(cb.lessThan(root.get("startTime"), hi));
	                
	                countPredicates.add(cb.greaterThanOrEqualTo(countRoot.get("startTime"), lo));
	                countPredicates.add(cb.lessThan(countRoot.get("startTime"), hi));
	                
                }
                catch(ParseException exc)
                {
                	LOG.info("Error while processing fltStartDate {}, skip filtering", param.getParam("fltStartDate"));
                }
			}

			
			
		}

        criteria.where(cb.and(predicates.toArray(new Predicate[0])));
        countCriteria.where(cb.and(countPredicates.toArray(new Predicate[0])));

		if (param.getSortColumn() != null
				&& param.getSortColumn().length() != 0) {
			if (param.getSortDirection() == PagingQuery.Direction.Asc) {
				criteria.orderBy(cb.asc(root.get(param.getSortColumn())));
			} else {
				criteria.orderBy(cb.desc(root.get(param.getSortColumn())));
			}
		}

		TypedQuery<DowntimeRecord> query = em.createQuery(criteria);
		TypedQuery<Long> countQuery = em.createQuery(countCriteria);

		if (param.getPageSize() != 0) {
			query.setMaxResults(param.getPageSize());
			query.setFirstResult(param.getPageIndex() * param.getPageSize());
		}

		List<DowntimeRecord> records = query.getResultList();
		Long recordCount = countQuery.getSingleResult();

		PagingResult<DowntimeRecord> pr = new PagingResult<DowntimeRecord>();
		pr.setPage(param.getPageIndex());
		pr.setRecords(records);
		pr.setTotalRecords(recordCount);
		pr.setSuccess(true);
		return pr;
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Long countAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);
		cq.select(cb.count(rb));

		TypedQuery<Long> tb = em.createQuery(cq);
		Long result = tb.getSingleResult();
		return result;
	}

    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<DowntimeRecord> findAllModifiedSince(Date timepoint) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
		Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);

        Expression<Date> exp = rb.get("updatedTime");
        Predicate p1 = cb.greaterThanOrEqualTo(exp, timepoint);

		cq.select(rb);
        cq.where(p1);
		
		TypedQuery<DowntimeRecord> tb = em.createQuery(cq);
		List<DowntimeRecord> result = tb.getResultList();
		return result;
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<DowntimeRecord> findOEE () {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
		Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);

       // Expression<Date> exp = rb.get("updatedTime");
       // Predicate p1 = cb.greaterThanOrEqualTo(exp, timepoint);

		cq.select(rb);
        //cq.where(p1);
		
		TypedQuery<DowntimeRecord> tb = em.createQuery(cq);
		List<DowntimeRecord> result = tb.getResultList();
		return result;
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<DowntimeRecord> findDowntimeRecord (Date dateSt, String cellId, String shift) {
		List<DowntimeRecord> result = new ArrayList<>();
		DowntimeRecord[] DowntimeRecorddata = null;
		try {
				DowntimeRecorddata = findDowntimeRecordReport(dateSt, cellId, shift);
			
			if (DowntimeRecorddata != null) {
				result.addAll(Arrays.asList(DowntimeRecorddata));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
    }
    
	private String extractValue(Object obj) {
		if (obj == null) {
			return null;
		}
		return obj.toString();
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DowntimeRecord[] findDowntimeRecordReport(Date stDate, String cellId, String shift) {
		List<DowntimeRecord> DowntimeRecorddt = new ArrayList<>();
		try {

		StoredProcedureQuery storedProcedureQuery = em.createStoredProcedureQuery("dbo.spe_report_downtime_record");
		
		storedProcedureQuery.registerStoredProcedureParameter("p_time", Date.class, ParameterMode.IN);
		storedProcedureQuery.setParameter("p_time", stDate);
		storedProcedureQuery.registerStoredProcedureParameter("p_assetID", Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter("p_assetID", Integer.parseInt(cellId));
		storedProcedureQuery.registerStoredProcedureParameter("p_shift", Integer.class, ParameterMode.IN);
		storedProcedureQuery.setParameter("p_shift", Integer.parseInt(shift));
		storedProcedureQuery.execute();

		List<Object> result = storedProcedureQuery.getResultList();

			if (result == null || result.size() == 0) {
				return null;
			}

			for (Object object : result) {
				Object[] cells = (Object[]) object;

				DowntimeRecord DowntimeRecordData = new DowntimeRecord();
				// DowntimeRecordData.setStartTime(extractValue(cells[0]) == null ? "--" : extractValue(cells[0]));
				// DowntimeRecordData.setEndTime(extractValue(cells[1]) == null ? "--" : extractValue(cells[1]));
				// DowntimeRecordData.setDuration(extractValue(cells[2]) == null ? "--" : extractValue(cells[2]));
				// DowntimeRecordData.setReason(extractValue(cells[3]));
				
				DowntimeRecorddt.add(DowntimeRecordData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return DowntimeRecorddt.toArray(new DowntimeRecord[0]);
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Long countAllModifiedSince(Date timepoint) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);

        Expression<Date> exp = rb.get("updatedTime");
        Predicate p1 = cb.greaterThanOrEqualTo(exp, timepoint);


		cq.select(cb.count(rb));
        cq.where(p1);

		TypedQuery<Long> tb = em.createQuery(cq);
		Long result = tb.getSingleResult();
		return result;
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DowntimeRecord findLatestRecordByContext(long contextId) {
		List<DowntimeRecord> listResult = new ArrayList();
		DowntimeRecord result = new DowntimeRecord();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
			Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);
			
			List<Predicate> predicates = new ArrayList<>();

			Join<DowntimeRecord,OeeCalculationContext> context = rb.join("context");
			
			Predicate predicate3 = cb.isNull(rb.get("endTime"));
			
			predicates.add(predicate3);
			
			cq.select(rb).where(cb.and(predicates.toArray(new Predicate[0]))).orderBy(cb.desc(rb.get("startTime"))).where(cb.equal(context.get("id"), contextId));
			TypedQuery<DowntimeRecord> query = em.createQuery(cq);

			listResult = query.getResultList();
			if (listResult.size() > 0){
				result = listResult.get(0);
			}else {
				result = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
    public void calculateDowntime(EventLog log) {
        EntityTransaction tx = em.getTransaction();

        if (log != null && log.getEvent() != null) {

            if (log.getEvent().toUpperCase().equals("OFF")) {
                try {
                    DowntimeRecord downtimeRecord = new DowntimeRecord();
                    downtimeRecord.setCellId(log.getCellId());
                    downtimeRecord.setStartTime(log.getEvTimestamp());
                    // downtimeRecord.setReason(log.getReason());
                    // downtimeRecord.setOperatorNotes(log.getNotes());

                    tx.begin();
                    em.persist(downtimeRecord);
                    tx.commit();
                    tx = null;

                } catch (Exception e) {
                    if (tx != null) {
                        tx.rollback();
                    }
                    e.printStackTrace();
                }

            } else if (log.getEvent().toUpperCase().equals("ON")) {
                try {
                    CriteriaBuilder cb = em.getCriteriaBuilder();
                    CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
                    Root<DowntimeRecord> root = cq.from(DowntimeRecord.class);
                    List<Predicate> predicates = new ArrayList<>();

                    Expression<Integer> expCell = root.get("cell_id");

                    Predicate predicateCell = cb.equal(expCell, log.getCellId());
                    predicates.add(predicateCell);

                    Expression<Date> exp = root.get("endTime");
                    Predicate predicate = cb.isNull(exp);
                    predicates.add(predicate);

                    cq.where(cb.and(predicates.toArray(new Predicate[0])));

                    TypedQuery<DowntimeRecord> tq = em.createQuery(cq);
                    if (!tq.getResultList().isEmpty()) {
                        DowntimeRecord obj = tq.getResultList().get(0);
                        obj.setEndTime(log.getCreatedDate());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
	
	@Override
    public void saveDowntimeRecord(DowntimeRecord record){
    	EntityTransaction tx = em.getTransaction();
    	try{
    		if(record != null){
				tx.begin();
				record.setCell(em.find(Cell.class, record.getCellId()));
				if(record.getReasonId() != null){
					record.setReason(em.find(Reason.class, record.getReasonId()));
				}
	    		em.persist(record);
	    		tx.commit();
	    		tx = null;
    		}
    	}catch(Exception e){
    		if(tx != null){
    			tx.rollback();
    		}
    		e.printStackTrace();
    	}
    }
    
	@Override
    public void updateDowntimeRecord(DowntimeRecord record){
    	EntityTransaction tx = em.getTransaction();
    	try{
    		if(record != null){
				tx.begin();
				record.setContext(em.find(OeeCalculationContext.class, record.getContextId()));
				record.setCell(em.find(Cell.class, record.getCellId()));
				if(record.getReasonId() != null){
					record.setReason(em.find(Reason.class, record.getReasonId()));
				}
    			em.merge(record);
    			tx.commit();
    			tx = null;
    		}
    	}catch(Exception e){
    		if(tx != null){
    			tx.rollback();
    		}
    		e.printStackTrace();
    	}
	}

	@Override
    public void updateDowntimeRecordReason(DowntimeRecord record){
    	EntityTransaction tx = em.getTransaction();
    	try{
    		if(record != null){
				tx.begin();
				record.setContext(em.find(OeeCalculationContext.class, record.getContextId()));
				record.setCell(em.find(Cell.class, record.getCellId()));
				if(record.getReasonId() != null){
					DowntimeRecord previousDowntime = em.find(DowntimeRecord.class, record.getId());

					record.setReason(em.find(Reason.class, record.getReasonId()));

					//check if previous downtime already have reason or no, if no just calculate, if yes
					//check value difference for recalculate and
					//Update OEE only for downtime record that already finished
					OeeCalculationContext context = record.getContext();
					if(previousDowntime.getReason() != null){
						if(previousDowntime.getReason().getType() != record.getReason().getType() ){
							if(record.getEndTime() != null){
								double downtimeDuration = (record.getEndTime().getTime() - record.getStartTime().getTime()) / 1000.0 / 60.0;
								if (record.getReason().getType() == 2) {
									//correct previous value
									context.setPlannedDownTime(context.getPlannedDownTime() + downtimeDuration);
									context.setUnplannedDownTime(context.getUnplannedDownTime() - downtimeDuration);
								} else {
									//correct previous value
									context.setUnplannedDownTime(context.getUnplannedDownTime() + downtimeDuration);
									context.setPlannedDownTime(context.getPlannedDownTime() - downtimeDuration);
								}
		
								double unplanned = context.getUnplannedDownTime();
								double planned = context.getPlannedDownTime();
								double cummulativeDowntime = context.getStopDuration();
		
								//NAT & NOT		
								Date now = new Date();		
								double nat = ((now.getTime() - context.getStartTime().getTime()) / 1000.0 / 60.0) - planned;
								double not = ((now.getTime() - context.getStartTime().getTime()) / 1000.0 / 60.0) - cummulativeDowntime;
								context.setNetAvailableTime(nat);
								context.setNetOperatingTime(not);
								context.getOee(); // force recalculate
		
								em.merge(context);
							}
						}
					}else{
						if(record.getEndTime() != null && record.getReason().getType() == 2){
							double downtimeDuration = (record.getEndTime().getTime() - record.getStartTime().getTime()) / 1000.0 / 60.0;

							context.setPlannedDownTime(context.getPlannedDownTime() + downtimeDuration);
							context.setUnplannedDownTime(context.getUnplannedDownTime() - downtimeDuration);
							
							double unplanned = context.getUnplannedDownTime();
							double planned = context.getPlannedDownTime();
							double cummulativeDowntime = context.getStopDuration();
	
							//NAT & NOT		
							Date now = new Date();		
							double nat = ((now.getTime() - context.getStartTime().getTime()) / 1000.0 / 60.0) - planned;
							double not = ((now.getTime() - context.getStartTime().getTime()) / 1000.0 / 60.0) - cummulativeDowntime;
							context.setNetAvailableTime(nat);
							context.setNetOperatingTime(not);
							context.getOee(); // force recalculate
	
							em.merge(context);
						}
					}
				}
    			em.merge(record);
    			tx.commit();
    			tx = null;
    		}
    	}catch(Exception e){
    		if(tx != null){
    			tx.rollback();
    		}
    		e.printStackTrace();
    	}
	}


	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DowntimeRecord findLatestRecordByCell(long cellId) {
		List<DowntimeRecord> listResult = new ArrayList();
		DowntimeRecord result = new DowntimeRecord();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<DowntimeRecord> cq = cb.createQuery(DowntimeRecord.class);
			Root<DowntimeRecord> rb = cq.from(DowntimeRecord.class);
			

			Join<DowntimeRecord,Cell> cell = rb.join("cell");
			
			cq.select(rb).where(cb.and(
				cb.equal(cell.get("id"), cellId),
				cb.or(cb.equal(rb.get("tagTrigger"), "EMERGENCY"),cb.equal(rb.get("tagTrigger"), "DOWNTIME")),
				cb.isNull(rb.get("previousDowntimeId"))
			)).orderBy(cb.desc(rb.get("startTime")));
			TypedQuery<DowntimeRecord> query = em.createQuery(cq);

			listResult = query.getResultList();
			if (listResult.size() > 0){
				result = listResult.get(0);
			}else {
				result = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}