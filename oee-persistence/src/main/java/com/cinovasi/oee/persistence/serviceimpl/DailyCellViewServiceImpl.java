package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.DailyCellView;
import com.cinovasi.oee.core.service.DailyCellViewService;

@Bean
@Service
@Transactional
public class DailyCellViewServiceImpl implements DailyCellViewService {

	Logger LOG = LoggerFactory.getLogger(DailyCellViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DailyCellView getDailyCellView(Integer id) {
		return em.find(DailyCellView.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<DailyCellView> getDailyCellViews() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DailyCellView> criteria = cb.createQuery(DailyCellView.class);
		Root<DailyCellView> root = criteria.from(DailyCellView.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

}