package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.OverallDowntimeRecordView;
import com.cinovasi.oee.core.service.OverallDowntimeRecordViewService;

@Bean
@Service
@Transactional
public class OverallDowntimeRecordViewServiceImpl implements OverallDowntimeRecordViewService {

	Logger LOG = LoggerFactory.getLogger(OverallDowntimeRecordViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public OverallDowntimeRecordView getOverallDowntimeRecordView(Integer id) {
		return em.find(OverallDowntimeRecordView.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<OverallDowntimeRecordView> getOverallDowntimeRecordViews() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<OverallDowntimeRecordView> criteria = cb.createQuery(OverallDowntimeRecordView.class);
		Root<OverallDowntimeRecordView> root = criteria.from(OverallDowntimeRecordView.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

}