package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.MonthlyDowntimeReportView;
import com.cinovasi.oee.core.service.MonthlyDowntimeReportViewService;

@Bean
@Service
@Transactional
public class MonthlyDowntimeReportViewServiceImpl implements MonthlyDowntimeReportViewService {

	Logger LOG = LoggerFactory.getLogger(MonthlyDowntimeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<MonthlyDowntimeReportView> getMonthlyDowntimeReportViewByCellAndMonthYear(Integer cellId, String month, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MonthlyDowntimeReportView> criteria = cb.createQuery(MonthlyDowntimeReportView.class);
		Root<MonthlyDowntimeReportView> root = criteria.from(MonthlyDowntimeReportView.class);
		
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("cellId"),cellId),
				cb.and(
					cb.equal(root.get("month"), month),
					cb.equal(root.get("year"), year)
				)
			)
		);


		return em.createQuery(criteria).getResultList();
	}

}