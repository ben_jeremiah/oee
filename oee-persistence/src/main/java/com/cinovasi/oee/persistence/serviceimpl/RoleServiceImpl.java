package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Role;
import com.cinovasi.oee.core.service.RoleService;

// @OsgiServiceProvider(classes = { RoleService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("RoleServiceImpl")

@Bean
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	Logger LOG = LoggerFactory.getLogger(RoleServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Role getRole(Integer id) {
		return em.find(Role.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Role> getRoles() {
		CriteriaQuery<Role> query = em.getCriteriaBuilder().createQuery(Role.class);
		return em.createQuery(query.select(query.from(Role.class))).getResultList();
	}

	@Override
	public void addRole(Role cell) {
		em.persist(cell);
		em.flush();
	}

	@Override
	public void updateRole(Role cell) {
		em.merge(cell);

	}

	@Override
	public void deleteRole(Integer id) {
		em.remove(getRole(id));
	}
}