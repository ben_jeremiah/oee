package com.cinovasi.oee.persistence.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Employee;
import com.cinovasi.oee.core.entities.Role;
import com.cinovasi.oee.core.service.EmployeeService;

@Bean
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Employee getEmployee(Integer id) {
		return em.find(Employee.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Employee> getEmployees() {
		CriteriaQuery<Employee> query = em.getCriteriaBuilder().createQuery(Employee.class);
		return em.createQuery(query.select(query.from(Employee.class))).getResultList();
	}

	@Override
	public void addEmployee(Employee employee) {
		employee.setRole(em.find(Role.class, employee.getRoleId()));
		em.persist(employee);
		em.flush();
	}

	@Override
	public void updateEmployee(Employee employee) {
		employee.setRole(em.find(Role.class, employee.getRoleId()));
		em.merge(employee);
	}

	@Override
	public void deleteEmployee(Integer id) {
		em.remove(id);
	}

	@Override
	public Employee authenticateEmployee(String username, String password) {
		LOG.info("EmployeeServiceImpl - param username : " + username);
		LOG.info("EmployeeServiceImpl - param password : " + password);

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
		Root<Employee> root = cq.from(Employee.class);

		List<Predicate> predicates = new ArrayList<>();

		Expression<String> exp = root.get("username");
		Predicate predicate = cb.equal(exp, username);
		predicates.add(predicate);

		exp = root.get("password");
		predicate = cb.equal(exp, password);
		predicates.add(predicate);

		// predicate = cb.equal(exp, CommonUtils.hashing(password,
		// CommonUtils.HASH_MD5));

		cq.where(cb.and(predicates.toArray(new Predicate[0])));
		TypedQuery<Employee> tq = em.createQuery(cq);

		if (tq.getResultList().isEmpty()) {
			LOG.info("EmployeeServiceImpl - employee not found");
			return null;
		} else {
			Employee employee = tq.getSingleResult();
			if (employee != null) {
				employee.setToken(employee.getToken() == null ? UUID.randomUUID().toString() : employee.getToken());
				employee.setLastAccess(new Date());
				em.merge(employee);
				return employee;
			} else
				return null;
		}
	}

}
