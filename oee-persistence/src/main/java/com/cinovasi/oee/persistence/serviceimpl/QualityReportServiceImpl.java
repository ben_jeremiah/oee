package com.cinovasi.oee.persistence.serviceimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.ProcessDataType;
import com.cinovasi.oee.core.entities.QualityReport;
import com.cinovasi.oee.core.service.QualityReportService;

@Bean
@Service
@Transactional
public class QualityReportServiceImpl implements QualityReportService {

	Logger LOG = LoggerFactory.getLogger(QualityReportServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public QualityReport getQualityReport(Integer id) {
		return em.find(QualityReport.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<QualityReport> getQualityReports() {
		CriteriaQuery<QualityReport> query = em.getCriteriaBuilder().createQuery(QualityReport.class);
		return em.createQuery(query.select(query.from(QualityReport.class))).getResultList();
	}

	@Override
	public void addQualityReport(QualityReport qualityReport) {
		qualityReport.setProcessDataType(em.find(ProcessDataType.class, qualityReport.getProcessDataTypeId()));
		em.persist(qualityReport);
		em.flush();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<QualityReport> getQualityReportWhenShift(Integer processDataTypeId, String date, String shiftStart, String shiftEnd) {
		//Easier using native query for this case
		Collection<QualityReport> res = em.createNativeQuery(
			"SELECT * from quality_report where"+
			"(timestamp\\:\\:time) between '"+shiftStart+"'"+
			"AND '"+shiftEnd+"' and process_data_type_id = "+processDataTypeId+" and timestamp\\:\\:date = '"+date+"' order by id",
			QualityReport.class).getResultList();


		return res;
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Object getQualityReportStatWhenShift(Integer processDataTypeId, String date, String shiftStart, String shiftEnd) {
		//Easier using native query for this case
		Object res = null;
		try {
			res = (Object) em.createNativeQuery(
			"SELECT Count(process_data_type_id), AVG(value) as avg_x, "+
			"((SUM(value_difference) - (SELECT value_difference from quality_report where (timestamp\\:\\:time) between '"+shiftStart+"' AND '"+shiftEnd+"' and "+
			"process_data_type_id = "+processDataTypeId+" and timestamp\\:\\:date = '"+date+"' order by id limit 1)) / (count(value_difference)-1)) as avg_r from quality_report where"+
			"(timestamp\\:\\:time) between '"+shiftStart+"'"+
			"AND '"+shiftEnd+"' and process_data_type_id = "+processDataTypeId+" and timestamp\\:\\:date = '"+date+"' group by process_data_type_id"
			).getSingleResult();
		} catch (Exception e) {
			// System.out.println(e);
		}
		
		return res;
	}
	
	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public QualityReport findPreviousReport(Integer processDataTypeId) {
		QualityReport res = null;
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<QualityReport> criteria = cb.createQuery(QualityReport.class);
			Root<QualityReport> root = criteria.from(QualityReport.class);

			Join<QualityReport,ProcessDataType> processDataType = root.join("processDataType");

			criteria.where(cb.equal(processDataType.get("id"), processDataTypeId)).orderBy(cb.desc(root.get("id")));        

			TypedQuery<QualityReport> tq = em.createQuery(criteria).setMaxResults(1);
			res = tq.getSingleResult();
		} catch (Exception e) {
			// System.out.println(e);
		}
		

		return res;
	}
}
