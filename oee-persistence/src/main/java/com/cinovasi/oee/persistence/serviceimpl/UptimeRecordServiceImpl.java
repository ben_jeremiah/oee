package com.cinovasi.oee.persistence.serviceimpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.dto.PagingQuery;
import com.cinovasi.oee.core.dto.PagingResult;
import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.entities.UptimeRecord;
import com.cinovasi.oee.core.entities.EventLog;
import com.cinovasi.oee.core.entities.OeeCalculationContext;
import com.cinovasi.oee.core.entities.Reason;
import com.cinovasi.oee.core.service.UptimeRecordService;

@Bean
@Service
@Transactional
public class UptimeRecordServiceImpl implements UptimeRecordService {

    Logger LOG = LoggerFactory.getLogger(UptimeRecordServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public UptimeRecord getUptimeRecord(Long id) {
		return em.find(UptimeRecord.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
    public Collection<UptimeRecord> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UptimeRecord> cq = cb.createQuery(UptimeRecord.class);
		Root<UptimeRecord> rb = cq.from(UptimeRecord.class);
		cq.select(rb);

		TypedQuery<UptimeRecord> tb = em.createQuery(cq);
		List<UptimeRecord> result = tb.getResultList();
		return result;
	}

    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public PagingResult<UptimeRecord> selectPaging(PagingQuery param) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<UptimeRecord> criteria = cb.createQuery(UptimeRecord.class);
		CriteriaQuery<Long> countCriteria = cb.createQuery(Long.class);

		Root<UptimeRecord> root = criteria.from(UptimeRecord.class);
		Root<UptimeRecord> countRoot = countCriteria.from(UptimeRecord.class);
		criteria.select(root);
		countCriteria.select(cb.count(countRoot));

		List<Predicate> predicates = new ArrayList<Predicate>();
		List<Predicate> countPredicates = new ArrayList<Predicate>();
		
		if (!param.getSearchParam().isEmpty()) {
			if(param.hasParam("fltCell"))
			{
				String toSearch = param.getParam("fltCell").toLowerCase();
				Join<UptimeRecord, Cell> equ = root.join("cell");
				predicates.add(cb.like(cb.lower(equ.get("name")), String.format("%%%s%%",toSearch)));
				Join<UptimeRecord, Cell> countEqu = countRoot.join("cell");
				countPredicates.add(cb.like(cb.lower(countEqu.get("name")), String.format("%%%s%%",toSearch)));
			}
			
			if(param.hasParam("fltStartDate"))
			{
                String toSearch = param.getParam("fltStartDate");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                try
                {
					Date lo = df.parse(toSearch);
	                Calendar cal = Calendar.getInstance();
	                cal.setTime(lo);
	                cal.add(Calendar.DAY_OF_MONTH, 1);
	                Date hi = cal.getTime();
	                
	                predicates.add(cb.greaterThanOrEqualTo(root.get("startTime"), lo));
	                predicates.add(cb.lessThan(root.get("startTime"), hi));
	                
	                countPredicates.add(cb.greaterThanOrEqualTo(countRoot.get("startTime"), lo));
	                countPredicates.add(cb.lessThan(countRoot.get("startTime"), hi));
	                
                }
                catch(ParseException exc)
                {
                	LOG.info("Error while processing fltStartDate {}, skip filtering", param.getParam("fltStartDate"));
                }
			}

			
			
		}

        criteria.where(cb.and(predicates.toArray(new Predicate[0])));
        countCriteria.where(cb.and(countPredicates.toArray(new Predicate[0])));

		if (param.getSortColumn() != null
				&& param.getSortColumn().length() != 0) {
			if (param.getSortDirection() == PagingQuery.Direction.Asc) {
				criteria.orderBy(cb.asc(root.get(param.getSortColumn())));
			} else {
				criteria.orderBy(cb.desc(root.get(param.getSortColumn())));
			}
		}

		TypedQuery<UptimeRecord> query = em.createQuery(criteria);
		TypedQuery<Long> countQuery = em.createQuery(countCriteria);

		if (param.getPageSize() != 0) {
			query.setMaxResults(param.getPageSize());
			query.setFirstResult(param.getPageIndex() * param.getPageSize());
		}

		List<UptimeRecord> records = query.getResultList();
		Long recordCount = countQuery.getSingleResult();

		PagingResult<UptimeRecord> pr = new PagingResult<UptimeRecord>();
		pr.setPage(param.getPageIndex());
		pr.setRecords(records);
		pr.setTotalRecords(recordCount);
		pr.setSuccess(true);
		return pr;
	}
    
    @Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Long countAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<UptimeRecord> rb = cq.from(UptimeRecord.class);
		cq.select(cb.count(rb));

		TypedQuery<Long> tb = em.createQuery(cq);
		Long result = tb.getSingleResult();
		return result;
	}

	@Override
    public void saveUptimeRecord(UptimeRecord record){
    	EntityTransaction tx = em.getTransaction();
    	try{
    		if(record != null){
				tx.begin();
				record.setCell(em.find(Cell.class, record.getCellId()));
	    		em.persist(record);
	    		tx.commit();
	    		tx = null;
    		}
    	}catch(Exception e){
    		if(tx != null){
    			tx.rollback();
    		}
    		e.printStackTrace();
    	}
    }
    
	@Override
    public void updateUptimeRecord(UptimeRecord record){
    	EntityTransaction tx = em.getTransaction();
    	try{
    		if(record != null){
				tx.begin();
				record.setContext(em.find(OeeCalculationContext.class, record.getContextId()));
				record.setCell(em.find(Cell.class, record.getCellId()));
    			em.merge(record);
    			tx.commit();
    			tx = null;
    		}
    	}catch(Exception e){
    		if(tx != null){
    			tx.rollback();
    		}
    		e.printStackTrace();
    	}
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public UptimeRecord findLatestRecordByContext(long contextId) {
		List<UptimeRecord> listResult = new ArrayList();
		UptimeRecord result = new UptimeRecord();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<UptimeRecord> cq = cb.createQuery(UptimeRecord.class);
			Root<UptimeRecord> rb = cq.from(UptimeRecord.class);
			
			List<Predicate> predicates = new ArrayList<>();

			Join<UptimeRecord,OeeCalculationContext> context = rb.join("context");
			
			Predicate predicate3 = cb.isNull(rb.get("endTime"));
			
			predicates.add(predicate3);
			
			cq.select(rb).where(cb.and(predicates.toArray(new Predicate[0]))).orderBy(cb.desc(rb.get("startTime"))).where(cb.equal(context.get("id"), contextId));
			TypedQuery<UptimeRecord> query = em.createQuery(cq);

			listResult = query.getResultList();
			if (listResult.size() > 0){
				result = listResult.get(0);
			}else {
				result = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


}