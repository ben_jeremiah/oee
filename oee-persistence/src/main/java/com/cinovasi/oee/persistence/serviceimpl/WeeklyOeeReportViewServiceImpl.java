package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.WeeklyOeeReportView;
import com.cinovasi.oee.core.service.WeeklyOeeReportViewService;

@Bean
@Service
@Transactional
public class WeeklyOeeReportViewServiceImpl implements WeeklyOeeReportViewService {

	Logger LOG = LoggerFactory.getLogger(WeeklyOeeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public WeeklyOeeReportView getWeeklyOeeReportViewByCellAndWeekYear(Integer cellId, String week, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WeeklyOeeReportView> criteria = cb.createQuery(WeeklyOeeReportView.class);
		Root<WeeklyOeeReportView> root = criteria.from(WeeklyOeeReportView.class);

		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("cellId"),cellId),				
				cb.equal(root.get("week"), week),
				cb.equal(root.get("year"), year)
			)
		);


		return em.createQuery(criteria).getSingleResult();
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<WeeklyOeeReportView> getAvailableWeeks() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WeeklyOeeReportView> criteria = cb.createQuery(WeeklyOeeReportView.class);
		Root<WeeklyOeeReportView> root = criteria.from(WeeklyOeeReportView.class);
		criteria.multiselect(root.get("year"), root.get("week"),root.get("startWeek"),root.get("endWeek")).where(cb.and(
			cb.isNotNull(root.get("year")),
			cb.isNotNull(root.get("week")),
			cb.isNotNull(root.get("startWeek")),
			cb.isNotNull(root.get("endWeek"))
		));
		criteria.groupBy(root.get("year"), root.get("week"), root.get("startWeek"),root.get("endWeek")).orderBy(cb.desc(root.get("year")),cb.desc(root.get("week")));

		Collection<WeeklyOeeReportView> res = em.createQuery(criteria).getResultList();

		return res;
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public WeeklyOeeReportView getWeeklyLineOeeReportViewByLineAndWeekYear(Integer lineId, String week, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WeeklyOeeReportView> criteria = cb.createQuery(WeeklyOeeReportView.class);
		Root<WeeklyOeeReportView> root = criteria.from(WeeklyOeeReportView.class);

		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("lineId"),lineId),				
				cb.equal(root.get("week"), week),
				cb.equal(root.get("year"), year)
			)
		).orderBy(cb.asc(root.get("availability")));


		return em.createQuery(criteria).setMaxResults(1).getSingleResult();
	}
}