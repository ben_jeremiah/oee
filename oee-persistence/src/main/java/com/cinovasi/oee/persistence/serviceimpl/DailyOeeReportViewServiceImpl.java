package com.cinovasi.oee.persistence.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.DailyOeeReportView;
import com.cinovasi.oee.core.service.DailyOeeReportViewService;

@Bean
@Service
@Transactional
public class DailyOeeReportViewServiceImpl implements DailyOeeReportViewService {

	Logger LOG = LoggerFactory.getLogger(DailyOeeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DailyOeeReportView getDailyOeeReportViewByCellAndDate(Integer cellId, String date) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DailyOeeReportView> criteria = cb.createQuery(DailyOeeReportView.class);
		Root<DailyOeeReportView> root = criteria.from(DailyOeeReportView.class);

		Date dateFormatted = null;
		try {
			dateFormatted = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("cellId"),cellId),
				cb.equal(root.get("date"),dateFormatted)
			));
		
		
		DailyOeeReportView res = null;
		try{
			res = em.createQuery(criteria).getSingleResult();
		}catch (NoResultException nre){
		//Ignore this because as per your logic this is ok!
		}
		
		if(res != null){
			return res;
		}else{
			return null;
		}
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public DailyOeeReportView getDailyLineOeeReportViewByLineAndDate(Integer lineId, String date) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DailyOeeReportView> criteria = cb.createQuery(DailyOeeReportView.class);
		Root<DailyOeeReportView> root = criteria.from(DailyOeeReportView.class);

		Date dateFormatted = null;
		try {
			dateFormatted = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("lineId"),lineId),
				cb.equal(root.get("date"),dateFormatted)
			)).orderBy(cb.asc(root.get("availability")));
		
		
		DailyOeeReportView res = null;
		try{
			res = em.createQuery(criteria).setMaxResults(1).getSingleResult();
		}catch (NoResultException nre){
		//Ignore this because as per your logic this is ok!
		}
		
		if(res != null){
			return res;
		}else{
			return null;
		}
	}

}