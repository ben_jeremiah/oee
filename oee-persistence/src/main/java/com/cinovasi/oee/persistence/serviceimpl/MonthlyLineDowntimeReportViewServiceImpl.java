package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.MonthlyLineDowntimeReportView;
import com.cinovasi.oee.core.service.MonthlyLineDowntimeReportViewService;

@Bean
@Service
@Transactional
public class MonthlyLineDowntimeReportViewServiceImpl implements MonthlyLineDowntimeReportViewService {

	Logger LOG = LoggerFactory.getLogger(MonthlyLineDowntimeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<MonthlyLineDowntimeReportView> getMonthlyLineDowntimeReportViewByLineAndMonthYear(Integer lineId, String month, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<MonthlyLineDowntimeReportView> criteria = cb.createQuery(MonthlyLineDowntimeReportView.class);
		Root<MonthlyLineDowntimeReportView> root = criteria.from(MonthlyLineDowntimeReportView.class);
		
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("lineId"),lineId),
				cb.and(
					cb.equal(root.get("month"), month),
					cb.equal(root.get("year"), year)
				)
			)
		);


		return em.createQuery(criteria).getResultList();
	}

}