package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Shift;
import com.cinovasi.oee.core.service.ShiftService;

// @OsgiServiceProvider(classes = { ShiftService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("ShiftServiceImpl")

@Bean
@Service
@Transactional
public class ShiftServiceImpl implements ShiftService {

	Logger LOG = LoggerFactory.getLogger(ShiftServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Shift getShift(Integer id) {
		return em.find(Shift.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Shift> getShifts() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Shift> criteria = cb.createQuery(Shift.class);
		Root<Shift> root = criteria.from(Shift.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

	@Override
	public void updateShift(Shift shift) {
		em.merge(shift);

	}
}