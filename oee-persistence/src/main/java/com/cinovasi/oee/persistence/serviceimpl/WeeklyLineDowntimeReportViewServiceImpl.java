package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
import java.util.Date;

// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.WeeklyLineDowntimeReportView;
import com.cinovasi.oee.core.service.WeeklyLineDowntimeReportViewService;

@Bean
@Service
@Transactional
public class WeeklyLineDowntimeReportViewServiceImpl implements WeeklyLineDowntimeReportViewService {

	Logger LOG = LoggerFactory.getLogger(WeeklyLineDowntimeReportViewServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<WeeklyLineDowntimeReportView> getWeeklyLineDowntimeReportViewByLineAndWeekYear(Integer lineId, String week, String year) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WeeklyLineDowntimeReportView> criteria = cb.createQuery(WeeklyLineDowntimeReportView.class);
		Root<WeeklyLineDowntimeReportView> root = criteria.from(WeeklyLineDowntimeReportView.class);
		
		criteria.select(root).where(
			cb.and(
				cb.equal(root.get("lineId"),lineId),
				cb.and(
					cb.equal(root.get("week"), week),
					cb.equal(root.get("year"), year)
				)
			)
		);


		return em.createQuery(criteria).getResultList();
	}

}