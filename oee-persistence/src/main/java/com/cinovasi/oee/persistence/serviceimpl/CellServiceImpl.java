package com.cinovasi.oee.persistence.serviceimpl;

import java.util.Collection;
// import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.aries.blueprint.annotation.bean.Bean;
import org.apache.aries.blueprint.annotation.service.Service;
// import org.ops4j.pax.cdi.api.OsgiServiceProvider;
// import org.ops4j.pax.cdi.api.Properties;
// import org.ops4j.pax.cdi.api.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cinovasi.oee.core.entities.Cell;
import com.cinovasi.oee.core.service.CellService;

// @OsgiServiceProvider(classes = { CellService.class })
// // The properties below allow to transparently export the service as a web
// // service using Distributed OSGi
// @Properties({ @Property(name = "service.exported.interfaces", value = "*") })
// @Named("CellServiceImpl")

@Bean
@Service
@Transactional
public class CellServiceImpl implements CellService {

	Logger LOG = LoggerFactory.getLogger(CellServiceImpl.class);

	@PersistenceContext(unitName = "oee")
	EntityManager em;

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Cell getCell(Integer id) {
		return em.find(Cell.class, id);
	}

	@Override
	@Transactional(Transactional.TxType.SUPPORTS)
	public Collection<Cell> getCells() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Cell> criteria = cb.createQuery(Cell.class);
		Root<Cell> root = criteria.from(Cell.class);
		criteria.orderBy(cb.asc(root.get("id")));

		return em.createQuery(criteria).getResultList();
	}

	@Override
	public void addCell(Cell cell) {
		em.persist(cell);
		em.flush();
	}

	@Override
	public void updateCell(Cell cell) {
		em.merge(cell);

	}

	@Override
	public void deleteCell(Integer id) {
		em.remove(getCell(id));
	}

	// @Override
	// public Collection<Cell> getCellsByLineId(Integer lineId) {
	// 	Line line = em.find(Line.class, lineId);
	// 	if (line != null)
	// 		return line.getCells();
	// 	else
	// 		return null;
	// }

}